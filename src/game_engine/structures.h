#ifndef STRUCTURES_H
#define STRUCTURES_H

#include <math.h>
#include <QColor>

struct Vector
{
    double X;
    double Y;
    double Z;

    Vector ()
    {
        X = 0;
        Y = 0;
        Z = 0;
    }

    Vector(double x, double y, double z)
    {
        X = x;
        Y = y;
        Z = z;
    }

    double Length ()
    {
        return sqrt(LengthSquared());
    }

    double LengthSquared ()
    {
        return (X*X + Y*Y + Z*Z);
    }

    bool Equals (Vector v)
    {
        return (X == v.X) && (Y == v.Y) && (Z == v.Z);
    }

    Vector Add (Vector v)
    {
        return Vector(X+v.X, Y+v.Y, Z+v.Z);
    }

    Vector Subtract (Vector v)
    {
        return Vector(X-v.X, Y-v.Y, Z-v.Z);
    }

    Vector Multiply (double value)
    {
        return Vector(X*value, Y*value, Z*value);
    }

    Vector Normalize (Vector v)
    {
        double length = v.Length();

        if(length != 0.0)
            return Vector(v.X/length, v.Y/length, v.Z/length);

        //It's not technically possible to normalize a zero vector but the
        //following is used to prevent errors
        return Vector(0,0,0);
    }

    double DotProduct (Vector v)
    {
        return (X*v.X) + (Y*v.Y) + (Z*v.Z);
    }

    Vector CrossProduct (Vector v)
    {
        double nx = Y*v.Z - Z*v.Y;
        double ny = Z*v.X - X*v.Z;
        double nz = X*v.Y - Y*v.X;

        return Vector(nx, ny, nz);
    }

    void operator = (const Vector &rightVector)
    {
        X = rightVector.X;
        Y = rightVector.Y;
        Z = rightVector.Z;
    }

    bool operator == (const Vector &rightVector)
    {
        return this->Equals(rightVector);
    }

    Vector operator + (const Vector &rightVector)
    {
        return this->Add(rightVector);
    }

    Vector operator * (double value)
    {
        return this->Multiply(value);
    }

    double operator * (const Vector &rightVector)
    {
        return this->DotProduct(rightVector);
    }

    Vector operator - (const Vector &rightVector)
    {
        return this->Subtract(rightVector);
    }

};

struct Point
{
    float X;
    float Y;

    Point ()
    {
        X = 0;
        Y = 0;
    }

    Point (float x, float y)
    {
        X = x;
        Y = y;
    }
};

struct Color
{
    float Red;
    float Green;
    float Blue;
    float Alpha;

    Color ()
    {
        Red = 1;
        Green = 1;
        Blue = 1;
        Alpha = 1;
    }

    Color(float red, float green, float blue, float alpha)
    {
        Red = red;
        Green = green;
        Blue = blue;
        Alpha = alpha;
    }

    Color(QColor other)
    {
        Red = other.redF ();
        Green = other.greenF ();
        Blue = other.blueF ();
        Alpha = other.alphaF ();
    }

    Color& operator=(const Color& other)
    {
        this->Red = other.Red;
        this->Green = other.Green;
        this->Blue = other.Blue;
        this->Alpha = other.Alpha;
        return *this;
    }
};

#endif // STRUCTURES_H
