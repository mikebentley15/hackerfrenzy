#include "game_engine/fastloopbase.h"
#include "audit.h"

#include <QDebug>
#include <QTimer>
#include <QTime>


/*** =Public Methods= ***/

FastLoopBase::FastLoopBase ()
{
    _timer = new QTimer(this);
    _watch = new QTime();

    _totalElapsedTime = 0;
  
    // makes the timeout signal happen whenever no other events are
    // to be processed.
    _timer->setInterval (0);  
  
    // Connect the timer to this class's
    connect (_timer, SIGNAL(timeout()),
             this,   SLOT(loopFunction()));
  
    // Register to the Audit class
    Audit* audit = Audit::get ();
    audit->registerQObject (this);
    audit->registerQObject (_timer);
    audit->objectCreated ("QTime");
}

FastLoopBase::~FastLoopBase ()
{
    delete _watch;
    Audit::get()->objectDestroyed ("QTime");
}

bool FastLoopBase::isActive ()
{
    return _timer->isActive ();
}


/*** =Public Slots= ***/

void FastLoopBase::start ()
{
    _timer->start ();
    _watch->start();
    _totalElapsedTime = 0;
}

void FastLoopBase::stop ()
{
    _timer->stop ();
}


/*** =Protected Slots= ***/

void FastLoopBase::loopFunction ()
{
    long newTotalElapsedTime = _watch->elapsed();
    long elapsedTime = newTotalElapsedTime - _totalElapsedTime;
    _totalElapsedTime = newTotalElapsedTime;

    double elapsedTimeSec = (double) elapsedTime / 1000;

    this->callLoopOnce (elapsedTimeSec);
}
