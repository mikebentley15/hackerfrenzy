#ifndef STATESYSTEM_H
#define STATESYSTEM_H

#include <QHash>
#include <QWidget> // includes all of the event objects

class State;

class StateSystem : public QObject
{
    Q_OBJECT

public:

    StateSystem(QWidget* parent);
    ~StateSystem ();
    
    void render ();
    bool exists (const QString &stateId) const;
    QString getCurrentState () const;
    State* getCurrentStateObject () const;
    void addState (const QString &stateId, State* state);

    // Events from the owning QWidget.  These get forwarded
    // to the active state.
    void closeEvent            (QCloseEvent* event);
    void dragEnterEvent        (QDragEnterEvent* event);
    void dragLeaveEvent        (QDragLeaveEvent* event);
    void dragMoveEvent         (QDragMoveEvent* event);
    void dropEvent             (QDropEvent* event);
    void enterEvent            (QEvent* event);
    void keyPressEvent         (QKeyEvent* event);
    void keyReleaseEvent       (QKeyEvent* event);
    void leaveEvent            (QEvent* event);
    void mouseDoubleClickEvent (QMouseEvent* event);
    void mouseMoveEvent        (QMouseEvent* event);
    void mousePressEvent       (QMouseEvent* event);
    void mouseReleaseEvent     (QMouseEvent* event);
    void resizeEvent           (QResizeEvent* event);
    void tabletEvent           (QTabletEvent* event);
    void wheelEvent            (QWheelEvent* event);

public slots:
    void changeState (const QString &stateId);
    void update (double elapsedTime);

signals:
    void stateChanged (const QString &newState);
    
private:
    // _stateStore   - Set of states in this state machine
    // _hashIter     - Iterator over the set of states
    // _currentState - Name of the current state
    QHash<QString, State*>* _stateStore;
    QHash<QString, State*>::iterator _hashIter;
    State* _currentState;
    QWidget* _parentWidget;
    QSize _windowSize;
};

#endif // STATESYSTEM_H
