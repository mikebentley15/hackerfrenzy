#ifndef TEXTURE_H
#define TEXTURE_H

class Texture
{
private:
    int _id;
    int _width;
    int _height;

public:
    Texture ();
    Texture (int id, int width, int height);

    Texture& operator = (const Texture &rightTexture);

    int getId () const;
    int getHeight () const;
    int getWidth () const;

};

#endif // TEXTURE_H
