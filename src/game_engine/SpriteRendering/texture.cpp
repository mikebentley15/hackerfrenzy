#include "game_engine/SpriteRendering/texture.h"

Texture::Texture ()
{
    _id = 0;
    _width = 0;
    _height = 0;
}

Texture::Texture(int id, int width, int height)
{
    _id = id;
    _width = width;
    _height = height;
}

int Texture::getHeight() const
{
    return _height;
}

int Texture::getId() const
{
    return _id;
}

int Texture::getWidth() const
{
    return _width;
}
Texture& Texture::operator = (const Texture &rightTexture)
{
    if(this == &rightTexture)
        return *this;

    //Nothing to clean up for memory

    _id = rightTexture.getId();
    _width = rightTexture.getWidth();
    _height = rightTexture.getHeight();

    return *this;
}
