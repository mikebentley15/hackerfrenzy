#ifndef RENDERSPRITE_H
#define RENDERSPRITE_H

#include <QObject>
#include "game_engine/structures.h"
#include "game_engine/TextRendering/text.h"

class Sprite;

class RenderSprite : public QObject
{
    Q_OBJECT
public:
    explicit RenderSprite(QObject *parent = 0);
    ~RenderSprite () {}

    void DrawImmediateModeVertex (Vector Position, Color color, Point uvs);
    void DrawSprite (Sprite* sprite);
    void DrawText (Text text);
signals:

public slots:

};

#endif // RENDER_H
