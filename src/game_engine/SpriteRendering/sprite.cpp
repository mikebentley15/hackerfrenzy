#include <QDebug>
#include <QSizeF>
#include "game_engine/SpriteRendering/sprite.h"



/*** =Public Methods= ***/

Sprite::Sprite(QObject *parent) :
    QObject(parent)
{
    _vertexPositions = new Vector[VertexAmount]();
    _vertexUVs = new Point[VertexAmount]();
    _vertexColors = new Color[VertexAmount] ();
    _changeable = true;

    initVertexPositions(Vector (0,0,0), 1, 1);
    setColor(Color(1,1,1,1));
    setUVs(Point(0,0),Point(1,1));

}

Sprite::~Sprite()
{
    if(_vertexPositions != NULL)
        delete[] _vertexPositions;
    if(_vertexColors != NULL)
        delete[] _vertexColors;
    if(_vertexUVs != NULL)
        delete[] _vertexUVs;
}

void Sprite::initVertexPositions(Vector position, double width, double height)
{
    double halfWidth = width/2;
    double halfHeight = height/2;

    // Clockwise creation of two triangles to make a quad.

    // TopLeft, TopRight, BottomLeft
    _vertexPositions[0] = Vector(position.X - halfWidth, position.Y + halfHeight, position.Z);
    _vertexPositions[1] = Vector(position.X + halfWidth, position.Y + halfHeight, position.Z);
    _vertexPositions[2] = Vector(position.X - halfWidth, position.Y - halfHeight, position.Z);

    // TopRight, BottomRight, BottomLeft
    _vertexPositions[3] = Vector(position.X + halfWidth, position.Y + halfHeight, position.Z);
    _vertexPositions[4] = Vector(position.X + halfWidth, position.Y - halfHeight, position.Z);
    _vertexPositions[5] = Vector(position.X - halfWidth, position.Y - halfHeight, position.Z);
}

void Sprite::setColor(Color color)
{
   for (int i = 0; i < this->VertexAmount; i++)
       _vertexColors[i] = color;
}

void Sprite::setTexture(Texture* texture)
{
    _texture = texture;
    initVertexPositions(getCenter(), _texture->getWidth(), _texture->getHeight());
}

void Sprite::setUVs(Point topLeft, Point bottomRight)
{
    // TopLeft, TopRight, BottomLeft
    _vertexUVs[0] = topLeft;
    _vertexUVs[1] = Point(bottomRight.X, topLeft.Y);
    _vertexUVs[2] = Point(topLeft.X, bottomRight.Y);

    // TopRight, BottomRight, BottomLeft
    _vertexUVs[3] = Point(bottomRight.X, topLeft.Y);
    _vertexUVs[4] = bottomRight;
    _vertexUVs[5] = Point(topLeft.X, bottomRight.Y);

}

void Sprite::setChangeability(bool status)
{
    _changeable = status;
}

bool Sprite::getChangeability()
{
    return _changeable;
}

Color Sprite::getColor()
{
    return *_vertexColors;
}

Vector Sprite::getPosition ()
{
   return *_vertexPositions;
}

double Sprite::getWidth()
{
    // topright - topleft
    return (_vertexPositions[1].X - _vertexPositions[0].X);
}

double Sprite::getHeight()
{
    // topleft - bottomleft
    return (_vertexPositions[0].Y - _vertexPositions[2].Y);
}

QSizeF Sprite::getSize()
{
    return QSizeF (this->getWidth(), this->getHeight());
}

Texture* Sprite::getTexture()
{
    return _texture;
}

Vector Sprite::getCenter()
{

    double halfWidth = getWidth () / 2;
    double halfHeight = getHeight () / 2;

    return Vector(_vertexPositions[0].X + halfWidth, _vertexPositions[0].Y - halfHeight, _vertexPositions[0].Z);
}

Vector Sprite::vertexPositons(int position)
{
    return _vertexPositions[position];
}

Color Sprite::vertexColors(int position)
{
    return _vertexColors[position];
}

Point Sprite::vertexUVs(int position)
{
    return _vertexUVs[position];
}



/*** =Public Slots= ***/

void Sprite::setPosition(double x, double y)
{
     setPosition(Vector(x, y, 0));
}

void Sprite::setPosition(Vector position)
{
    initVertexPositions(position, getWidth(), getHeight());
}

void Sprite::setPosition(QPointF position)
{
    setPosition(Vector(position.x(), position.y(), 0));
}

void Sprite::setWidth(double width)
{
    initVertexPositions(getCenter(), width, getHeight());
}

void Sprite::setHeight(double height)
{
    initVertexPositions(getCenter(), getWidth(), height);
}



