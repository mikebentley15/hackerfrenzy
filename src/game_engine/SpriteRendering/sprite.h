#ifndef SPRITE_H
#define SPRITE_H

#include <QObject>
#include <QPoint>
#include <QSizeF>
#include "game_engine/structures.h"
#include "game_engine/SpriteRendering/texture.h"


class Sprite : public QObject
{
    Q_OBJECT
private:
    Vector* _vertexPositions;
    Color* _vertexColors;
    Point* _vertexUVs;

    Texture* _texture;
    bool _changeable;

public:
    explicit Sprite(QObject *parent = 0);
    ~Sprite ();

    static const int VertexAmount = 6;

    void initVertexPositions (Vector position, double width, double height);
    void setColor (Color color);
    void setTexture (Texture* texture);
    void setUVs (Point topLeft, Point bottomRight);
    void setChangeability(bool status);

    bool getChangeability();
    double getWidth ();
    double getHeight ();
    QSizeF getSize ();

    Texture* getTexture ();
    Vector getCenter ();
    Color getColor ();
    Vector getPosition ();

    Color vertexColors (int arrayPosition);
    Point vertexUVs (int arrayPosition);
    Vector vertexPositons (int arrayPosition);
signals:

public slots:
    void setPosition (double x, double y);
    void setPosition (Vector position);
    void setPosition (QPointF position);
    void setHeight (double height);
    void setWidth (double width);

};

#endif // SPRITE_H
