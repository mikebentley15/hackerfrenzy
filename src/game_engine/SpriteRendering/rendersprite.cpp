#include <QtOpenGL>

#include "game_engine/SpriteRendering/rendersprite.h"
#include "game_engine/SpriteRendering/sprite.h"
#include "game_engine/TextRendering/charactersprite.h"
#include "game_engine/TextRendering/text.h"

RenderSprite::RenderSprite(QObject *parent) :
    QObject(parent)
{
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void RenderSprite::DrawImmediateModeVertex(Vector position, Color color, Point uvs)
{
    glColor4f(color.Red, color.Green, color.Blue, color.Alpha);
    glTexCoord2d(uvs.X, uvs.Y);
    glVertex3d(position.X, position.Y, position.Z);
}

void RenderSprite::DrawSprite(Sprite* sprite)
{
    glBindTexture(GL_TEXTURE_2D, sprite->getTexture()->getId());

    glBegin(GL_TRIANGLES);
    {
        for(int i = 0; i < sprite->VertexAmount; i++)
        {
            Point test = sprite->vertexUVs(i);

            DrawImmediateModeVertex
                (
                    sprite->vertexPositons(i),
                    sprite->vertexColors(i),
                    sprite->vertexUVs(i)
                );
        }
    }
    glEnd();

}

void RenderSprite::DrawText(Text text)
{
    foreach (CharacterSprite* c, text.characterSprites())
    {
        DrawSprite(c->getSprite());
    }
}
