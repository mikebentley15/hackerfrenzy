#include <QtOpenGL>
#include <QDebug>

#include "game_engine/SpriteRendering/texture.h"
#include "game_engine/SpriteRendering/texture_manager.h"

TextureManager::TextureManager()
{
    _textureDatabase = new QHash<QString, Texture*> ();

}

TextureManager::~TextureManager()
{
    foreach(const QString &key, _textureDatabase->keys())
        delete _textureDatabase->value(key);

    //glDeleteTextures(1, new int []{_textureDatabase->value(i)->getId()});

    delete _textureDatabase;
}

Texture* TextureManager::Get(const QString &textureId)
{
    return _textureDatabase->value(textureId);
}

void TextureManager::LoadTexture(const QString &textureId, GLuint textureAdd,
                                 const QImage &image)
{
    _textureDatabase->insert(textureId, 
                             new Texture((int)textureAdd,
                                         image.width(), image.height()));
}
