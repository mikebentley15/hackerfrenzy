#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H

#include <QtOpenGL>
#include <QHash>

class Texture;

class TextureManager
{
public:
    TextureManager();
    ~TextureManager();
    
    void LoadTexture (const QString &textureID, GLuint textureAdd,
                      const QImage &image);
    void Dispose ();
    
    Texture* Get(const QString &textureId);
    
private:
    
    QHash<QString, Texture*>* _textureDatabase;
    QHash<QString, Texture*>::iterator _hashIter;
    
};
#endif // TEXTURE_MANAGER_H
