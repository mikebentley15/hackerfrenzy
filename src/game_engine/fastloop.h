#ifndef FASTLOOP_H
#define FASTLOOP_H

#include "game_engine/fastloopbase.h"

#include <QObject>
#include <QTimer>
#include <QTime>


template<class Type>
class FastLoop : public FastLoopBase
{
public:
    FastLoop (Type* objectPtr, void (Type::*funcPtr) (double));
    ~FastLoop();
  
    virtual void callLoopOnce (double elapsedTime);
  
private:
    Type *_objectPtr;
    void (Type::*_funcPtr) (double);
};


#include "fastloop-inl.h"


#endif // FASTLOOP_H
