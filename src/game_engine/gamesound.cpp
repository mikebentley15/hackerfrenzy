#include "game_engine/gamesound.h"
#include "macros.h"

//#include <phonon/MediaObject>
//#include <phonon/AudioOutput>
//#include <phonon/MediaSource>
//#include <phonon/Path>


#ifdef USE_SOUNDS

GameSound::GameSound(QObject* parent) : QObject(parent)
{ }

GameSound::~GameSound()
{
    foreach (QString lookupName, _soundLookup.keys())
        delete _soundLookup.take(lookupName);
    foreach (QObject* outputStream, _outputStreams)
        delete outputStream;
    _outputStreams.clear ();
}

void GameSound::addSound(const QString &lookupName, const QString &filename)
{
    //Phonon::MediaObject* mediaObject = new Phonon::MediaObject();
    //mediaObject->setCurrentSource (Phonon::MediaSource(filename));
    //_soundLookup.insert (lookupName, mediaObject);
}

void GameSound::playRepeatingBackgroundMusic(const QString &lookupName)
{
    //Phonon::MediaObject* mediaObject = _soundLookup.value (lookupName, NULL);
    //if (mediaObject == NULL) return;

    //Phonon::AudioOutput* audioOutput = new Phonon::AudioOutput(Phonon::MusicCategory);
    //Phonon::Path path = Phonon::createPath(mediaObject, audioOutput);
    //_outputStreams.insert (audioOutput);

    //mediaObject->play ();
    // TODO: connect it so that it repeats when it's finished
}

void GameSound::stopRepeatingBackgroundMusic()
{
    // TODO: stop the background music -- implement
    // TODO: add a variable to show what the background music is.
    // TODO: delete the audioOutput object associated with it
}

void GameSound::playSoundEffect(const QString &lookupName)
{
    Q_UNUSED (lookupName);
    // TODO: play sound effect -- implement
    // TODO: connect a slot that deletes the audioOutput object afterwards.
}

#else // !defined(USE_SOUNDS)

GameSound::GameSound (QObject *parent) : QObject(parent) { }
GameSound::~GameSound () { }
void GameSound::addSound (const QString &lookupName, const QString &filename)
    { Q_UNUSED (lookupName); Q_UNUSED (filename); }
void GameSound::playRepeatingBackgroundMusic (const QString &lookupName)
    { Q_UNUSED (lookupName); }
void GameSound::stopRepeatingBackgroundMusic () { }
void GameSound::playSoundEffect (const QString &lookupName)
    { Q_UNUSED(lookupName); }

#endif // defined(USE_SOUNDS)
