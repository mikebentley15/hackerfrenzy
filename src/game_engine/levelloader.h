#ifndef LEVELLOADER_H
#define LEVELLOADER_H

#include "graph/computer.h"
#include "state/state.h"

#include <QObject>
#include <QHash>

class Attacker;
class ChangeMapForm;
class Font;
class FontParser;
class GameSound;
class Graph;
class mapReader;
class RenderSprite;
class Sprite;
class StateSystem;
class Text;
class Texture;
class TextureManager;

class LevelLoader : public State
{
    Q_OBJECT

    struct DeviceSelection
    {
        int _deviceIdx;
        Sprite* _sprite;
        qreal _timeSelected;

        // Creates an unselected device selection
        DeviceSelection ()
        {
            markUnselected ();
        }

        // Checks to see if this item represents an unselected device
        bool isUnselected ()
        {
            return _sprite == NULL || _deviceIdx == -1;
        }

        void markUnselected ()
        {
            _sprite = NULL;
            _deviceIdx = -1;
            _timeSelected = 0;
        }

        bool operator == (const DeviceSelection &other)
        {
            return _deviceIdx == other._deviceIdx &&
                   _sprite == other._sprite;
        }

        bool operator != (const DeviceSelection &other)
        {
            return !(*this == other);
        }
    };

public:
    LevelLoader(int map, StateSystem* system, TextureManager* textureManager, GameSound *soundCreator);
    ~LevelLoader();

    /** Virtual Methods from Base **/
    void Update (double elapsedTime);
    void Render ();
    QString uploadTexture (QString type);
    void updateSprites (Action event, Sprite *sprite);

    int getLevelCount() const;

public slots:
    // Destroys the old stuff and loads the map recreating all of the structures
    void loadMap (int levelNumber);
    void loadMap (const QString &mapFileName);

    void resetMap ();

    void activateState ();
    void deactivateState ();

    void attackerSent (Attacker* attacker);
    void updateAttackerPosition (Attacker* attacker);
    void updateAttackerDirection (Attacker* attacker);
    void attackerReachedDestination (Attacker* attacker);

    void initiateAttack (Computer *from, Computer *to);
    void registerAttack (Attack* attack);
    void attackIsDone ();

    void unmarkHoveredOverDevice ();
    void markHoveredOverDevice (int deviceIdx);
    void markHoveredOverDevice (QPoint gridLocation);

    void unselectDevice (int deviceIdx);
    void unselectDevice (QPoint gridLocation);
    void unselectAllDevices ();
    void selectDevice (int deviceIdx);
    void selectDevice (QPoint gridLocation);
    void deviceControllerChanged (Computer* device);
    void setDeviceStrengthLabel (int deviceIdx);

    void playerLost (Character* thisCharacter);

protected:
    void resizeEvent (QResizeEvent *event);
    void mousePressEvent (QMouseEvent *);
    void mouseReleaseEvent (QMouseEvent *event);
    void mouseDoubleClickEvent (QMouseEvent *event);
    void keyPressEvent (QKeyEvent *event);

signals:
    void timeUpdate (int elapsedMillis);

private:
    ChangeMapForm* _mapForm;
    int _currentLevel;   // if it's not a level then = -1
    QString _currentMap; // if it's not a level then = ""

    QVector<Computer*> _devices;
    Sprite** _deviceSprites;
    Sprite** _connectorSprites;
    int _playerCount;
    QHash<QPair<Computer*,Computer*>, Attack*> _attackTracker;
    QHash<Attacker*, Sprite*> _attackerTracker;
    DeviceSelection _hoveredOverDevice;
    DeviceSelection _lastClickedDevice;
    QHash<int, DeviceSelection>* _selectedDevices;

    mapReader* _loadMap;
    Graph* _level;
    bool _pause;

    int _deviceCount;
    int _connectorCount;
    Text** _deviceCurrentStrengthTexts;

    Text* _pausedText;
    Text* _gamewinner;
    Text* _returnInstructions;

    bool _gameover;

    Font* _font;
    FontParser* _fontParser;

    void setupDevices ();
    void setupDeviceCurrentStrengthTexts ();
    void setupAIs ();

    QString getSpriteKeyValue (int pos);
    QString getSpriteKeyValue (int x, int y);

    void adjustViewport ();
    void positionDevices ();
    void positionDeviceCurrentStrengthTexts ();
    void positionConnectors ();

    void renderConnectors ();
    void renderDevices ();
    void renderAttackers ();
    void renderPause ();
    void renderGameOver ();

    QPointF fromGlCoordinatesToGrid (QPointF point);
    QPoint  fromGlCoordinatesToGrid (QPoint  point);
    QPointF fromGridToGlCoordinates (QPointF point);
    QPoint  fromGridToGlCoordinates (QPoint  point);

    bool isConnectedToASelectedDevice (int deviceIdx);

    // Returns true if the device belongs to the user who is playing the game
    bool belongsToThisPlayer (int deviceIdx);
    bool belongsToThisPlayer (Computer* device);
    bool belongsToANeutralPlayer (int deviceIdx);
    bool belongsToANeutralPlayer (Computer* device);

    // these are simply helper functions for the mousePressEvent() method
    void leftClickMouseEvent       (QMouseEvent *event);
    void rightClickMouseEvent      (QMouseEvent *event);
    void shiftLeftClickMouseEvent  (QMouseEvent *event);
    void shiftRightClickMouseEvent (QMouseEvent *event);
    void ctrlLeftClickMouseEvent   (QMouseEvent *event);
    void ctrlRightClickMouseEvent  (QMouseEvent *event);

    void cleanupMemory ();
    void init ();
};



#endif // LEVELLOADER_H


