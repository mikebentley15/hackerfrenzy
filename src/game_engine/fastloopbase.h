#ifndef FASTLOOPBASE_H
#define FASTLOOPBASE_H

#include <QObject>
#include <QTimer>
#include <QTime>

class PreciseTimer;

/**
 * An abstract class that is the base for a fast looping class
 * such as is needed for a game engine.
 * 
 * A derived class needs to implement:
 *   public virtual void callFunction (int elapsedTime)
 */
class FastLoopBase : public QObject
{
    Q_OBJECT
  
public:
    FastLoopBase ();
    ~FastLoopBase ();
    bool isActive ();
    virtual void callLoopOnce (double elapsedTime) = 0;
  
public slots:
    void start ();
    void stop ();

protected slots:
    void loopFunction ();

protected:
    QTimer *_timer;
    PreciseTimer *_watch2;
    QTime  *_watch;
    long _totalElapsedTime;
};

#endif // FASTLOOPBASE_H
