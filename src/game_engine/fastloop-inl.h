#ifndef FASTLOOP_INL_H
#define FASTLOOP_INL_H

#include "audit.h"

/*** =Public Methods= ***/

template<class Type>
FastLoop<Type>::FastLoop (Type* objectPtr, void (Type::*funcPtr) (double))
{
    _objectPtr = objectPtr;
    _funcPtr = funcPtr;
    Audit::get()->objectCreated ("FastLoop");
}

template<class Type>
FastLoop<Type>::~FastLoop ()
{
    Audit::get()->objectDestroyed ("FastLoop");
}

template<class Type>
void FastLoop<Type>::callLoopOnce (double elapsedTime)
{
    (_objectPtr->*_funcPtr) (elapsedTime);
}


#endif

