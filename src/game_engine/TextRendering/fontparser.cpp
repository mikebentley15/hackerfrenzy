#include "game_engine/TextRendering/fontparser.h"

#include <QDebug>
#include <QFile>
#include <QStringList>


FontParser::FontParser()
{
}

FontParser::~FontParser()
{
}

int FontParser::getValue(QString s)
{
    QString value = s.mid(s.indexOf('=')+1);
    return value.toInt();
}

QHash<QChar, CharacterData> FontParser::Parse(QString filepath)
{
    QFile file(filepath);

    if(!file.open(QIODevice::ReadOnly))
        qDebug () << "Couldn't open font file.";

    QVector<QString> linesInFile;

    while(!file.atEnd())
        linesInFile.push_back(file.readLine());

    for (int i = HEADER_SIZE; i < linesInFile.size(); i++)
    {
        QString currentLine = linesInFile.at(i);

        QStringList parsedLine = currentLine.split(" ", QString::SkipEmptyParts);

        CharacterData tempCharData;

        tempCharData.setId      (getValue(parsedLine[1]));
        tempCharData.setX       (getValue(parsedLine[2]));
        tempCharData.setY       (getValue(parsedLine[3]));
        tempCharData.setWidth   (getValue(parsedLine[4]));
        tempCharData.setHeight  (getValue(parsedLine[5]));
        tempCharData.setXOffset (getValue(parsedLine[6]));
        tempCharData.setYOffset (getValue(parsedLine[7]));
        tempCharData.setXAdvance(getValue(parsedLine[8]));

        QChar test(tempCharData.getId());

        _charDictionary.insert(test, tempCharData);
    }

    return _charDictionary;

}
