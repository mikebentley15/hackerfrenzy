#ifndef FONTPARSER_H
#define FONTPARSER_H

#include <QHash>
#include "game_engine/TextRendering/characterdata.h"

//class CharacterData;

class FontParser
{
public:
    FontParser();
    ~FontParser();

    const static int HEADER_SIZE = 4;
    QHash<QChar, CharacterData> Parse (QString filepath);

private:
    static int getValue (QString s);

    QHash<QChar, CharacterData> _charDictionary;
};

#endif // FONTPARSER_H
