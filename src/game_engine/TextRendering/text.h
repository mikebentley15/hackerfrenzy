#ifndef TEXT_H
#define TEXT_H

#include <QList>
#include <QString>
#include <QPointF>
#include <QSizeF>

#include "game_engine/structures.h"
#include "game_engine/TextRendering/charactersprite.h"
#include "game_engine/TextRendering/font.h"

class Text
{
public:
    Text(const QString &text, const Font &font);
    ~Text();

    QPointF pos () const;
    void setPos (double x, double y);
    void setPos (const QPointF &point);

    qreal width () const;
    qreal height () const;
    QSizeF size () const;

    QList<CharacterSprite *> characterSprites() const;

    QString text () const;
    void setText (const QString &text);

    Font font () const;
    void setFont (const Font &font);

    void setScale (double scale);
    void setColor (Color color);

private:
    QList<CharacterSprite*> _bitmapText;
    Font _font;
    QString _text;
    QPointF _pos;
    QSizeF _size;
    double _scale;
    Color _color;
    void recalibrateText ();
};

#endif // TEXT_H
