#include "game_engine/TextRendering/text.h"

#include "game_engine/TextRendering/characterdata.h"
#include "game_engine/SpriteRendering/sprite.h"


/*** =Public Methods= ***/

Text::Text(const QString &text, const Font &font)
{
    _text = text;
    _font = font;
    _scale = 1;
    _color = Color(1,1,1,1);

    setPos (0, 0);
}

Text::~Text()
{
    for (int i = 0; i < _bitmapText.size(); i++)
    {
        delete _bitmapText.at(i);
    }
    _bitmapText.clear();
}

QPointF Text::pos() const
{
    return _pos;
}

void Text::setPos (double x, double y)
{
    return setPos (QPointF (x, y));
}

void Text::setPos(const QPointF &point)
{
    _pos = point;
    recalibrateText ();
}

qreal Text::width() const
{
    return _size.width ();
}

qreal Text::height() const
{
    return _size.height ();
}

QSizeF Text::size() const
{
    return _size;
}

QList<CharacterSprite*> Text::characterSprites() const
{
    return _bitmapText;
}

QString Text::text() const
{
    return _text;
}

void Text::setText(const QString &text)
{
    _text = text;
    recalibrateText ();
}

Font Text::font() const
{
    return _font;
}

void Text::setFont(const Font &font)
{
    _font = font;
    recalibrateText ();
}

void Text::setScale (double scale)
{
    _scale = scale;
    recalibrateText ();
}

void Text::setColor (Color color)
{
    _color = color;
    recalibrateText ();
}

/*** =Private Methods= ***/

void Text::recalibrateText()
{
    for (int i = 0; i < _bitmapText.size(); i++)
    {
        delete _bitmapText.at(i);
    }
    _bitmapText.clear();

    double currentX = _pos.x ();
    double currentY = _pos.y ();
    qreal width = 0;
    qreal height = 0;

    // Horizontally center the image
    const QHash <QChar, CharacterData>* charDataHash = _font.characterData ();
    foreach (QChar c, _text)
        currentX -= charDataHash->value(c).getXAdvance () / (2 * _scale);

    // Create one sprite for each letter.
    foreach(QChar c, _text)
    {
        CharacterSprite* tempSprite = _font.createSprite(c);

        float xOffset = (float)tempSprite->getCharData()->getXOffset()/2;
        float yOffset = (float)tempSprite->getCharData()->getYOffset()/2;

        tempSprite->getSprite()->setWidth(tempSprite->getSprite()->getWidth() * _scale);
        tempSprite->getSprite()->setHeight(tempSprite->getSprite()->getHeight() * _scale);

        tempSprite->getSprite()->setPosition(currentX + xOffset, currentY - yOffset);
        currentX += tempSprite->getCharData()->getXAdvance() * _scale;

        width    += tempSprite->getCharData()->getXAdvance();
        height    = tempSprite->getSprite ()->getHeight ();

        tempSprite->getSprite()->setColor(_color);

        _bitmapText.append(tempSprite);
    }

    _size.setWidth (width);
    _size.setHeight (height);
}



