#ifndef CHARACTERSPRITE_H
#define CHARACTERSPRITE_H

#include "game_engine/TextRendering/characterdata.h"

class Sprite;

class CharacterSprite
{
public:
    CharacterSprite ();
    explicit CharacterSprite(Sprite* sprite, CharacterData* charData);
    ~CharacterSprite ();

    CharacterData* getCharData ();
    Sprite* getSprite ();

    void setCharData (CharacterData* data);
    void setSprite (Sprite* sprite);

    void copySprite(Sprite* sprite);

private:
    CharacterData _charData;
    Sprite* _sprite;
};

#endif // CHARACTERSPRITE_H
