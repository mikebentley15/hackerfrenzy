#include "game_engine/TextRendering/characterdata.h"

CharacterData::CharacterData()
{
}

CharacterData& CharacterData::operator= (const CharacterData &right)
{
    if(this == &right)
        return *this;

    //Nothing to clean up in memory

    _id = right.getId();
    _x = right.getX();
    _y = right.getY();
    _width = right.getWidth();
    _height = right.getHeight();
    _xOffset = right.getXOffset();
    _yOffset = right.getYOffset();
    _xAdvance = right.getXAdvance();

    return *this;
}

//Not sure if we'll ever need this but I threw it in just in case.
bool CharacterData::operator == (const CharacterData &right)
{
    if(this == &right)
        return true;

    if(_id == right.getId() &&
       _x  == right.getX() &&
       _y  == right.getY() &&
       _width  == right.getWidth() &&
       _height == right.getHeight() &&
       _xOffset == right.getXOffset() &&
       _yOffset == right.getYOffset() &&
       _xAdvance == right.getXAdvance())
        return true;

    return false;
}

void CharacterData::setId (int id)
{
    _id = id;
}

void CharacterData::setX (int x)
{
    _x = x;
}

void CharacterData::setY (int y)
{
    _y = y;
}

void CharacterData::setWidth (int width)
{
    _width = width;
}

void CharacterData::setHeight (int height)
{
    _height = height;
}

void CharacterData::setXOffset (int xOffset)
{
    _xOffset = xOffset;
}

void CharacterData::setYOffset (int yOffset)
{
    _yOffset = yOffset;
}

void CharacterData::setXAdvance (int xAdvance)
{
    _xAdvance = xAdvance;
}

int CharacterData::getId () const
{
    return _id;
}

int CharacterData::getX () const
{
    return _x;
}

int CharacterData::getY () const
{
    return _y;
}

int CharacterData::getWidth () const
{
    return _width;
}

int CharacterData::getHeight () const
{
    return _height;
}

int CharacterData::getXOffset () const
{
    return _xOffset;
}

int CharacterData::getYOffset () const
{
    return _yOffset;
}

int CharacterData::getXAdvance () const
{
    return _xAdvance;
}
