#include "game_engine/TextRendering/charactersprite.h"

#include "game_engine/SpriteRendering/sprite.h"
#include "game_engine/TextRendering/characterdata.h"

CharacterSprite::CharacterSprite ()
{
    _sprite = new Sprite ();
}

CharacterSprite::CharacterSprite(Sprite* sprite, CharacterData* charData)
{
    _charData = *charData;

    _sprite = new Sprite ();

    copySprite(sprite);
}

CharacterSprite::~CharacterSprite ()
{
    //delete _sprite;
}

void CharacterSprite::copySprite (Sprite * sprite)
{
    //We have to have this class since Sprite is an
    //QObject and won't allow copy constructors.
    _sprite->setChangeability(sprite->getChangeability());

    _sprite->setColor(sprite->getColor());
    _sprite->setTexture(sprite->getTexture());


    _sprite->setHeight(sprite->getHeight());
    _sprite->setWidth(sprite->getWidth());
    _sprite->setPosition(sprite->getPosition());
    _sprite->setUVs(sprite->vertexUVs(0), sprite->vertexUVs(4));
}

CharacterData* CharacterSprite::getCharData ()
{
    return &_charData;
}

Sprite* CharacterSprite::getSprite ()
{
    return _sprite;
}

void CharacterSprite::setCharData (CharacterData* data)
{
    _charData = *data;
}

void CharacterSprite::setSprite (Sprite* sprite)
{
    copySprite(sprite);
}


