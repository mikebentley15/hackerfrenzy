#ifndef FONT_H
#define FONT_H

#include <QChar>
#include <QHash>

#include "game_engine/SpriteRendering/texture.h"
#include "game_engine/TextRendering/characterdata.h"
#include "game_engine/TextRendering/charactersprite.h"

class Sprite;

class Font
{
public:
    explicit Font ();
    explicit Font(Texture texture, QHash<QChar, CharacterData> characterData);
    ~Font ();

    CharacterSprite* createSprite (QChar c);
    const QHash<QChar, CharacterData>* characterData () const;

    Font& operator= (const Font &right);

private:
    Texture _texture;
    Sprite* sprite;
    QHash<QChar, CharacterData> _characterData;
};

#endif // FONT_H
