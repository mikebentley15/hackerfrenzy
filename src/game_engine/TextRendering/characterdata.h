#ifndef CHARACTERDATA_H
#define CHARACTERDATA_H

class CharacterData
{
public:
    CharacterData();

    CharacterData& operator= (const CharacterData &right);
    bool operator == (const CharacterData &right);

    void setId (int id);
    void setX (int x);
    void setY (int y);
    void setWidth (int width);
    void setHeight (int height);
    void setXOffset (int xOffset);
    void setYOffset (int yOffset);
    void setXAdvance (int xAdvance);

    int getId () const;
    int getX () const;
    int getY () const;
    int getWidth () const;
    int getHeight () const;
    int getXOffset () const;
    int getYOffset () const;
    int getXAdvance () const;

private:
    int _id;
    int _x, _y;
    int _width, _height;
    int _xOffset, _yOffset;
    int _xAdvance;
};

#endif // CHARACTERDATA_H
