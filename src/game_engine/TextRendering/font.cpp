#include "game_engine/TextRendering/font.h"
#include "game_engine/SpriteRendering/sprite.h"

Font::Font ()
{
    sprite = new Sprite();
}

Font::Font(Texture texture, QHash<QChar, CharacterData> characterData)
{
    _texture = texture;
    _characterData = characterData;
}

Font::~Font()
{
}

Font& Font::operator= (const Font &right)
{
    if(this == &right)
        return *this;

    //Nothing to clean up in memory yet

    this->_texture = right._texture;
    this->_characterData = right._characterData;

    return *this;
}

CharacterSprite* Font::createSprite(QChar c)
{
    CharacterData charData = _characterData.value(c);

    //Setup UVs
    Point topLeft = Point((float)charData.getX()/(float)_texture.getWidth(),
                          (float)charData.getY()/(float)_texture.getHeight());
    Point bottomRight = Point(topLeft.X + ((float)charData.getWidth()/(float)_texture.getWidth()),
                              topLeft.Y + ((float)charData.getHeight()/(float)_texture.getHeight()));

    sprite->setTexture (&_texture);
    sprite->setChangeability (true);
    sprite->setPosition (0, 0);
    sprite->setUVs (topLeft, bottomRight);
    sprite->setWidth (charData.getWidth());
    sprite->setHeight (charData.getHeight());
    sprite->setColor (Color(1, 1, 1, 1));

    return new CharacterSprite(sprite, &charData);
}

const QHash<QChar, CharacterData> *Font::characterData() const
{
    return &_characterData;
}
