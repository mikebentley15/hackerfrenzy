#include "game_engine/levelloader.h"

#include <QtOpenGL>
#include <QDebug>
#include <QSignalMapper>
#include <cstdio>

#include "attack.h"
#include "attacker/attacker.h"
#include "audit.h"
#include "character/ai.h"
#include "character/humanplayer.h"
#include "character/neutralplayer.h"
#include "forms/changemapform.h"
#include "game_engine/gamesound.h"
#include "game_engine/SpriteRendering/rendersprite.h"
#include "game_engine/SpriteRendering/sprite.h"
#include "game_engine/SpriteRendering/texture.h"
#include "game_engine/SpriteRendering/texture_manager.h"
#include "game_engine/statesystem.h"
#include "game_engine/structures.h"
#include "game_engine/TextRendering/font.h"
#include "game_engine/TextRendering/fontparser.h"
#include "game_engine/TextRendering/text.h"
#include "graph/computer.h"
#include "graph/mapreader.h"
#include "graph/node.h"
#include "state/splashscreenstate.h"



/*** =Public Methods= ***/

LevelLoader::LevelLoader(int map, StateSystem* system, TextureManager* textureManager, GameSound* soundCreator)
    : State ()
{
    Audit::get()->objectCreated ("LevelLoader");
    _pause = false;
    _system = system;
    _textureManager = textureManager;
    _renderSprite = new RenderSprite ();
    _soundCreator = soundCreator;
    _currentLevel = map;
    _currentMap = "";

    _loadMap = new mapReader ();
    _level = _loadMap->loadMap(_currentLevel);

    QHash <QChar, CharacterData> temp;
    _fontParser = new FontParser ();
    temp = _fontParser->Parse (":/images/Font/font.txt");

    _font = new Font (*_textureManager->Get ("fonttexture1"),
                      _fontParser->Parse (":/images/Font/font.txt"));

    _pausedText = new Text("Paused", *_font);
    _gamewinner = new Text("You Won", *_font);
    _returnInstructions = new Text("To return press Esc", *_font);
    _returnInstructions->setPos (-170, -70);
    _returnInstructions->setScale (1.5);
    _returnInstructions->setColor (Color(1,0,0,1));

    init ();
}

LevelLoader::~LevelLoader ()
{
    Audit::get()->objectDestroyed ("LevelLoader");
    cleanupMemory ();

    delete _loadMap;
    delete _renderSprite;
    delete _pausedText;
    delete _gamewinner;
    delete _returnInstructions;
}

void LevelLoader::Update (double elapsedTime)
{
    if(!_pause)
    {
        _elapsedTime = elapsedTime;

        //Used to see if the mouse is over a sprite
        for(int i = 0; i < _deviceCount; i++ )
        {
            QString key = getSpriteKeyValue(i);
            if(mouseOnSprite(key))
            {
                if (_hoveredOverDevice._deviceIdx == i)
                    _hoveredOverDevice._timeSelected += elapsedTime;
                else
                    markHoveredOverDevice (i);
            }
            else if (_hoveredOverDevice._deviceIdx == i)
                unmarkHoveredOverDevice ();
        }

        // Update the time selected to all of the selected devices
        foreach (int key, _selectedDevices->keys ())
            (*_selectedDevices)[key]._timeSelected += elapsedTime;

        // Signal any objects that update themselves
        emit timeUpdate (static_cast<int>(elapsedTime * 1000));
    }
}

void LevelLoader::Render ()
{
    glClearColor (0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    renderBackground();
    renderConnectors();
    renderAttackers();
    renderDevices();

    if(_pause)
        renderPause ();

    if(_gameover)
        renderGameOver ();

    glFinish();
}



QString LevelLoader::uploadTexture (QString type)
{
    if (type == "\\/")
        return "connecttexture4";

    else if(type == "/\\")
        return "connecttexture3";

    else if(type == "\\\\")
        return "connecttexture2";

    else if(type == "//")
        return "connecttexture1";

    else if(type == "==")
        return "connecttexture5";

    else if(type == "||")
        return "connecttexture6";

    else if(type.at(0) == 'D')
        return "devicetexture2";

    else if (type.at(0) == 'L')
        return "devicetexture3";

    else if (type.at(0) == 'S')
        return "devicetexture4";

    else if (type.at(0) == 'R')
        return "devicetexture5";

    else if (type.at(0) == 'T')
        return "devicetexture6";

    else if (type.at(0) == 'M')
        return "devicetexture7";

    else
        return "devicetexture1";
}

void LevelLoader::updateSprites (Action event, Sprite* sprite)
{

    if (event == MouseEnter)
    {
        // nothing for now
    }

    else if (event == MouseClick)
    {
        sprite->setHeight(70);
        sprite->setWidth(70);
    }

    else if (event == MouseExit)
    {
        // nothing for now
    }

}

int LevelLoader::getLevelCount() const
{
    return _loadMap->MAP_LIST.size ();
}



/*** =Public Slots= ***/

void LevelLoader::loadMap(int levelNumber)
{
    cleanupMemory ();

    delete _loadMap;
    _loadMap = new mapReader ();

    _currentLevel = levelNumber;
    _currentMap = "";
    _level = _loadMap->loadMap(_currentLevel);

    init ();

    QResizeEvent* event = new QResizeEvent(QSize(0,0), windowSize ());
    resizeEvent (event);
    delete event;
}

void LevelLoader::loadMap(const QString &mapFileName)
{
    cleanupMemory ();

    delete _loadMap;
    _loadMap = new mapReader ();

    _currentLevel = -1;
    _currentMap = mapFileName;
    _level = _loadMap->loadMap (_currentMap);

    init ();

    QResizeEvent* event = new QResizeEvent(QSize(0,0), windowSize());
    resizeEvent (event);
    delete event;
}

void LevelLoader::resetMap()
{
    cleanupMemory ();

    delete _loadMap;
    _loadMap = new mapReader ();

    if (_currentLevel == -1)
        _level = _loadMap->loadMap (_currentMap);
    else
        _level = _loadMap->loadMap (_currentLevel);

    init ();

    QResizeEvent* event = new QResizeEvent(QSize(0,0), windowSize ());
    resizeEvent (event);
    delete event;
}

void LevelLoader::activateState ()
{
    _mapForm = new ChangeMapForm(this, _loadMap->MAP_LIST.size ());
    _mapForm->show();
}

void LevelLoader::deactivateState ()
{
    _mapForm->close ();
    _mapForm->deleteLater ();
    _mapForm = NULL;

    _soundCreator->stopRepeatingBackgroundMusic ();
}

void LevelLoader::attackerSent(Attacker *attacker)
{
    attacker->setParent (this);

    Sprite* attackerSprite = new Sprite(attacker);
    attackerSprite->setPosition (fromGridToGlCoordinates (attacker->pos()));
    attackerSprite->setTexture (_textureManager->Get ("attacktexture1"));
    attackerSprite->setHeight(12);
    attackerSprite->setWidth(12);
    attackerSprite->setColor(attacker->owner ()->getColor ());

    _attackerTracker.insert (attacker, attackerSprite);

    // Create connections
    connect (attacker, SIGNAL(posChanged(Attacker*)),
             this, SLOT(updateAttackerPosition(Attacker*)));
    connect (attacker, SIGNAL(directionChanged(Attacker*)),
             this, SLOT(updateAttackerDirection(Attacker*)));
    connect (attacker, SIGNAL(reachedDestination(Attacker*)),
             this, SLOT(attackerReachedDestination(Attacker*)));
    connect (this, SIGNAL(timeUpdate(int)),
             attacker, SLOT(updateTime(int)));
}

void LevelLoader::updateAttackerPosition (Attacker *attacker)
{
    Sprite* attackerSprite = _attackerTracker.value (attacker, NULL);
    if (attackerSprite != NULL)
        attackerSprite->setPosition(fromGridToGlCoordinates(attacker->pos()));
}

void LevelLoader::updateAttackerDirection (Attacker* attacker)
{
    Q_UNUSED (attacker);
    // Nothing yet
}

void LevelLoader::attackerReachedDestination (Attacker *attacker)
{

    // This is available in all editors.

    // Let the computer handle the attacker effects
    attacker->destination()->attackerReachedMe (attacker);

    // remove it from the tracker
    _attackerTracker.remove(attacker);

    // Disconnect everything between the attacker and this object
    disconnect (attacker, 0, this, 0);
    disconnect (this, 0, attacker, 0);

    // Delete the attacker and its sprite
    // Note: since the attacker is the sprite's parent, the sprite will be
    //       deleted when the attacker is deleted.
    attacker->deleteLater();
}

void LevelLoader::initiateAttack(Computer *from, Computer *to)
{
    // If the two computers aren't directly connected, then don't
    // initiate an attack (as that doesn't make sense).
    if (!from->isConnectedTo (to))
        return;

    // Retrieve the attack if it already exists
    Attack* attack = _attackTracker.value(
                QPair<Computer*, Computer*>(from, to), NULL);

    // If it doesn't already exist then create it
    if (attack == NULL)
    {
        // Create the attack
        attack = new Attack(from, to, _level);
        attack->setStrength (from->getAvailableStrength () / 2);
        attack->setStrengthPerAttacker (3);
        attack->setAttackersPerSecond (15);
        attack->setAttackerSpeedFactor (3);

        registerAttack (attack);
    }
    else
    {
        attack->increaseStrength (from->getAvailableStrength () / 2);
    }
}

void LevelLoader::registerAttack (Attack* attack)
{
    Computer* from = attack->from();
    Computer* to   = attack->to();

    // Register it
    from->registerAttack (attack);
    _attackTracker.insert (QPair<Computer*, Computer*>(from, to), attack);

    // Setup connections
    connect (attack, SIGNAL(attackIsDone()),
             this,   SLOT(attackIsDone()));
    connect (attack, SIGNAL(sendAttacker(Attacker*)),
             this,   SLOT(attackerSent(Attacker*)));
    connect (this,   SIGNAL(timeUpdate(int)),
             attack, SLOT(updateTime(int)));
}

void LevelLoader::attackIsDone()
{
}

void LevelLoader::unmarkHoveredOverDevice()
{
    // Update the sprite (if any)
    if (_hoveredOverDevice.isUnselected ())
        updateSprites (MouseExit, _hoveredOverDevice._sprite);

    // No device is hovered over now
    _hoveredOverDevice.markUnselected ();
}

void LevelLoader::markHoveredOverDevice(int deviceIdx)
{
    QString key = getSpriteKeyValue (deviceIdx);

    // Mark the device as selected
    _hoveredOverDevice._deviceIdx = deviceIdx;
    _hoveredOverDevice._sprite = _spriteMap.value (key);
    _hoveredOverDevice._timeSelected = 0;

    // Update the sprite
    updateSprites (MouseEnter, _hoveredOverDevice._sprite);
}

void LevelLoader::markHoveredOverDevice(QPoint gridLocation)
{
    int deviceIdx = _level->deviceLocations()->indexOf (gridLocation);
    markHoveredOverDevice (deviceIdx);
}

void LevelLoader::unselectDevice(int deviceIdx)
{
    // Remove the selection from the set of devices
    DeviceSelection selection = _selectedDevices->take (deviceIdx);

    // undo the effects of the selection
    if (!selection.isUnselected ())
    {
        selection._sprite->setHeight (56);
        selection._sprite->setWidth (56);
    }


}

void LevelLoader::unselectDevice(QPoint gridLocation)
{
    int deviceIdx = _level->deviceLocations()->indexOf (gridLocation);
    selectDevice (deviceIdx);
}

void LevelLoader::unselectAllDevices()
{
    QList<int> indexes = _selectedDevices->keys ();
    foreach (int deviceIdx, indexes)
        unselectDevice (deviceIdx);
}

void LevelLoader::selectDevice(int deviceIdx)
{
    QString key = getSpriteKeyValue (deviceIdx);

    DeviceSelection selection;

    // Mark the device as selected
    selection._deviceIdx = deviceIdx;
    selection._sprite = _spriteMap.value (key);
    selection._timeSelected = 0;
    _selectedDevices->insert (deviceIdx, selection);

    _soundCreator->playSoundEffect ("click");

    // Update the sprite
    updateSprites (MouseClick, selection._sprite);
}

void LevelLoader::selectDevice(QPoint gridLocation)
{
    int deviceIdx = _level->deviceLocations()->indexOf (gridLocation);
    selectDevice (deviceIdx);
}

void LevelLoader::deviceControllerChanged(Computer *device)
{
    int deviceIdx = _level->deviceLocations()->indexOf(device->getParentNode()->getPos ());
    _deviceSprites[deviceIdx]->setColor(device->getController()->getColor ());

    // Unpause if it's not a neutral.  Pause if it is a neutral.
    if (!belongsToANeutralPlayer (device))
        device->unpause ();
    else
        device->pause ();

    // If the current computer is selected, then unselect it
    if (_selectedDevices->contains (deviceIdx))
        unselectDevice (deviceIdx);
}

void LevelLoader::setDeviceStrengthLabel (int deviceIdx)
{
    int textSize = QString::number(_devices[deviceIdx]->getCapacity ()).size();
    char* currentStrengthText = new char[textSize * 2 + 2];
    char* formatString = new char[10];
    sprintf (formatString, "%%%dd/%%%dd", textSize, textSize);
    sprintf (currentStrengthText, formatString,
             _devices[deviceIdx]->getCurrentStrength (),
             _devices[deviceIdx]->getCapacity ());

    _deviceCurrentStrengthTexts[deviceIdx]->setScale(0.6);
    _deviceCurrentStrengthTexts[deviceIdx]->setText (currentStrengthText);
}

void LevelLoader::playerLost(Character *thisCharacter)
{
    _playerCount--;
    if (_playerCount == 1)
    {
        // Find out who won.
        Character* playerWhoWon;
        foreach (Character* player, *_level->getPlayers ())
        {
            // if the player has not lost and is not a neutral player
            if (player->computersControlled () > 0 &&
                qobject_cast<NeutralPlayer*>(player) == NULL)
            {
                playerWhoWon = player;
                break;
            }
        }

        // If the player is a human, then display You Won
        if (qobject_cast<HumanPlayer*>(playerWhoWon) != NULL)
            _gamewinner->setText ("You Won!!");
        // Otherwise, display You Lost, You Loser!
        else
            _gamewinner->setText ("You Lost!!");

        _gameover = true;
    }

    // Disconnect all signals
    disconnect (thisCharacter, 0, this, 0);
    disconnect (this, 0, thisCharacter, 0);

}



/*** =Protected Methods= ***/

void LevelLoader::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED (event);
    adjustViewport ();
    positionDevices ();
    positionDeviceCurrentStrengthTexts ();
    positionConnectors ();
}

void LevelLoader::mousePressEvent (QMouseEvent *event)
{
    // remember the last device that was clicked on.
    _lastClickedDevice = _hoveredOverDevice;

    if (event->modifiers ().testFlag (Qt::ShiftModifier))
    {
        if (event->button () == Qt::LeftButton)
            shiftLeftClickMouseEvent (event);
        else if (event->button () == Qt::RightButton)
            shiftRightClickMouseEvent (event);
    }
    else if (event->modifiers ().testFlag (Qt::ControlModifier))
    {
        if (event->button () == Qt::LeftButton)
            ctrlLeftClickMouseEvent (event);
        else if (event->button() == Qt::RightButton)
            ctrlRightClickMouseEvent (event);
    }
    else if (event->button () == Qt::LeftButton)
        leftClickMouseEvent (event);
    else if (event->button () == Qt::RightButton)
        rightClickMouseEvent (event);
}

void LevelLoader::mouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED (event);

    if (!_hoveredOverDevice.isUnselected () &&
        _hoveredOverDevice != _lastClickedDevice)
    {
        foreach (int deviceIdx, _selectedDevices->keys())
        {
            initiateAttack (_devices[deviceIdx],
                            _devices[_hoveredOverDevice._deviceIdx]);
        }

        unselectAllDevices ();
    }
}

void LevelLoader::mouseDoubleClickEvent(QMouseEvent *event)
{
    Q_UNUSED (event);

    // If the double click was not on a device
    if (_hoveredOverDevice.isUnselected ())
    {
        return;
    }

    // If the double click was on a player's device
    if (belongsToThisPlayer (_hoveredOverDevice._deviceIdx))
    {
        for (int i = 0; i < _deviceCount; i++)
            if (belongsToThisPlayer (i))
                selectDevice (i);
        return;
    }

    // If the double click was on an opponent device
    if (!belongsToThisPlayer (_hoveredOverDevice._deviceIdx))
    {
        // select all
        for (int i = 0; i < _deviceCount; i++)
            if (belongsToThisPlayer (i))
                selectDevice (i);

        // start the attack
        Computer* toAttack = _devices[_hoveredOverDevice._deviceIdx];
        foreach (int deviceIdx, _selectedDevices->keys())
            initiateAttack (_devices[deviceIdx], toAttack);

        // unselect all
        unselectAllDevices ();

        return;
    }
}

void LevelLoader::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Escape)
    {
        _system->changeState ("level_select");
    }

    else if (event->key() == Qt::Key_P || event->key() == Qt::Key_Space)
    {
        _pause = !_pause;
    }
}




/*** =Private Methods= ***/

void LevelLoader::setupDevices()
{
    // Populate the devices array so that the deviceIdx works with this.
    _devices.resize (_deviceCount);
    QSignalMapper* signalMapper = new QSignalMapper (this);
    for (int deviceIdx = 0; deviceIdx < _deviceCount; deviceIdx++)
    {
        QPoint p = _level->deviceLocations()->at (deviceIdx);
        _devices[deviceIdx] = _level->at(p)->getComp ();
        _deviceSprites[deviceIdx]->setColor(
                    _devices[deviceIdx]->getController ()->getColor ());

        connect (this,                    SIGNAL(timeUpdate(int)),
                 _devices[deviceIdx], SLOT(updateTime(int)));
        connect (_devices[deviceIdx], SIGNAL(controllerChanged(Computer*)),
                 this,                    SLOT(deviceControllerChanged(Computer*)));

        // If the device is controlled by a neutral player, then pause it.
        if (belongsToANeutralPlayer(deviceIdx))
        {
            _devices[deviceIdx]->pause();
        }

        // map the signals to send the device index rather than the new
        // strength
        signalMapper->setMapping (_devices[deviceIdx], deviceIdx);
        connect (_devices[deviceIdx], SIGNAL(strengthChanged(int)),
                 signalMapper,            SLOT(map()));
    }

    // Connect the mapper to this object
    connect (signalMapper, SIGNAL(mapped(int)),
             this,          SLOT(setDeviceStrengthLabel(int)));
}

void LevelLoader::setupDeviceCurrentStrengthTexts ()
{
    _deviceCurrentStrengthTexts = new Text*[_deviceCount] ();
    for (int i = 0; i < _deviceCount; ++i)
    {
        _deviceCurrentStrengthTexts[i] = new Text ("", *_font);
        setDeviceStrengthLabel (i);
    }
}

void LevelLoader::setupAIs()
{
    foreach (Character* player, *_level->getPlayers ())
    {
        // If the player is an AI
        if (qobject_cast<AI*>(player) != NULL)
        {
            AI* ai = qobject_cast<AI*>(player);
            connect (ai, SIGNAL(initiateAttack(Computer*, Computer*)),
                     this, SLOT(initiateAttack(Computer*, Computer*)));
            connect (this, SIGNAL(timeUpdate(int)),
                     ai, SLOT(updateTime(int)));
        }
    }
}

QString LevelLoader::getSpriteKeyValue (int pos)
{
    if(pos > _level->getNumberOfDevices())
        return NULL;

    QString x, y;
    QPoint location = _level->deviceLocations()->at(pos);
    QString key = _level->at(location)->getType()
            + x.setNum(location.x()) + "," + y.setNum(location.y());

    return key;
}

QString LevelLoader::getSpriteKeyValue (int x, int y)
{
    //Check to make sure an error hasn't occured and the click algorithm
    //has set the x position incorrectly if so return NULL for now
    if(x >= _level->getSize().width() || x < 0)
        return NULL;

    //Check the y position like we did for x
    if(y >= _level->getSize().height() || y < 0)
        return NULL;

    QString temp = _level->at(x,y)->getType();

    //check to see if you've click on a wall
    if (_level->at(x,y)->getType() == "00")
        return NULL;

    //if you made it to here then the user has clicked on an item
    QString xString, yString;
    QString key = _level->at(x, y)->getType() + xString.setNum(x) + "," + yString.setNum(y);

    return key;
}

void LevelLoader::adjustViewport()
{
    _viewportAdjustor.setX((double)windowSize().width()/_level->getSize().width());
    _viewportAdjustor.setY((double)windowSize().height()/_level->getSize().height());
}

void LevelLoader::positionDevices()
{
    QVector<QPoint>* locations = _level->deviceLocations ();
    for (int i = 0; i < locations->size(); i++)
    {
        QString key = QString(_level->at(locations->at(i))->getType() + "%1,%2")
                .arg(locations->at(i).x())
                .arg(locations->at(i).y());
        Sprite* toPosition = getSprite(key);

        toPosition->setPosition (fromGridToGlCoordinates (locations->at(i)));
    }
}

void LevelLoader::positionDeviceCurrentStrengthTexts()
{
    for (int i = 0; i < _deviceCount; ++i)
    {
        _deviceCurrentStrengthTexts[i]
                ->setPos (_deviceSprites[i]->getCenter().X + _deviceSprites[i]->getWidth()*1.4,
                          _deviceSprites[i]->getCenter().Y + _deviceSprites[i]->getHeight()/2);
    }
}

void LevelLoader::positionConnectors()
{
    QVector<QPoint>* locations = _level->connectorLocations ();
    for (int i = 0; i < locations->size(); i++)
    {
        QString key = QString(_level->at(locations->at(i))->getType() + "%1,%2")
                .arg(locations->at(i).x()).arg(locations->at(i).y());
        Sprite* toPosition = getSprite(key);

        toPosition->setPosition (fromGridToGlCoordinates (locations->at(i)));
    }
}

void LevelLoader::renderPause ()
{
    _pausedText->setScale(6);
    _pausedText->setColor(Color(1,0,0,1));
    _pausedText->setPos(QPoint(-200,0));
    _renderSprite->DrawText(*_pausedText);
}

void LevelLoader::renderConnectors()
{
    if(_pause)
        for (int i = 0; i < _connectorCount; i++)
        {
            _connectorSprites[i]->setColor(Color(1,1,1,0.3));
            _renderSprite->DrawSprite (_connectorSprites[i]);
        }
    else
        for (int i = 0; i < _connectorCount; i++)
        {
            _connectorSprites[i]->setColor(Color(1,1,1,1));
            _renderSprite->DrawSprite (_connectorSprites[i]);
        }
}

void LevelLoader::renderDevices()
{
    if(_pause)
        for (int i = 0; i < _deviceCount; i++)
        {
            Color temp = _deviceSprites[i]->getColor();
            Color newColor (temp.Red, temp.Green, temp.Blue, 0.3);
            _deviceSprites[i]->setColor(newColor);
            _deviceCurrentStrengthTexts[i]->setColor(Color(1,1,1,0.3));

            _renderSprite->DrawSprite (_deviceSprites[i]);
            _renderSprite->DrawText (*_deviceCurrentStrengthTexts[i]);
        }
    else
        for (int i = 0; i < _deviceCount; i++)
        {
            Color temp = _deviceSprites[i]->getColor();
            Color newColor (temp.Red, temp.Green, temp.Blue, 1);
            _deviceSprites[i]->setColor(newColor);
            _deviceCurrentStrengthTexts[i]->setColor(Color(1,1,1,1));

            _renderSprite->DrawSprite (_deviceSprites[i]);
            _renderSprite->DrawText (*_deviceCurrentStrengthTexts[i]);
        }
}

void LevelLoader::renderGameOver ()
{
    _gamewinner->setScale (4);
    _gamewinner->setColor (Color(1,0.8,0.19,1));
    _gamewinner->setPos (QPointF(-270, 0));
    _renderSprite->DrawText (*_gamewinner);

    _renderSprite->DrawText (*_returnInstructions);
}

void LevelLoader::renderAttackers()
{
    QList<Sprite*> attackerSprites = _attackerTracker.values();

    if(_pause)
        for (int i = 0; i < attackerSprites.size(); ++i)
        {
            Color temp = attackerSprites[i]->getColor();
            Color newColor (temp.Red, temp.Green, temp.Blue, 0.3);
            attackerSprites[i]->setColor(newColor);

            _renderSprite->DrawSprite (attackerSprites.at(i));
        }

    else
        for (int i = 0; i < attackerSprites.size(); ++i)
        {
            Color temp = attackerSprites[i]->getColor();
            Color newColor (temp.Red, temp.Green, temp.Blue, 1);
            attackerSprites[i]->setColor(newColor);

            _renderSprite->DrawSprite (attackerSprites.at(i));
        }
}

QPointF LevelLoader::fromGlCoordinatesToGrid (QPointF point)
{
    qreal x = ((double)windowSize().width()/2.2 + point.x()) / _viewportAdjustor.x();
    qreal y = ((double)windowSize().height()/2.5 - point.y()) / _viewportAdjustor.y();
    return QPointF (x, y);
}

QPoint LevelLoader::fromGlCoordinatesToGrid (QPoint point)
{
    return fromGlCoordinatesToGrid(QPointF (point)).toPoint();
}

QPointF LevelLoader::fromGridToGlCoordinates (QPointF point)
{
    qreal x = _viewportAdjustor.x() * (point.x() - _level->getSize().width() / 2.0 + 1 / 2.0);
    qreal y = _viewportAdjustor.y() * (_level->getSize().height() / 2.0 - point.y() - 1 / 2.0);
    return QPointF (x, y);
}

QPoint LevelLoader::fromGridToGlCoordinates (QPoint point)
{
    QPointF toReturn = fromGridToGlCoordinates(QPointF (point));
    return QPoint(qFloor(toReturn.x()), qFloor(toReturn.y()));
}

bool LevelLoader::isConnectedToASelectedDevice(int deviceIdx)
{
    foreach (int idx, _selectedDevices->keys())
        if (_devices[idx]->isConnectedTo (_devices[deviceIdx]))
            return true;

    return false;
}

bool LevelLoader::belongsToThisPlayer(int deviceIdx)
{
    return belongsToThisPlayer (_devices[deviceIdx]);
}

bool LevelLoader::belongsToThisPlayer (Computer *device)
{
    HumanPlayer* player = qobject_cast<HumanPlayer*> (device->getController ());
    if (player != NULL)
    {
        // TODO: check which player it is
        return true;
    }
    else
        return false;
}

bool LevelLoader::belongsToANeutralPlayer (int deviceIdx)
{
    return belongsToANeutralPlayer (_devices[deviceIdx]);
}

bool LevelLoader::belongsToANeutralPlayer (Computer *device)
{
    return qobject_cast<NeutralPlayer*> (device->getController());
}

void LevelLoader::leftClickMouseEvent(QMouseEvent *event)
{
    Q_UNUSED (event);

    // if the click was not on a device
    if (_hoveredOverDevice.isUnselected ())
    {
        unselectAllDevices ();
        return;
    }

    // If the clicked device is connected to one of the selected computers
    if (isConnectedToASelectedDevice (_hoveredOverDevice._deviceIdx))
    {
        // initiate an attack from each of the selected computers
        // Note that only direct connections will actually cause an attack
        Computer *toAttack = _devices[_hoveredOverDevice._deviceIdx];
        foreach (int deviceIdx, _selectedDevices->keys())
            initiateAttack (_devices[deviceIdx], toAttack);

        // Unselect all selected devices after the attack initiates
        unselectAllDevices ();

        return;
    }

    // If the clicked device is owned by the player
    if (belongsToThisPlayer (_hoveredOverDevice._deviceIdx))
    {
        unselectAllDevices ();
        selectDevice (_hoveredOverDevice._deviceIdx);
        return;
    }

    // if the clicked device is not owned by the player
    if (!belongsToThisPlayer (_hoveredOverDevice._deviceIdx))
    {
        unselectAllDevices ();
        return;
    }

    qWarning () << "leftClickMouseEvent should not get to this point";
}

void LevelLoader::rightClickMouseEvent(QMouseEvent *event)
{
    Q_UNUSED (event);

    // TODO: do something with the right click
}

void LevelLoader::shiftLeftClickMouseEvent(QMouseEvent *event)
{
    Q_UNUSED (event);

    // if the hovered over device is not selected and belongs to the player,
    // then select it
    if (!_hoveredOverDevice.isUnselected () &&
        !_selectedDevices->contains (_hoveredOverDevice._deviceIdx) &&
        belongsToThisPlayer (_hoveredOverDevice._deviceIdx))
    {
        selectDevice (_hoveredOverDevice._deviceIdx);
        return;
    }
}

void LevelLoader::shiftRightClickMouseEvent(QMouseEvent *event)
{
    Q_UNUSED (event);

    // TODO: do something with shift right-click
}

void LevelLoader::ctrlLeftClickMouseEvent(QMouseEvent *event)
{
    Q_UNUSED (event);

    // if the hovered over device is not selected and belongs to the player
    if (!_hoveredOverDevice.isUnselected () &&
        !_selectedDevices->contains (_hoveredOverDevice._deviceIdx) &&
        belongsToThisPlayer (_hoveredOverDevice._deviceIdx))
    {
        selectDevice (_hoveredOverDevice._deviceIdx);
        return;
    }

    // if the hovered over device is already selected
    if (_selectedDevices->contains (_hoveredOverDevice._deviceIdx))
    {
        unselectDevice (_hoveredOverDevice._deviceIdx);
        return;
    }
}

void LevelLoader::ctrlRightClickMouseEvent(QMouseEvent *event)
{
    Q_UNUSED (event);

    // TODO: do something with ctrl right-click
}

void LevelLoader::cleanupMemory()
{
    for (int i = 0; i < _deviceCount; i++)
    {
        _deviceSprites[i]->deleteLater ();
        _deviceSprites[i] = NULL;
        disconnect (this,        SIGNAL(timeUpdate(int)),
                    _devices[i], SLOT(updateTime(int)));
        disconnect (_devices[i], SIGNAL(controllerChanged(Computer*)),
                    this,        SLOT(deviceControllerChanged(Computer*)));
    }
    for (int i = 0; i < _connectorCount; i++)
    {
        _connectorSprites[i]->deleteLater ();
        _connectorSprites[i] = NULL;
    }
    foreach (Attack* attack, _attackTracker.values())
    {
        attack->deleteLater ();
    }
    foreach (Attacker* attacker, _attackerTracker.keys())
    {
        attacker->deleteLater ();
    }
    foreach (Character* player, *_level->getPlayers ())
    {
        // If the player is an AI
        if (qobject_cast<AI*>(player) != NULL)
        {
            AI* ai = qobject_cast<AI*>(player);
            disconnect (ai, SIGNAL(initiateAttack(Computer*, Computer*)),
                        this, SLOT(initiateAttack(Computer*, Computer*)));
            disconnect (this, SIGNAL(timeUpdate(int)),
                        ai, SLOT(updateTime(int)));
        }
    }

    delete _selectedDevices;      _selectedDevices = NULL;
    _background->deleteLater ();  _background = NULL;
    delete[] _deviceSprites;      _deviceSprites = NULL;
    delete[] _connectorSprites;   _connectorSprites = NULL;
    delete _selectedDevices;      _selectedDevices = NULL;

    _attackTracker.clear ();
    _attackerTracker.clear ();
    _spriteMap.clear ();
    _devices.clear ();

    _tempSprite = NULL;
    _hoveredOverDevice.markUnselected ();
    _elapsedTime = 0;
    _totalTime = 0;
    _deviceCount = 0;
    _connectorCount = 0;
}

void LevelLoader::init()
{
     _gameover = false;

    // Nothing is selected to start out with.
    _hoveredOverDevice.markUnselected ();
    _selectedDevices = new QHash<int, DeviceSelection> ();


    _background = new Sprite ();
    _background->setTexture(_textureManager->Get("backgroundtexture2"));

    _deviceCount = _level->getNumberOfDevices();
    _deviceSprites = new Sprite* [_deviceCount];
    _connectorCount = _level->getNumberOfConnectors();
    _connectorSprites = new Sprite*[_connectorCount];

    _tempSprite = NULL;

    createSprites (_deviceCount, _deviceSprites, "device", _level);
    createSprites (_connectorCount, _connectorSprites, "connector", _level);

    setupDevices ();
    setupDeviceCurrentStrengthTexts ();
    setupAIs ();

    _playerCount = _level->getPlayers ()->size ();
    foreach (Character* player, *_level->getPlayers ())
    {
        // if the player is a neutral player
        if (qobject_cast<NeutralPlayer*>(player) != NULL)
        {
            _playerCount--; // don't count the neutral players in the player count
        }
        else
        {
            connect (player, SIGNAL(lostAllComputers(Character*)),
                     this,   SLOT(playerLost(Character*)));
        }
    }
}



