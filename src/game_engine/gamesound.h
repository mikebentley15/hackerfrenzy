#ifndef GAMESOUND_H
#define GAMESOUND_H

#include <QObject>
#include <QHash>
#include <QSet>

class GameSound : public QObject
{
    Q_OBJECT

public:
    GameSound (QObject *parent = 0);
    ~GameSound ();

    void addSound (const QString &lookupName, const QString &filename);
    void playRepeatingBackgroundMusic (const QString &lookupName);
    void stopRepeatingBackgroundMusic ();
    void playSoundEffect (const QString &lookupName);

private:
    QHash<QString, QObject*> _soundLookup;
    QSet<QObject*> _outputStreams;
};

#endif // GAMESOUND_H
