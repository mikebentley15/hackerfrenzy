#include "audit.h"
#include "game_engine/statesystem.h"
#include "state/state.h"

#include <QMouseEvent>

/*** =Public Methods= ***/

StateSystem::StateSystem(QWidget *parent) : QObject (parent)
{
    Audit::get()->objectCreated ("StateSystem");
    _stateStore = new QHash<QString, State*> ();
    _currentState = NULL;
    _parentWidget = parent;
}

StateSystem::~StateSystem()
{
    Audit::get()->objectDestroyed ("StateSystem");

    // deactivate the current state
    if (_currentState != NULL)
    {
        _currentState->deactivateState();
        _currentState = NULL;
    }

    foreach(const QString &key, _stateStore->keys())
        delete _stateStore->value(key);

    delete _stateStore;
}

void StateSystem::render()
{
    if(_currentState == NULL)
        return;

    _currentState->Render();
}

bool StateSystem::exists(const QString &stateId) const
{
    QHash<QString, State*>::const_iterator hashIter
        = _stateStore->find(stateId);
    
    if (hashIter != _stateStore->end())
        return true;
    
    return false;
}

QString StateSystem::getCurrentState () const
{
    if (_currentState == NULL)
        return "";
    else
        return _stateStore->key (_currentState);
}

State* StateSystem::getCurrentStateObject () const
{
    return _currentState;
}

void StateSystem::addState(const QString &stateId, State* state)
{
    if(stateId != "" && state != NULL)
    {
        _stateStore->insert(stateId, state);

        // Log the add into the state system
        Audit::get()->log (tr("State added: ") + stateId + "\n");
    }
}

// Deactivates the active state since the owning widget was closed
void StateSystem::closeEvent            (QCloseEvent* event)
{
    if (_currentState != NULL)
    {
        _currentState->closeEvent (event);
        _currentState->deactivateState ();
        _currentState = NULL;
    }
}

void StateSystem::dragEnterEvent        (QDragEnterEvent* event)
{
    if (_currentState != NULL)
        _currentState->dragEnterEvent (event);
}

void StateSystem::dragLeaveEvent        (QDragLeaveEvent* event)
{
    if (_currentState != NULL)
        _currentState->dragLeaveEvent (event);
}

void StateSystem::dragMoveEvent         (QDragMoveEvent* event)
{
    if (_currentState != NULL)
        _currentState->dragMoveEvent (event);
}

void StateSystem::dropEvent             (QDropEvent* event)
{
    if (_currentState != NULL)
        _currentState->dropEvent (event);
}

void StateSystem::enterEvent            (QEvent* event)
{
    if (_currentState != NULL)
        _currentState->enterEvent (event);
}

void StateSystem::keyPressEvent         (QKeyEvent* event)
{
    if (_currentState != NULL)
        _currentState->keyPressEvent (event);
}

void StateSystem::keyReleaseEvent       (QKeyEvent* event)
{
    if (_currentState != NULL)
        _currentState->keyReleaseEvent (event);
}

void StateSystem::leaveEvent            (QEvent* event)
{
    if (_currentState != NULL)
        _currentState->leaveEvent (event);
}

void StateSystem::mouseDoubleClickEvent (QMouseEvent* event)
{
    if (_currentState != NULL)
        _currentState->mouseDoubleClickEvent (event);
}

void StateSystem::mouseMoveEvent        (QMouseEvent* event)
{
    if (_currentState != NULL)
    {
        // Set the position of the mouse before sending the event
        _currentState->setMousePosition(event->pos());

        _currentState->mouseMoveEvent(event);
    }
}

void StateSystem::mousePressEvent       (QMouseEvent* event)
{
    if (_currentState != NULL)
        _currentState->mousePressEvent (event);
}

void StateSystem::mouseReleaseEvent     (QMouseEvent* event)
{
    if (_currentState != NULL)
        _currentState->mouseReleaseEvent (event);
}

void StateSystem::resizeEvent           (QResizeEvent *event)
{
    _windowSize = event->size();

    if (_currentState != NULL)
    {
        // Set the window size in the State before sending the event
        _currentState->setWindowSize(_windowSize);

        _currentState->resizeEvent (event);
    }
}

void StateSystem::tabletEvent           (QTabletEvent* event)
{
    if (_currentState != NULL)
        _currentState->tabletEvent (event);
}

void StateSystem::wheelEvent            (QWheelEvent* event)
{
    if (_currentState != NULL)
        _currentState->wheelEvent (event);
}


/*** =Public Slots= ***/

void StateSystem::changeState(const QString &stateId)
{
    if(exists(stateId))
    {
        State* requestedState = _stateStore->value(stateId);
        if (_currentState != requestedState)
        {
            // Log the state change.
            Audit::get()->log(tr("Changed to state ") + stateId + "\n");

            State* previousState = _currentState;

            // change the current state
            _currentState = requestedState;
            emit stateChanged (stateId);

            // set the current window size in the new state
            QResizeEvent myEvent(_windowSize, QSize(0,0));
            this->resizeEvent (&myEvent);

            if (previousState != NULL)
            {
                // Move over the current mouse position from the
                // previous state to the new state
                _currentState->setMousePosition (
                            previousState->mousePosition());

                // deactivate the previous state
                previousState->deactivateState();
            }

            // activate the new state
            _currentState->activateState();
        }
    }
}

void StateSystem::update(double elapsedTime)
{
    if(_currentState == NULL)
        return;
    
    _currentState->Update(elapsedTime);
}

