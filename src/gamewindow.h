#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <QtOpenGL/QGLWidget>

class GameSound;
class StateSystem;
class Texture;
class TextureManager;
template<class Type> class FastLoop;

class GameWindow : public QGLWidget
{
    Q_OBJECT

public:
    GameWindow(QWidget *parent = 0);
    ~GameWindow();

    void gameLoop (double elapsedTime);

signals:

public slots:

protected:
    // These are events from QWidget that will be forwarded to _stateSystem
    // which is forwarded to the current active state for processing
    virtual void closeEvent            (QCloseEvent* event);
    virtual void dragEnterEvent        (QDragEnterEvent* event);
    virtual void dragLeaveEvent        (QDragLeaveEvent* event);
    virtual void dragMoveEvent         (QDragMoveEvent* event);
    virtual void dropEvent             (QDropEvent* event);
    virtual void enterEvent            (QEvent* event);
    virtual void keyPressEvent         (QKeyEvent* event);
    virtual void keyReleaseEvent       (QKeyEvent* event);
    virtual void leaveEvent            (QEvent* event);
    virtual void mouseDoubleClickEvent (QMouseEvent* event);
    virtual void mouseMoveEvent        (QMouseEvent* event);
    virtual void mousePressEvent       (QMouseEvent* event);
    virtual void mouseReleaseEvent     (QMouseEvent* event);
    virtual void resizeEvent           (QResizeEvent* event);
    virtual void tabletEvent           (QTabletEvent* event);
    virtual void wheelEvent            (QWheelEvent* event);


private:

    void (*my_funct_ptr)(void*, int);
    StateSystem* _system;
    Texture* testTexture;
    TextureManager* _textureManager;
    FastLoop<GameWindow>* _fastLoop;
    QSize _windowDeminsions;
    GameSound* _soundCreator;


    void initializeGL();
    void paintGL();
    void Setup2DGraphics (int width, int height);
    //void RenderScene ();t
    void FlipImage (QImage &image);
    void LoadStates();
    void LoadTextures ();
    void loadTextureDirectory (const QString &directory, int count,
                               const QString &baseLookupName);
    void loadSounds ();
};


#endif // GAMEWINDOW_H
