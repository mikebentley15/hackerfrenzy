#include "forms/changemapform.h"
#include "game_engine/levelloader.h"
#include "macros.h"
#include "ui_changemapform.h"


#ifdef CHANGE_MAP_FORM  // if we are to show the change map form window...

ChangeMapForm::ChangeMapForm(LevelLoader *creator, int mapCount)
    : QWidget(), ui(new Ui::ChangeMapForm)
{
    _creator = creator;
    ui->setupUi(this);

    for (int i = 0; i < mapCount; i++)
        ui->comboBox->addItem (QString::number (i));

    connect (ui->apply_button, SIGNAL(released()),
             this, SLOT(applyMapChoice()));
    connect (ui->reset_button, SIGNAL(released()),
             _creator, SLOT(resetMap()));
}

ChangeMapForm::~ChangeMapForm()
{
    delete ui;
}

void ChangeMapForm::applyMapChoice()
{
    _creator->loadMap (ui->comboBox->currentIndex ());
}

#else // !defined(CHANGE_MAP_FORM)


ChangeMapForm::ChangeMapForm(LevelLoader *creator, int mapCount) : QWidget()
{ Q_UNUSED(creator); Q_UNUSED(mapCount); }
ChangeMapForm::~ChangeMapForm       () { }
void ChangeMapForm::show            () { }
void ChangeMapForm::close           () { }
void ChangeMapForm::applyMapChoice  () { }


#endif // defined(CHANGE_MAP_FORM)

