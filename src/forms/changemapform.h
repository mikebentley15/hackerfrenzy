#ifndef CHANGEMAPFORM_H
#define CHANGEMAPFORM_H

#include <QWidget>
#include "macros.h"

class LevelLoader;

namespace Ui {
class ChangeMapForm;
}

class ChangeMapForm : public QWidget
{
    Q_OBJECT
    
public:
    explicit ChangeMapForm(LevelLoader* creator, int mapCount);
    ~ChangeMapForm();

#ifndef CHANGE_MAP_FORM  // If we are not to show the change map form window,
public slots:            // then override the show() and close() methods.
    void show ();
    void close ();
#endif

private:
    LevelLoader *_creator;
    Ui::ChangeMapForm *ui;

private slots:
    void applyMapChoice ();
};

#endif // CHANGEMAPFORM_H
