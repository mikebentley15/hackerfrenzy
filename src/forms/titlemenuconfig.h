#ifndef TITLEMENUCONFIG_H
#define TITLEMENUCONFIG_H

#include <QDialog>

namespace Ui {
class TitleMenuConfig;
}

class TitleMenuConfig : public QDialog
{
    Q_OBJECT
    
public:
    explicit TitleMenuConfig(QWidget *parent = 0);
    ~TitleMenuConfig();
    
public slots:
    void setTitleX    (int newX);
    void setPlayX     (int newX);
    void setTutorialX (int newX);
    void setQuitX     (int newX);
    void setTitleY    (int newY);
    void setPlayY     (int newY);
    void setTutorialY (int newY);
    void setQuitY     (int newY);

#ifndef TITLE_CONFIG_MENU // if we are not to show the title menu
    void show ();         // then override the show and close methods
    void close ();
#endif

signals:
    void titleXChanged    (int newX = 0);
    void playXChanged     (int newX = 0);
    void tutorialXChanged (int newX = 0);
    void quitXChanged     (int newX = 0);
    void titleYChanged    (int newY = 0);
    void playYChanged     (int newY = 0);
    void tutorialYChanged (int newY = 0);
    void quitYChanged     (int newY = 0);

private:
    Ui::TitleMenuConfig *ui;
};


#endif // TITLEMENUCONFIG_H
