
#include "forms/titlemenuconfig.h"
#include "ui_titlemenuconfig.h"
#include "macros.h"

#ifdef TITLE_MENU_CONFIG  // if we are to show the title menu config window
/*** =Public Methods= ***/

TitleMenuConfig::TitleMenuConfig(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TitleMenuConfig)
{
    ui->setupUi(this);

    // Exit button
    connect (ui->_exitButton,    SIGNAL(clicked()),
             this,               SLOT(close()));

    // Setup the x ones first, then y

    // LCD Displays
    connect (ui->_xSliderTitle,    SIGNAL(valueChanged(int)),
             ui->_xTitleDisplay,     SLOT(display(int)));
    connect (ui->_xSliderPlay,     SIGNAL(valueChanged(int)),
             ui->_xPlayDisplay,      SLOT(display(int)));
    connect (ui->_xSliderTutorial, SIGNAL(valueChanged(int)),
             ui->_xTutorialDisplay,  SLOT(display(int)));
    connect (ui->_xSliderQuit,     SIGNAL(valueChanged(int)),
             ui->_xQuitDisplay,      SLOT(display(int)));
    // Signals
    connect (ui->_xSliderTitle,    SIGNAL(valueChanged(int)),
             this,                 SIGNAL(titleXChanged(int)));
    connect (ui->_xSliderPlay,     SIGNAL(valueChanged(int)),
             this,                 SIGNAL(playXChanged(int)));
    connect (ui->_xSliderTutorial, SIGNAL(valueChanged(int)),
             this,                 SIGNAL(tutorialXChanged(int)));
    connect (ui->_xSliderQuit,     SIGNAL(valueChanged(int)),
             this,                 SIGNAL(quitXChanged(int)));



    // Setup the y ones now

    // LCD Displays
    connect (ui->_ySliderTitle,    SIGNAL(valueChanged(int)),
             ui->_yTitleDisplay,     SLOT(display(int)));
    connect (ui->_ySliderPlay,     SIGNAL(valueChanged(int)),
             ui->_yPlayDisplay,      SLOT(display(int)));
    connect (ui->_ySliderTutorial, SIGNAL(valueChanged(int)),
             ui->_yTutorialDisplay,  SLOT(display(int)));
    connect (ui->_ySliderQuit,     SIGNAL(valueChanged(int)),
             ui->_yQuitDisplay,      SLOT(display(int)));
    // Signals
    connect (ui->_ySliderTitle,    SIGNAL(valueChanged(int)),
             this,                 SIGNAL(titleYChanged(int)));
    connect (ui->_ySliderPlay,     SIGNAL(valueChanged(int)),
             this,                 SIGNAL(playYChanged(int)));
    connect (ui->_ySliderTutorial, SIGNAL(valueChanged(int)),
             this,                 SIGNAL(tutorialYChanged(int)));
    connect (ui->_ySliderQuit,     SIGNAL(valueChanged(int)),
             this,                 SIGNAL(quitYChanged(int)));
}

TitleMenuConfig::~TitleMenuConfig()
{
    delete ui;
}



/*** =Public Slots= ***/

void TitleMenuConfig::setTitleX    (int newX)
{
    ui->_xSliderTitle->setValue(newX);
}

void TitleMenuConfig::setPlayX     (int newX)
{
    ui->_xSliderPlay->setValue(newX);
}

void TitleMenuConfig::setTutorialX (int newX)
{
    ui->_xSliderTutorial->setValue(newX);
}

void TitleMenuConfig::setQuitX     (int newX)
{
    ui->_xSliderQuit->setValue(newX);
}

void TitleMenuConfig::setTitleY    (int newY)
{
    ui->_ySliderTitle->setValue(newY);
}

void TitleMenuConfig::setPlayY     (int newY)
{
    ui->_ySliderPlay->setValue(newY);
}

void TitleMenuConfig::setTutorialY (int newY)
{
    ui->_ySliderTutorial->setValue(newY);
}

void TitleMenuConfig::setQuitY     (int newY)
{
    ui->_ySliderQuit->setValue(newY);
}

#else // !define(TITLE_MENU_CONFIG)

// If we aren't to show the title menu config window, then simply make it so
// that none of the methods do anything.

TitleMenuConfig::TitleMenuConfig(QWidget *parent) : QDialog(parent) { }
TitleMenuConfig::~TitleMenuConfig() { }
void TitleMenuConfig::setTitleX    (int newX)  { Q_UNUSED (newX); }
void TitleMenuConfig::setPlayX     (int newX)  { Q_UNUSED (newX); }
void TitleMenuConfig::setTutorialX (int newX)  { Q_UNUSED (newX); }
void TitleMenuConfig::setQuitX     (int newX)  { Q_UNUSED (newX); }
void TitleMenuConfig::setTitleY    (int newY)  { Q_UNUSED (newY); }
void TitleMenuConfig::setPlayY     (int newY)  { Q_UNUSED (newY); }
void TitleMenuConfig::setTutorialY (int newY)  { Q_UNUSED (newY); }
void TitleMenuConfig::setQuitY     (int newY)  { Q_UNUSED (newY); }
void TitleMenuConfig::show ()  { }
void TitleMenuConfig::close () { }

#endif // define(TITLE_MENU_CONFIG)
