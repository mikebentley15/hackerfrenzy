#ifndef AUDIT_H
#define AUDIT_H

#include "macros.h"

#include <QFile>
#include <QHash>
#include <QString>
#include <QTime>
#include <QTextEdit>

/**
 * Performs logs and counts of resources and activities.
 * 
 * This class follows the singleton pattern where the constructor
 * is private and there is a creator method that will call the
 * constructor only the first time it is called.
 * 
 * Will also keep itself as one of the resources that need to be
 * freed.
 */
class Audit : private QObject
{
    Q_OBJECT

public:
    /**
     * Destructor
     * 
     * If you destroy this singleton Audit object, the next getAudit() call
     * will return a new Audit object as the new singleton.
     * 
     * The user is responsible to delete the Audit object when done with it.
     */
    ~Audit ();
  
    /**
     * Creates the singleton object if it hasn't yet, otherwise returns
     * the pointer to it.
     * 
     * If you delete the singleton object and then call this method, it
     * will create a new one for you to use with defaulted values again.
     */
    static Audit* get ();
  
    /** Returns the total object count in existance not including this object */
    int getTotalObjectCount () const;
  
    /**
     * Keeps a count of total objects created and destroyed as well as
     * an individual count of each type of object.
     * 
     * These will write into the ".audit_log" file describing the state
     * of the program at the time of creation or deletion.
     */
    virtual void objectCreated (const QString &type);
    virtual void objectDestroyed (const QString &type);
  
    /**
     * Will count the object creation and deletion for any QObject.
     * 
     * This method alone is functionally equivalent to calling
     * objectCreated() when the QObject is created and objectDestroyed
     * when the object is deleted.
     */
    virtual void registerQObject (QObject *object);
  
    /** Logs the message to the log file .audit_log */
    void log (const QString &message) const;
  
protected slots:
    virtual void registerQObjectAsDestroyed (QObject *object);
  
protected:
    static QString nameOfQObjectClass (QObject *object);
  
private:
    static Audit* _theSingletonObject;
  
    QFile *_logFile;
    int _totalObjectCount;
    QHash<QString, int> *_objectTracker;
    QHash<QObject*, QString> *_qObjectNames;
    QTextEdit* _logWindow;
  
    Audit ();
    void startLog ();
  
    DISALLOW_COPY_AND_ASSIGN (Audit);
};

#endif // AUDIT_H
