#ifndef ATTACK_H
#define ATTACK_H

#include <QObject>

class Attacker;
class Computer;
class Graph;
class Character;
struct Color;

/**
 * A manager for sending the attacker objects from an initiated attack from
 * one computer to another.  Since there is a restriction of bandwidth in
 * the wires, this will periodically send out attackers and decrement the
 * strength in the Computer from which the attack is originating.
 */
class Attack : public QObject
{
    Q_OBJECT

public:
    Attack (Computer* from, Computer* to, Graph* graph);
    ~Attack ();

    // where the attack is coming from
    Computer* from ();
    // where the attack is going
    Computer* to ();
    // Character that owns this Attack
    Character* owner ();
    // the map of the level to give to the attacker
    Graph* graph ();

    // strength left in the attack
    virtual int strengthLeft ();
    virtual qreal attackersPerSecond ();
    virtual int strengthPerAttacker ();
    // speed of the attackers in grid nodes per second.
    virtual qreal attackerSpeedFactor ();
    Color getColor ();
    bool isPaused ();

public slots:
    /**
     * Updates the time of this object and allows this object to determine
     * if it is time to send out a new attacker.  When it is time to send
     * out a new attacker, a new attacker object is created and the
     * sendAttacker() signal is emitted.
     *
     * If the strengthLeft for this objet is or becomes zero in this method,
     * then the attackIsDone() signal is sent.  This object will still
     * continue to work as before after this signal.  To reuse this object,
     * simply call setStrength() or increaseStrength() and it will work.
     *
     * As an attacker is created and sent out, the strength of the from
     * Computer will be decreased accordingly.
     *
     * If this object is paused, then the updateTime() method does nothing.
     */
    virtual void updateTime (int elapsedMillis);

    virtual void increaseStrength (int toAdd);
    virtual void setStrength (int newStrength);
    void clearStrength ();  // sets the strength to zero
    virtual void setAttackersPerSecond (qreal attackersPerSecond);
    virtual void setStrengthPerAttacker (int strengthPerAttacker);
    virtual void setAttackerSpeedFactor (qreal attackerSpeedFactor);

    /**
     * Changes the owner to a new owner.  If the owner changes, then
     * the remaining strength left in the attack is returned to zero.
     */
    void setOwner (Character* newOwner);

    // Paused means that the updateTime does nothing.
    void pause ();
    void unpause ();
    void setPaused (bool toPause);

signals:
    /**
     * This attacker is created on the heap and sent.  The attacker has
     * already been unpaused and sent the amount of leftover milliseconds
     * from the updateTime() method.
     *
     * Note that this Attacker class is the parent to the newAttacker, so
     * if it is not deleted by the recipient of this signal, then it will
     * be deleted when this object is deleted.
     *
     * If you want to delete this object and not the newAttacker with it,
     * then you must reset the parent.
     *
     * To find out how much strength was sent with this attacker, simply
     * call newAttacker->quantity()
     */
    void sendAttacker (Attacker* newAttacker);

    /**
     * This signal is sent when the _strengthLeft is at or becomes zero
     * in the updateTime() method.  This includes calling the updateTime()
     * method before initializing the strength of the attack.
     */
    void attackIsDone ();

private:
    /**
     * _from                 - The origin Computer of the attack
     * _to                   - The destination Computer of the attack
     * _owner                - Character that initiated this attack
     * _graph                - The game board
     * _strengthLeft         - Strength of the attack left.  The attack is done
     *                         when this reaches zero.
     * _attackersPerSecond   - Number of attackers to send out each second
     * _strengthPerAttacker  - How much of the attack strength will be sent
     *                         with each attacker.  This will be the "quantity"
     *                         that the attacker represents.
     * _attackerSpeedFactor  - speed factor for the created attackers
     *                         in grid nodes per second
     * _millisSinceLastAttacker
     *                       - Number of milliseconds since the last attacker
     *                         was created and sent out.
     * _isPaused             - indicates if the updateTime() method will work.
     *                         If this is true, then updateTime will do nothing,
     *                         otherwise updateTime will periodically send out
     *                         attackers.
     *                         Note: This is useful if you intend to reuse
     *                         this object again and again but don't want
     *                         _millisSinceLastAttacker to accumulate.
     */
    Computer* _from;
    Computer* _to;
    Character* _owner;
    Graph* _graph;
    int _strengthLeft;
    qreal _attackersPerSecond;
    int _strengthPerAttacker;
    qreal _attackerSpeedFactor;
    int _millisSinceLastAttacker;
    bool _isPaused;
};

#endif // ATTACK_H
