#include <QtCore/QString>
#include <QtTest/QtTest>
#include "../computer.h"

class ComputerTester : public QObject
{
    Q_OBJECT
    
public:
    ComputerTester();
    int capacity;
    int generateSpeed;
    int strength;
    QString ID;
    QString controller;
    qreal graphicalSize;
    Computer* test;

private Q_SLOTS:
    void computerDefaultConstructorTest();
    void computerConstructorTest();
    void setIdentityTest();
    void setCapacityTest();
    void setCurrentStrengthTest();
    void setControllerTest();
    void setGenerateSpeedTest();
    void isPausedTest();
    void isConnectedToComputerTest();
    void setEdgeTest();


signals:
    void unpauseComputer();
    void pauseComputer();

};

ComputerTester::ComputerTester()
{
    capacity = 50;
    generateSpeed = 2;
    strength = 20;
    ID = "D3";
    controller = "A";
    graphicalSize = 20;
    test = new Computer(capacity, generateSpeed, strength, ID, controller, graphicalSize);
    connect (this, SIGNAL(unpauseComputer()),
             test, SLOT(unpause()));
    connect (this, SIGNAL(pauseComputer()),
             test, SLOT(pause()));
}

void ComputerTester::computerDefaultConstructorTest()
{
    Computer* defTest = new Computer();
    QVERIFY2(defTest!=NULL, "Computer constructor returns null");
    delete defTest;
}

void ComputerTester::computerConstructorTest()
{

    QVERIFY2(test->getCapacity()==capacity, "Capacity not setting or returning correctly");
    QVERIFY2(test->getGenerateSpeed()==generateSpeed, "Generate speed not setting or returning correctly");
    QVERIFY2(test->getCurrentStrength()==strength, "Strength not setting or returning correctly");
    QVERIFY2(test->getIdentity()==ID, "ID not setting or returning correctly");
    QVERIFY2(test->getController()==Computer::Player, "Controller not setting or returning correctly");
    QVERIFY2(test->getGraphicalSize()==graphicalSize, "Graphical size not setting or returning correctly");

}

void ComputerTester::setIdentityTest()
{
    QString newIdent = "L2";
    test->setIdentity(newIdent);
    QVERIFY2(test->getIdentity()==newIdent, "Identity setter failing");
}

void ComputerTester::setCapacityTest()
{
    int newCap = 30;
    test->setCapacity(newCap);
    QVERIFY2(test->getCapacity()==newCap, "Capacity setter failing");
}

void ComputerTester::setCurrentStrengthTest()
{
    //test addStrength
    int addToStrength = 5;
    test->addStrength(addToStrength);
    QVERIFY2(test->getCurrentStrength()==strength+addToStrength, "Strength not adding correctly");

    //test strength setter
    test->setCurrentStrength(strength);
    QVERIFY2(test->getCurrentStrength()==strength, "Strength setter failing");

    //test subtractStrength
    int subtractFromStrength = 5;
    test->subtractStrength(subtractFromStrength);
    QVERIFY2(test->getCurrentStrength()==strength-subtractFromStrength, "subtractFromStrength failing");

}

void ComputerTester::setControllerTest()
{
    Computer::Controller newController = Computer::AI;
    test->setController(newController);
    QVERIFY2(test->getController()==newController, "Controller setter failing");
}

void ComputerTester::setGenerateSpeedTest()
{
    int newGenerateSpeed = 30;
    test->setGenerateSpeed(newGenerateSpeed);
    QVERIFY2(test->getGenerateSpeed()==newGenerateSpeed, "Generate Speed setter failing");
}

void ComputerTester::isPausedTest()
{

    //if timer is paused, returrns true
    //test->pause();
    emit pauseComputer ();
    QVERIFY2(test->isPaused()==true, "Pause failing");
    //test->unpause();
    emit unpauseComputer ();
    QVERIFY2(test->isPaused()==false, "Unpause failing");
}

void ComputerTester::isConnectedToComputerTest()
{
    Computer* testDirection = new Computer(capacity, generateSpeed, strength, ID, controller, graphicalSize);
    QVERIFY2(test->getDirection(testDirection)==Computer::NoDirection, "testDirection says they're connected when they're not");
    QVERIFY2(test->isConnectedToComputer(testDirection)==false, "testDirection says they're connected when they're not");
    test->setEdge(testDirection, Computer::East);
    QVERIFY2(test->isConnectedToComputer(testDirection)==true, "testDirection says they are not connected when they are");
    QVERIFY2(test->getDirection(testDirection)==Computer::East, "getDirection returning wrong value");
}

void ComputerTester::setEdgeTest()
{
    Computer* testEdges = new Computer(capacity, generateSpeed, strength, ID, controller, graphicalSize);
    QVERIFY2(test->isConnectedToComputer(testEdges)==false, "Computers need to start disconnected");
    test->setEdge(testEdges, Computer::North);
    QVERIFY2(test->isConnectedToComputer(testEdges)==true, "Computers are not connected");
    QVERIFY2(test->getEdge(Computer::North)==testEdges, "getEdges returning wrong value");
}

QTEST_APPLESS_MAIN(ComputerTester)

#include "tst_computertester.moc"
