/**
   Node class used to represent a piece of the network in hackerfrenzy

   Authors: Brenden Tyler and Ryan McMillan
   CS3505 Spring 2012
*/

//library includes
#include <QString>
#include <QVector>
#include <iostream>

//local includes
#include "graph/node.h"
#include "graph/computer.h"

//constructor.  Sets the node to the passed in type.  If node is a computer,
// constructs the computer as well
Node::Node (const QPoint &position, const QString &nodeType, Computer* addComp)
{
    _comp = addComp;
    _type = nodeType;
    _position = position;
}

//returns the type of the node
QString Node::getType () const
{
    return _type;
}

//returns the computer in the node.  If there is no computer, returns null
Computer* Node::getComp () const
{
    return _comp;
}

QPoint Node::getPos () const
{
    return _position;
}

//sets the node's type
void Node::setType (const QString &toSet)
{
    _type = toSet;
}

//sets the node's computer
void Node::setComp (Computer *toSet)
{
    // TODO add a check in here.  If there is no computer in the node, 
    // don't allow one to be set
    _comp = toSet;
}

void Node::setPos (const QPoint &position)
{
    _position = position;
}

