
/** A graph that is used to represent a network
 *
 *  Authors: Brenden Tyler and Ryan McMillan
 *  CS3505 Spring 2012
 *
 *  Different pieces of the network are defined as shown below
 *  Horizontal Wire:    ==
 *  Vertical Wire:      ||
 *  Wire Turn Left:     //
 *  Wire Turn Right:    \\
 *  Computer:           A1 (computerTypecomputerNumber)
 *      Computer Types:
 *          D = Desktop
 *  Edge:               ##
 *  Nothing:            00
 *
 */


#ifndef GRAPH_H
#define GRAPH_H

//system includes
#include <QString>
#include <iostream>
#include <QVector>
#include <QSize>

//local includes
#include "graph/node.h"
#include "graph/computer.h"

class Graph
{
public:
    //constructor
    Graph ();
    
    //constructor that takes in a file name.  Uses the file to create the graph
    Graph (int xSize);

    ~Graph();

    //returns the 2D grid used to represent the network
    QVector<QVector<Node> >* getNetwork();
    
    //returns the contents? of the node at location
    bool isInside (const QPoint &location) const;

    //returns the contents? of the node at the specified indecies
    bool isInside (int x, int y) const;

    //returns the dimensions of the graph as a QSize
    QSize getSize () const;

    //returns a pointer to the node at location
    Node* at (const QPoint &location);
    const Node* at (const QPoint &location) const;

    //returns a pointer to the node at the sepcified indecies
    Node* at (int x, int y);
    const Node* at (int x, int y) const;
    
    int getNumberOfDevices () const;
    int getNumberOfConnectors() const;
    void setNumberOfDevices (int num);
    void setNumberOfConnectors (int num);
    void addDeviceLocation(int x,int y);
    void addConnectorLocation(int x,int y);
    QVector<QPoint>* deviceLocations ();
    const QVector<QPoint>* deviceLocations () const;
    QVector<QPoint>* connectorLocations ();
    const QVector<QPoint>* connectorLocations () const;
    void addPlayer (Character* player);
    QVector<Character*>* getPlayers ();
    const QVector<Character*>* getPlayers () const;

private:
    //a 2d array to store the nodes of the network
    QVector<QVector<Node> >* _network;
    QVector<QPoint>* _deviceLocations;
    QVector<QPoint>* _connectorLocations;
    QVector<Character*>* _players;
    int _numDevices;
    int _numConnectors;

};

#endif // GRAPH_H
