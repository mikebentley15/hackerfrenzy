

#include <iostream>
#include <QString>
#include <QSet>
#include <QtCore/qmath.h>

#include "attack.h"
#include "attacker/attacker.h"
#include "graph/computer.h"

using namespace std;

/*** Constructors and Destructors ***/

Computer::Computer(int capacity, int generateSpeed, int strength,
                   const QString &ID, const QString &controller,
                   qreal graphicalSize)
{
    _identity = ID;
    _capacity = capacity;
    _currentStrength = strength;
    _controllerID = controller;
    _controller = NULL;
    _generateSpeed = generateSpeed;
    _graphicalSize = graphicalSize;
    _edges[Computer::North] = NULL;
    _edges[Computer::East]  = NULL;
    _edges[Computer::South] = NULL;
    _edges[Computer::West]  = NULL;
    _node = NULL;
    _millisSinceLastStrengthIncrease = 0;
    _adjacencyList = new QVector<Computer*>();
    _registeredAttacks = new QSet<Attack*>();
    _isPaused = false;
}

Computer::~Computer()
{
    delete _adjacencyList;
}



/*** =Static Methods= ***/

QPoint Computer::moveInDirection (const QPoint &location,
                                  Computer::Direction direction)
{
    if (direction == Computer::NoDirection)
        return QPoint(location);
    if (direction == Computer::North)
        return QPoint(location.x(), location.y() - 1);
    if (direction == Computer::East)
        return QPoint(location.x() + 1, location.y());
    if (direction == Computer::South)
        return QPoint(location.x(), location.y() + 1);
    if (direction == Computer::West)
        return QPoint(location.x() - 1, location.y());
    
    return QPoint(location);
}

Computer::Direction Computer::oppositeDirection (Computer::Direction direction)
{
    if (direction == Computer::North)
        return Computer::South;
    if (direction == Computer::East)
        return Computer::West;
    if (direction == Computer::South)
        return Computer::North;
    if (direction == Computer::West)
        return Computer::East;
    
    return Computer::NoDirection;
}



/*** =Public Methods= ***/

Computer& Computer::operator =(const Computer &right)
{
    if (&right != this)
    {
        _identity        = right._identity;
        _capacity        = right._capacity;
        _currentStrength = right._currentStrength;
        _controller      = right._controller;
        _generateSpeed   = right._generateSpeed;
        _graphicalSize   = right._graphicalSize;
        _edges[Computer::North] = right._edges[Computer::North];
        _edges[Computer::East]  = right._edges[Computer::East];
        _edges[Computer::South] = right._edges[Computer::South];
        _edges[Computer::West]  = right._edges[Computer::West];
        _node            = right._node;
        _millisSinceLastStrengthIncrease = right._millisSinceLastStrengthIncrease;
        _isPaused        = right._isPaused;

        delete _adjacencyList;
        _adjacencyList   = new QVector<Computer*> (*right._adjacencyList);

        delete _registeredAttacks;
        _registeredAttacks = new QSet<Attack*> (*right._registeredAttacks);
    }

    return *this;

}

//**Getters**//
QVector<Computer*> Computer::getAdjacencyList () const
{
    return *_adjacencyList;
}

//returns the computer's identifier
QString Computer::getIdentity () const
{
    return _identity;
}

//get the max number of troops the computer can hold
int Computer::getCapacity () const
{
    return _capacity;
}

//get the number of troops a computer currently has
int Computer::getCurrentStrength () const
{
    return qFloor(_currentStrength);
}

int Computer::getAvailableStrength() const
{
    int availableStrength = getCurrentStrength ();
    foreach (Attack* attack, *_registeredAttacks)
        availableStrength -= attack->strengthLeft ();

    return availableStrength;
}

QString Computer::getControllerID() const
{
    return _controllerID;
}

//get who currently controls the computer
Character* Computer::getController () const
{
    return _controller;
}

// speed that strength is generated (milliseconds / one strength)
int Computer::getGenerateSpeed() const
{
    return _generateSpeed;
}

qreal Computer::getGraphicalSize () const
{
    return _graphicalSize;
}

Node* Computer::getParentNode () const
{
    return _node;
}

// paused means the computer is not generating strength
bool Computer::isPaused () const
{
    return _isPaused;
}

// by connected we mean directly connected
bool Computer::isConnectedTo (Computer *otherComputer) const
{
    return _edges[Computer::North] == otherComputer ||
           _edges[Computer::East]  == otherComputer ||
           _edges[Computer::South] == otherComputer ||
           _edges[Computer::West]  == otherComputer;
}

Computer* Computer::getEdge (Computer::Direction direction) const
{
    if (direction == Computer::NoDirection)
        return NULL;
    
    return _edges[direction];
}

Computer::Direction Computer::getDirection (Computer *connectedComputer) const
{
    if (connectedComputer == NULL)
        return Computer::NoDirection;
    if (connectedComputer == _edges[Computer::North])
        return Computer::North;
    if (connectedComputer == _edges[Computer::East])
        return Computer::East;
    if (connectedComputer == _edges[Computer::South])
        return Computer::South;
    if (connectedComputer == _edges[Computer::West])
        return Computer::West;
    
    return Computer::NoDirection;
}




/*** =Public Slots= ***/

void Computer::updateTime(int elapsedMillis)
{
    if (!_isPaused)
    {
        _millisSinceLastStrengthIncrease += elapsedMillis;

        // Evaluate the new strength while capping it at _capacity
        qreal oldStrength = _currentStrength;
        _currentStrength = oldStrength + _millisSinceLastStrengthIncrease / _generateSpeed;
        _currentStrength = qBound (0.0, _currentStrength, _capacity);

        // update the leftover milliseconds
        _millisSinceLastStrengthIncrease %= _generateSpeed;

        // If the integer value of the strength changed, then emit the signal
        if (qFloor(oldStrength) != qFloor(_currentStrength))
            emit strengthChanged (_currentStrength);
    }
}

QSet<Attack*>* Computer::getRegisteredAttacks()
{
    return _registeredAttacks;
}

//changes the computer's unique identifier
void Computer::setIdentity (const QString &newID)
{
    _identity = newID;
}

//resets the comptuer's max troop number to newCapacity
void Computer::setCapacity (int newCapacity)
{
    _capacity = newCapacity;
}

//sets the computer's strength to newStrength
void Computer::setCurrentStrength (int newStrength)
{
    // don't allow the newStrength to be outside of allowed ranges
    newStrength = qBound (0.0, (qreal)newStrength, _capacity);
    
    if (qFloor(_currentStrength) != newStrength)
    {
        // Set the new strength, but keep the fractional part
        qreal fractionalPart = _currentStrength - qFloor(_currentStrength);
        _currentStrength = qBound ((qreal)0.0,
                                   newStrength + fractionalPart,
                                   (qreal)_capacity);
        emit strengthChanged (_currentStrength);
    }
}

void Computer::addToAdjacencyList (Computer* comp)
{
    // TODO: reconcile the differences between Adjacency list and edges
    _adjacencyList->push_back(comp);
}

void Computer::addStrength (int toAdd)
{
    qreal oldStrength = _currentStrength;
    _currentStrength = qMin (oldStrength + toAdd, (qreal)_capacity); // allows for negative numbers
    
    if (qFloor(_currentStrength) != qFloor(oldStrength))
        emit strengthChanged (_currentStrength);
}

void Computer::subtractStrength (int toSubtract)
{
    this->addStrength (-toSubtract);
}

void Computer::setGenerateSpeed (int newGenerateSpeed)
{
    _generateSpeed = newGenerateSpeed;
}

//sets the computer's controller to newController (player, ai, or opponent)
void Computer::setController (Character* newController)
{
    if (_controller != newController)
    {
        if (_controller != NULL)
            _controller->removeFromControlled (this);

        _controller = newController;

        if (_controller != NULL)
            _controller->addToControlled (this);

        emit controllerChanged (this);
    }
}

//sets the adjacency list to the passed in list
void Computer::setEdge(Computer *computer, Computer::Direction direction)
{
    if (direction != NoDirection)
        _edges[direction] = computer;
}

void Computer::setParentNode (Node *node)
{
    _node = node;
}

void Computer::pause ()
{
    _isPaused = true;
}

void Computer::unpause ()
{
    _isPaused = false;
}

// simply a setter for the _isPaused variable
void Computer::setPaused (bool toPause)
{
    toPause ? pause() : unpause();
}

void Computer::registerAttack(Attack *attack)
{
    if (attack != NULL && attack->from() == this && !_registeredAttacks->contains(attack))
    {
        _registeredAttacks->insert(attack);
        connect (attack, SIGNAL(sendAttacker(Attacker*)),
                 this,   SLOT(attackerSent(Attacker*)));
        connect (attack, SIGNAL(destroyed(QObject*)),
                 this,   SLOT(removeAttack(QObject*)));
        connect (this, SIGNAL(controllerChanged(Computer*)),
                 attack, SLOT(clearStrength()));
    }
}

void Computer::attackerReachedMe(Attacker *attacker)
{
    // If this computer's owner is the same as the attacker's owner,
    // then reinforce me.
    if (attacker->owner () == attacker->destination ()->getController ())
    {
        // TODO: have different attacker abilities here.
        addStrength (attacker->quantity ());
    }

    // Otherwise, the attacker is attacking this computer
    else
    {
        _currentStrength -= attacker->quantity ();

        // If the attack overcame this computer
        if (_currentStrength < 0)
        {
            _currentStrength = -_currentStrength;
            this->setController (attacker->owner());
        }

        emit strengthChanged (_currentStrength);
    }
}




/*** =Private Slots= ***/

void Computer::removeAttack(QObject *destroyedAttacker)
{
    Attack* toRemove = static_cast<Attack*>(destroyedAttacker);
    _registeredAttacks->remove (toRemove);
}

void Computer::attackerSent(Attacker *attacker)
{
    this->subtractStrength (attacker->quantity ());
}


