#ifndef MAPREADER_H
#define MAPREADER_H

//system includes
#include <QVector>
#include <QString>
#include <QHash>
#include <QFile>

//local includes
#include "character/ai.h"
#include "character/character.h"
#include "game_engine/structures.h"
#include "graph/computer.h"
#include "graph/graph.h"

class mapReader
{
public:
    Computer* tempComp;
    Node * tempNode;
    //
    qint64 MAX_MAP_CHARS;

    //A list of filenames for available maps
    QVector<QString> MAP_LIST;

    //default constructor
    mapReader();

    //Constructor.  Creates a mapReaded using the specified file
    mapReader(const QString &filename);

    //Destructor
    ~mapReader();

    //
    Graph* loadMap(const QString &filename);

    //
    Graph* loadMap(int mapNumber);

    /**
     * Helper method for building the adjacency list
     *
     * Takes a direction and a location and determines the next direction
     * to travel based on the current direction and the type of node.
     *
     * If there is a problem such as a node has an impossible wire value for
     * the direction we are heading, then Computer::NoDirection will
     * be returned.  This includes if the node is a computer.
     */
    static Computer::Direction updateDirection (
        QString nodeString, Computer::Direction currentDirection);

private:
    /**
     *  _devices         - maps computer names to devices for use in
     *                     building the graph
     *  _players         - tracks the player objects
     *  _mapName         - name of the map
     *  _mapDimension    - dimension specified for the map in the file
     *  _mapDifficulty   - used for AI (at least maybe in the future)
     *  _gameBoard       - gameboard created by the map file
     *  _listFile        - list of internal maps making the level selection
     *
     * STATIC VARIABLES
     *  _colors          - order of colors to assign to the players
     *  _neutralColor    - color of the neutral players
     */
    QHash<QString, Computer*> _devices;
    QHash<QString, Character*> _players;
    QString _mapName;
    QPoint _mapDimension;
    int _mapDifficulty;
    Graph* _gameBoard;
    QFile* _listFile;
    static const Color _colors[];
    static const Color _neutralColor;

    //parses the file from loadMap and creates a map
    Graph* buildMap(const QVector<QString> &mapTokens);

    void loadMapList (QFile* _listFile);
    void findDimensions (int &index, const QVector<QString> &mapTokens);
    void uploadMap (int &index, const QVector<QString> &mapTokens);
    void uploadDevices (int &index, const QVector<QString> &mapTokens);
    void pairCompsAndConts();
    void uploadAdjacencyList (int &index, const QVector<QString> &mapTokens);
    void createPlayers (int &index, const QVector<QString> &mapTokens);
    void developAdjacencyDeviceList();

};

#endif // MAPREADER_H

