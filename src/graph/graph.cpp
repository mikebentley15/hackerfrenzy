/**
  A graph that is used to represent a network in the game hackerfrenzy

  Authors: Brenden Tyler and Ryan McMillan
  CS3505 Spring 2012
  */

//system includes
#include <fstream>
#include <iostream>
#include <QVector>
#include <QString>
#include <QSize>


//local includes
#include "graph/graph.h"
#include "graph/node.h"
#include "graph/computer.h"

//default constructor.  Initialized the network to an empty 20x20 grid
Graph::Graph()
{
    _network = new QVector<QVector<Node> >(20);
    _deviceLocations = new QVector<QPoint> ();
    _connectorLocations = new QVector<QPoint> ();
    _numDevices = 0;
    _players = new QVector<Character*> ();

    QVector<QVector<Node> >::iterator it;
    for(it = _network->begin(); it!=_network->end(); ++it)
    {
        it->resize(20);
    }
}

Graph::Graph(int xSize)
{
    _network = new QVector<QVector<Node> > (xSize);
    _deviceLocations = new QVector<QPoint> ();
    _connectorLocations = new QVector<QPoint> ();
    _numDevices = 0;
    _players = new QVector<Character*> ();
}

Graph::~Graph()
{
    delete _deviceLocations;
    delete _connectorLocations;
    delete _network;
    delete _players;
}

//Assignment


//Copy Constructor


QVector<QVector<Node> >* Graph::getNetwork()
{
    return _network;
}

bool Graph::isInside (const QPoint &location) const
{
    return isInside (location.x(), location.y());
}

bool Graph::isInside (int x, int y) const
{
    return 
        x >= 0 &&
        y >= 0 &&
        x < this->getSize().width() &&
        y < this->getSize().height();
}

QSize Graph::getSize () const
{
    return QSize (_network->size(), _network->at(0).size());
}

Node* Graph::at (const QPoint &location)
{
    return at (location.x(), location.y());
}

const Node* Graph::at (const QPoint &location)  const
{
    return at (location.x(), location.y());
}

Node* Graph::at (int x, int y)
{
    return &(*_network)[x][y];
}

const Node* Graph::at (int x, int y) const
{
    return &(*_network)[x][y];
}

int Graph::getNumberOfDevices() const
{
    return _numDevices;
}

void Graph::setNumberOfDevices (int num)
{
    _numDevices = num;
}

int Graph::getNumberOfConnectors() const
{
    return _numConnectors;
}

void Graph::setNumberOfConnectors (int num)
{
    _numConnectors = num;
}

void Graph::addDeviceLocation(int x,int y)
{
    _deviceLocations->push_back(QPoint(x,y));
}

QVector<QPoint>* Graph::deviceLocations ()
{
    return _deviceLocations;
}

const QVector<QPoint>* Graph::deviceLocations () const
{
    return _deviceLocations;
}

void Graph::addConnectorLocation(int x,int y)
{
    _connectorLocations->push_back(QPoint(x,y));
}

QVector<QPoint>* Graph::connectorLocations ()
{
    return _connectorLocations;
}

const QVector<QPoint> *Graph::connectorLocations() const
{
    return _connectorLocations;
}

void Graph::addPlayer(Character *player)
{
    _players->append (player);
}

QVector<Character *> *Graph::getPlayers()
{
    return _players;
}

const QVector<Character *> *Graph::getPlayers() const
{
    return _players;
}
