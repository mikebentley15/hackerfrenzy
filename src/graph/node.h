/**
 * Node class used to represent a piece of the network in hackerfrenzy
 *
 * Authors: Brenden Tyler and Ryan McMillan
 * CS3505 Spring 2012
 */

#ifndef NODE_H
#define NODE_H

//system includes
#include <iostream>
#include <list>
#include <QString>
#include <QVector>
#include <QObject>
#include <QPoint>

//local includes
#include "graph/computer.h"


class Node
{

public:

    //constructor that sets the node's type
    explicit Node (const QPoint &position = QPoint(),
                   const QString &nodeType = "##",
                   Computer* addComp = NULL);
    
    //returns the type of the node
    QString getType() const;
    
    // returns the computer object stored in the node.  If no computer, returns
    // null
    Computer* getComp () const;
    
    // returns the position of this Node on the Graph
    QPoint getPos () const;
    
    //changes the type of the node
    void setType(const QString &toSet);
    
    // Changes the computer assigned to this node
    void setComp(Computer *toSet);
    
    // changes the position of this node.
    void setPos (const QPoint &position);
    
private:
    //member variables
    
    //tracks the node's type
    QString _type;
    
    //if the node contains a computer, it is stored here
    Computer *_comp;
    
    // The position of this Node on the Graph
    QPoint _position;
};
#endif
