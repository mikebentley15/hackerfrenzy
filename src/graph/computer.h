/**
 * Represents a computer in the hackerfrenzy game
 *
 * Author: Brenden Tyler, Ryan McMillan, and Michael Bentley
 * CS3505 Spring 2012
 *
 */

#ifndef COMPUTER_H
#define COMPUTER_H

#include <iostream>
#include <QString>
#include <QObject>
#include <QBasicTimer>
#include <QSet>
#include <QTime>
#include <QVector>

#include "character/character.h"

class Attack;
class Attacker;
class Node;
class QPoint;


class Computer : public QObject
{
    Q_OBJECT
    
public:
    
    /**
     * Which side of the computer is connected to a pipe
     *
     * Used in the adjacency list to know which direction to start
     * traveling to get to a destination connected computer.
     */
    enum Direction {
        NoDirection = -1,
        North = 0,
        East = 1,
        South = 2,
        West = 3
    };
    
    static QPoint moveInDirection (const QPoint &location, Direction direction);
    static Direction oppositeDirection (Direction original);
    
    /**
     * Constructs the computer object with the various necessary parameters
     *
     * Computer starts out in the paused state.  To start generating strength,
     * you must call unpause() or setPaused(false).
     *
     * @param parentNode
     *   Node where this computer resides on the graph (center of the computer)
     * @param capacity
     *   Total strength this computer can generate
     * @param generateSpeed
     *   How many milliseconds pass for one more strength to be added.
     * @param strength
     *   Initial strength
     * @param ID
     *   Unique identifier for this computer
     * @param controller
     *   Who controlls the computer
     *   TODO: describe how to specify the controller
     * @param graphicalSize
     *   Width relative to the size of one Node on the Graph.  The height
     *   will be scaled according to this width as well.
     */
    explicit Computer (int capacity = 0, int generateSpeed = 0,
                       int strength = 0, const QString &ID = "",
                       const QString &controller = "",
                       qreal graphicalSize = 0);
    ~Computer ();

    // getters
    QString getIdentity () const;
    int getCapacity () const;
    int getCurrentStrength () const;
    // returns the current strength minus the strength of registered attacks
    int getAvailableStrength () const;
    QString getControllerID() const;
    Character *getController() const;
    int getGenerateSpeed () const;
    qreal getGraphicalSize () const;
    Node* getParentNode () const;
    bool isPaused () const;
    Computer& operator =(const Computer &right);
    QVector<Computer*> getAdjacencyList () const;
    void addToAdjacencyList (Computer* comp);
    QSet<Attack*>* getRegisteredAttacks();

    /**
     * Returns true if this computer is connected to the otherComputer
     * directly through a wire.
     *
     * If NULL is given, then this method returns false only if there
     * is a connection on all four sides.
     */
    bool isConnectedTo (Computer *otherComputer) const;
    
    /**
     * Returns the pointer to the computer that can be reached by traveling
     * the wire in the indicated direction from this computer.
     *
     * If there is no connection to another computer starting with the given
     * direction, NULL will be returned.  That includes the case that the
     * value NoDirection is given.
     */
    Computer* getEdge (Direction direction) const;
    
    /**
     * Returns the direction to go to get to the connectedComputer
     * from this computer.
     *
     * If this computer is not connected directly to the computer
     * provided, then this method returns NoDirection.  Also if
     * the user provides a NULL value, then this method returns
     * NoDirection.
     */
    Direction getDirection (Computer *connectedComputer) const;
    
signals:
    void controllerChanged (Computer* thisComputer);

    /**
     * signal for when the strength value changes
     * Computer's strength is set to newStrength
     */
    void strengthChanged (int newStrength);
    
    
public slots:
    /**
     * updates the strength.  This is called by the game to have this
     * computer update itself.
     *
     * If this Computer is paused, then this method does nothing and
     * returns.
     */
    void updateTime (int elapsedMillis);

    /**
     * Identifier is a unique string used to identify each computer
     * setIdentity changes the computer's identifier to newID
     */
    void setIdentity (const QString &newID);
    
    //changes the computer's capacity
    void setCapacity (int newCapacity);
    
    //changes the computer's current troop count
    void setCurrentStrength (int newStrength);
    
    // Adds to the current strength
    void addStrength (int toAdd);
    
    // Subtracts from the current strength
    void subtractStrength (int toSubtract);
    
    void setGenerateSpeed (int newGenerateSpeed);
    
    //changes the computer's controller
    void setController (Character *newController);
    
    //changes the list of edges for the node
    void setEdge (Computer *computer, Direction direction);
    
    // Changes the parent node of this computer on the graph
    void setParentNode (Node *node);
    
    // Pauses the node pausing the incremental strength increase
    void pause ();
    void unpause ();
    void setPaused (bool toPause);
    
    /**
     * Registers an attack that is originating from this Computer
     *
     * If the origin of the attack is not this Computer, then it
     * is not registered.  Also, if the attack is already registered,
     * then it won't be registered again.
     *
     * Registering an attack means that the strength of the attack
     * is subtracted from the overall strength of this computer
     * when computing available strength.
     *
     * Registering also means that the sendAttacker() signal from
     * the Attack object will automatically remove the strength
     * of the attacker from this Computer.
     *
     * This object will register the destructed signal on the attack
     * object and will unregister it automatically.  Therefore, to
     * unregister the attack, merely delete it.
     */
    void registerAttack (Attack* attack);

    virtual void attackerReachedMe (Attacker* attacker);

protected:

private:
    // _identity        - unique identifier for this computer
    // _capacity        - maximum capacity for this computer
    // _currentStrength - current number of troops in the computer
    // _controllerID    - used to identify which player initially controls computer
    // _controller      - who currently controls the comptuer
    // _generateSpeed   - amount of time the needs to pass before adding
    //                    to the strength (in ms)
    // _graphicalSize   - size of the image of the comptuer on the game board
    // _edges           - a list of other computers this one is connected to
    // _node            - node where this computer resides
    // _timer           - triggers the timerEvent every _generateSpeed ms
    // _watch           - a stopwatch to keep track of intervals between
    //                    timerEvent calls
    // _millisSinceLastStrengthIncrease
    //                  - leftover time from the last strength increase.
    // _adjacencyList   - A redundant list of adjacencies from the adjacency
    //                    list section of the map.  This only has the list
    //                    of connected computers but gives no information
    //                    about which direction is connected.
    // _registeredAttacks
    //                  - The attaks that have been registered with this
    //                    device.
    // _isPaused        - Makes the updateTime() method not do anything if
    //                    this is true.  If false, then updateTime() will
    //                    update the strength of the computer.
    QString            _identity;
    qreal              _capacity;
    qreal              _currentStrength;
    QString            _controllerID;
    Character*         _controller;
    int                _generateSpeed;
    qreal              _graphicalSize;
    Computer*          _edges[4];
    Node*              _node;
    int                _millisSinceLastStrengthIncrease;
    QVector<Computer*>*  _adjacencyList;
    QSet<Attack*>*     _registeredAttacks;
    bool               _isPaused;

private slots:
    /**
     * Deregisters the attack from this object.  Connected to the destroyed()
     * signal from the Attack object to be removed.
     */
    void removeAttack (QObject *destroyedAttacker);

    // Subtracts the strength of the attacker from this Computer.
    void attackerSent (Attacker* attacker);
};
#endif

