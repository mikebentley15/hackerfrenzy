#ifndef MAPREADER_CPP
#define MAPREADER_CPP

//system includes
#include <QString>
#include <QFile>
#include <QVector>
#include <iostream>
#include <QRegExp>
#include <QSet>
#include <QTextStream>

//local includes
#include "audit.h"
#include "character/ai.h"
#include "character/humanplayer.h"
#include "character/neutralplayer.h"
#include "game_engine/structures.h"
#include "graph/computer.h"
#include "graph/graph.h"
#include "graph/mapreader.h"
#include "graph/node.h"


using namespace std;

const Color mapReader::_colors[] =
{
    Color(0.2f, 1.0f, 0.2f, 1.0f),   //green
    Color(1.0f, 0.3f, 0.3f, 1.0f),   //red
    Color(0.5f, 0.5f, 0.5f, 1.0f),   //grey
    Color(1.0f, 0.5f, 0.0f, 1.0f),   //orange
    Color(1.0f, 0.2f, 1.0f, 1.0f),   //purple
    Color(0.2f, 1.0f, 1.0f, 1.0f)    //blue
};

const Color mapReader::_neutralColor = Color(1.0f, 1.0f, 1.0f, 1.0f);

//Constructor.  Creates mapreader object
mapReader::mapReader()
{
    MAX_MAP_CHARS=5000;
    tempComp = new Computer();
    tempNode = new Node ();

    _listFile = new QFile(":/graph/Maps/HFZ_MAPS.hfz");
    loadMapList(_listFile);
}

mapReader::mapReader(const QString &filename)
{
    MAX_MAP_CHARS=5000;
    tempComp = new Computer();
    tempNode = new Node ();

    _listFile = new QFile(filename);
    loadMapList(_listFile);
}

//destructor
mapReader::~mapReader()
{
    delete tempNode;
    delete tempComp;
    delete _listFile;
    delete _gameBoard;
}

void mapReader::loadMapList (QFile* listFile)
{
    if(!listFile->open(QIODevice::ReadOnly))
        cout << "Couldn't open map list file.";

    while(!listFile->atEnd())
    {
        QString temp = listFile->readLine();
        if(temp.contains('\n'))
        {
            temp.remove('\n');
        }
        MAP_LIST.push_back(temp);
    }
}

//loads a map
Graph* mapReader::loadMap(const QString &filename)
{
    QVector<QString> lineTokens;    //start processing the map

    //create the file object
    QFile mapFile (filename);

    //open it
    if(!mapFile.open(QIODevice::ReadOnly))
        Audit::get()->log(QString("Couldn't open map file %1\n").arg(filename));

    QTextStream stream (&mapFile);
    while (!stream.atEnd ())
    {
        QString token;
        stream >> token;
        lineTokens.push_back (token);
    }

    return buildMap(lineTokens);
}

//loads a map based on the index in MAP_LIST
Graph* mapReader::loadMap(int mapNumber)
{
    return loadMap(":/graph/Maps/" + MAP_LIST.at(mapNumber));
}

/*** =Private Methods= ***/

//parses the read in file and uses it to create a map
Graph* mapReader::buildMap(const QVector<QString> &mapTokens)
{
    int tokenCount = mapTokens.count ();

    for(int i = 0; i < tokenCount; i++)
    {
        // if we need to find the dimensions from the file
        if (mapTokens.at(i) == "Dimensions:")
        { findDimensions (i, mapTokens); continue; }

        // not doing anything with this yet, used when AI is included
        if (mapTokens.at(i) == "Difficulty:")
        { _mapDifficulty = mapTokens.at(++i).toInt(); continue; }

        // remove the level's name from the file
        if (mapTokens.at(i) == "LevelName:")
        { _mapName = mapTokens.at(++i); continue; }

        // Determine what devices are listed in the file for this level
        if (mapTokens.at(i) == "Devices:")
        { uploadDevices(i, mapTokens); continue; }

        // Create the players
        if (mapTokens.at(i) == "Players:")
        { createPlayers(i, mapTokens); continue; }

        // Determine the layout of the map
        if (mapTokens.at(i) == "Map:")
        { uploadMap(i, mapTokens); continue; }

        // figure out the pathways
        if (mapTokens.at(i) == "AdjacencyList:")
        { uploadAdjacencyList(i, mapTokens); continue; }
    }

    // Develop the adjacency list for the devices
    developAdjacencyDeviceList();

    //pairs up the computers and characters (players)
    pairCompsAndConts();

    //Set the number of devices
    _gameBoard->setNumberOfDevices(_devices.count());
    _gameBoard->setNumberOfConnectors(_gameBoard->connectorLocations()->size());
    foreach (Character* player, _players)
        _gameBoard->addPlayer (player);

    return _gameBoard;
}

void mapReader::findDimensions (int &index, const QVector<QString> &mapTokens)
{
    //if we find the dimensions
    if(mapTokens.at(index) == "Dimensions:")
    {
        //extracty the x dimension
        _mapDimension.setX(mapTokens.at(++index).toInt());
        _mapDimension.setY(mapTokens.at(++index).toInt());

        //create the graph with the correct dimensions
        _gameBoard = new Graph(_mapDimension.x());
    }
}

void mapReader::uploadMap (int &index, const QVector<QString> &mapTokens)
{
    while(mapTokens.at(index) != "EndMap:" && index <= (_mapDimension.x() * _mapDimension.y()))
    {
        // this loop gets each element in the grid from left
        // to right, top down fashion.
        // not sure what we want to do with this
        /**
         * sample map
         * 00 00 00 00 00 00 00 00 00 00 00
         * 00 00 00 00 00 d1 == == == \\ 00
         * 00 d2 00 00 00 || 00 00 00 || 00
         * 00 || 00 00 00 d3 00 00 00 || 00
         * 00 || 00 00 00 00 00 00 00 || 00
         * 00 \\ == == == == == == == // 00
         * 00 00 00 00 00 00 00 00 00 00 00
         */
        QVector<QVector<Node> >* map = _gameBoard->getNetwork();

        //a regex to identify computers in the map
        QRegExp testForComp ("^[A-Z][1-9]$");

        //for the number of times in the y direction
        for(int yDim = 0; yDim < _mapDimension.y(); yDim++)
        {
            for(int xDim = 0; xDim < _mapDimension.x(); xDim++)
            {
                ++index;

                QString token = mapTokens.at(index);
                //if it is a computer, make sure the computer is in the node
                if(testForComp.exactMatch(token))
                {
                    tempComp = _devices.value(mapTokens.at(index));
                    tempNode = new Node(QPoint(xDim, yDim), mapTokens.at(index), tempComp);
                    tempComp->setParentNode (tempNode);

                    //This is used to build the gui side of things
                    _gameBoard->addDeviceLocation(xDim,yDim);
                }

                //if it isn't, just create the node
                else
                {
                    tempNode = new Node(QPoint(xDim, yDim), mapTokens.at(index));
                }

                (*map)[xDim].push_back(*tempNode);

            }
        }
    }
}

Computer::Direction mapReader::updateDirection (
        QString nodeString, Computer::Direction currentDirection)
{
    if (nodeString == "||")
    {
        if (currentDirection == Computer::South ||
                currentDirection == Computer::North)
            return currentDirection;

        return Computer::NoDirection;
    }

    if (nodeString == "==")
    {
        if (currentDirection == Computer::West ||
                currentDirection == Computer::East)
            return currentDirection;

        return Computer::NoDirection;
    }

    if (nodeString == "//" || nodeString == "/\\")
    {
        if (currentDirection == Computer::North)
            return Computer::East;
        if (currentDirection == Computer::East)
            return Computer::North;
        if (currentDirection == Computer::South)
            return Computer::West;
        if (currentDirection == Computer::West)
            return Computer::South;

        return Computer::NoDirection;
    }

    if (nodeString == "\\\\" || nodeString == "\\/")
    {
        if (currentDirection == Computer::North)
            return Computer::West;
        if (currentDirection == Computer::East)
            return Computer::South;
        if (currentDirection == Computer::South)
            return Computer::East;
        if (currentDirection == Computer::West)
            return Computer::North;

        return Computer::NoDirection;
    }

    // Not acceptable
    return Computer::NoDirection;
}

void mapReader::uploadDevices (int &index, const QVector<QString> &mapTokens)
{
    while(mapTokens.at(++index) != "EndDevices:")
    {
        //computer's name
        QString deviceName = mapTokens.at(index);

        //max capacity
        int deviceCapacity = mapTokens.at(++index).toInt();

        //initial strength
        int deviceStartStrength = mapTokens.at(++index).toInt();

        //owner
        QString deviceController = mapTokens.at(++index);

        //generation rate
        int deviceRegeneration = mapTokens.at(++index).toInt();

        //graphical size
        int deviceSize = mapTokens.at(++index).toInt();

        //  All params for a single computer are now known so build the computer
        tempComp = new Computer (deviceCapacity,
                                 deviceRegeneration,
                                 deviceStartStrength,
                                 deviceName,
                                 deviceController,
                                 deviceSize);

        //put the computer in the hash map for further reference
        _devices.insert(deviceName, tempComp);

        //continue looping to see if more devices are in the list
    }
}

void mapReader::pairCompsAndConts()
{
    // loop through the computers, find and set the appropriate controllers
    // add the computer to the controller's controlled list

    // get an iterator to go throught the list of computers
    QHash<QString, Computer*>::iterator computerIt;

    // set the iterator to the start of devices
    computerIt = _devices.begin();

    Computer* testController;
    QString initialController;
    Character* player;

    // loop through the computers
    while (computerIt != _devices.end())
    {
        // temporary storage for computer being modified
        testController = computerIt.value();

        // Character representing who initially controls the comptuer
        initialController = testController->getControllerID();

        player = _players.value(initialController);

        //set the controller to the one associated with the initial controller value
        testController->setController(player);

        //add this computer to the controller's list of computers
        player->addToControlled(testController);

        //increment the iterator
        computerIt++;
    }
}

void mapReader::uploadAdjacencyList (int &index, const QVector<QString> &mapTokens)
{
    while(mapTokens.at(++index) != "EndAjacencyList:" && index < mapTokens.size() - 2)
    {

        //get the computer we're modifying
        QString key = mapTokens.at(index);
        Computer* toModify = _devices.value(key);

        //add other computers to the list until there's a :
        while (mapTokens.at(++index) != ":")
        {
            //get the computer pointer and put it in the adjacency list
            toModify->addToAdjacencyList(_devices.value(mapTokens.at(index)));
        }
        //rinse lather repeat

        // TODO: change this section of the reader to set each adjacent
        //       Node of the Computer and which direction to go to get
        //       to that adjacent node.

        //set adjacencyList to the computer's adjacency list
        //toModify->setEdges(adjacencyList);
    }
}

void mapReader::createPlayers(int &index, const QVector<QString> &mapTokens)
{
    QVector<QString> playersCreated = QVector<QString>();
    int colorNum = 0;

    while (mapTokens.at(++index) != "EndPlayers:")
    {
        Audit::get()->log(QString("index = %1, mapTokens.size() = %2").arg(index).arg(mapTokens.size()));
        QString playerName = mapTokens.at(index);
        QString playerType = mapTokens.at(++index);

        //check to see if the player that controlles that computer has been created
        if(!playersCreated.contains(playerName))
        {

            if (playerType == "human")
                _players.insert(playerName, new HumanPlayer(playerName, _colors[colorNum++]));

            else if (playerType == "RANDOM_AI")
                _players.insert(playerName, new AI(playerName, _colors[colorNum++], AI::RANDOM_AI));

            else if (playerType == "SIMPLE_AI")
                _players.insert(playerName, new AI(playerName, _colors[colorNum++], AI::SIMPLE_AI));
            else if (playerType == "MEDIUM_AI")
                _players.insert(playerName, new AI(playerName, _colors[colorNum++], AI::MEDIUM_AI));
            else // if (playerType == "neutral" || playerType == anything else)
                _players.insert(playerName, new NeutralPlayer(playerName, _neutralColor));

            //add the player's id to playersCreated
            playersCreated.push_back(playerName);
        }
    }
}

void mapReader::developAdjacencyDeviceList()
{
    QPoint currentPos, startPos, newPos;
    QString device, nodeString, temp;

    // Develop the adjacency list for the devices
    QList<Computer*> computers = _devices.values();
    for (int i = 0; i < computers.size(); i++)
    {
        // Start at the computer's location and try to find the adjacent
        // computers.
        device = computers.at(i)->getParentNode()->getType();

        startPos = computers.at(i)->getParentNode()->getPos();
        for (int dir = 0; dir < 4; dir++) // dir = direction from computers[i]
        {
            Computer::Direction direction = (Computer::Direction) dir;

            // If we already found a connection here, then skip this direction
            if (computers[i]->getEdge (direction) != NULL)
                continue;

            // Follow the pipe until we hit another computer or find out that
            // the pipe doesn't connect to anything.
            currentPos = startPos;
            while (true)
            {
                //move on the graph in the direction specified by dir
                newPos = Computer::moveInDirection(currentPos, direction);

                //if we're still inside the game board
                if (_gameBoard->isInside (newPos))
                {

                    //get the string of the node that we're at
                    temp = _gameBoard->at(newPos)->getType();

                    //if temp == "00" then there's nothing in this direction
                    if (temp == "00")
                    {
                        //there's no computer so set the edge to NULL
                        computers[i]->setEdge(NULL, (Computer::Direction) dir);
                    }

                    //We've ran into a passageway
                    else if (temp == "==" ||
                             temp == "//" ||
                             temp == "||" ||
                             temp == "\\\\" ||
                             temp == "\\/" ||
                             temp == "/\\")

                    {
                        //add the connector to the list
                        _gameBoard->addConnectorLocation(newPos.x(), newPos.y());
                    }

                    // If we run into another computer, then setup the adjacency
                    // connection
                    else
                    {
                        //get the computer from the node
                        tempComp = _gameBoard->at(newPos)->getComp();

                        //set the edge in the original computer to the one we found
                        computers[i]->setEdge (tempComp, (Computer::Direction) dir);

                        //set the edge in the found computer to the original
                        tempComp->setEdge (computers[i], Computer::oppositeDirection(direction));

                        //we got what we came here for
                        break;
                    }

                    currentPos = newPos;
                    nodeString = _gameBoard->at(currentPos)->getType();


                    // Update direction based on the string at currentPos.
                    direction = mapReader::updateDirection (nodeString, direction);

                    // If there was a problem with the map, then give up on
                    // this path
                    if (direction == Computer::NoDirection)
                        break;
                }

                // The wire went off of the board, which means stop following it
                else
                    break;
            }
        }
    }
}

#endif
