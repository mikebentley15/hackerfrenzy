#include <QtCore/QString>
#include <QtTest/QtTest>
#include <QString>
#include <QPoint>

#include "../node.h"
#include "../computer.h"

class NodeTest : public QObject
{
    Q_OBJECT
    
public:
    NodeTest();

private Q_SLOTS:
    void NodeDefaultConstructorTest();
    void NodeConstructorTest();
    void setTypeTest();
    void setCompTest();
    void setPosTest();

private:
    QString _type;
    Computer* _comp;
    QPoint _position;
    Node* _testNode;
    Computer* _testComp;
    int _xCoord;
    int _yCoord;

};

NodeTest::NodeTest()
{
    _xCoord = 2;
    _yCoord = 5;
    _type = "D1";
    _position = QPoint(_xCoord, _yCoord);
    _testComp = new Computer();
    _testNode = new Node(_position, _type, _testComp);
}

void NodeTest::NodeDefaultConstructorTest()
{
    Node* test = new Node();
    QVERIFY2(test!=NULL, "Default constructor failing");
}


void NodeTest::NodeConstructorTest()
{
    QVERIFY2(_testNode!=NULL, "Constructor failing");
    QVERIFY2(_testNode->getComp()==_testComp, "getComp failing");
    QVERIFY2(_testNode->getPos()==_position, "getPos failing");
    QVERIFY2(_testNode->getType()==_type, "getType failing");
}

void NodeTest::setTypeTest()
{
    QString newType = "L2";
    _testNode->setType(newType);
    QVERIFY2(_testNode->getType()==newType, "setType failing");
}

void NodeTest::setCompTest()
{
    Computer* newComp = new Computer();
    _testNode->setComp(newComp);
    QVERIFY2(_testNode->getComp()==newComp, "setComp failing");
}

void NodeTest::setPosTest()
{
    int newXCoord = 0;
    int newYCoord = 1;
    QPoint newPosition(newXCoord, newYCoord);
    _testNode->setPos(newPosition);
    QVERIFY2(_testNode->getPos().x()==newXCoord, "setPos failing with x coordinate");
    QVERIFY2(_testNode->getPos().y()==newYCoord, "setPos failing with y coordinate");
}


QTEST_APPLESS_MAIN(NodeTest)

#include "tst_nodetest.moc"
