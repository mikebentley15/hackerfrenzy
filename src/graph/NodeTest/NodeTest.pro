#-------------------------------------------------
#
# Project created by QtCreator 2012-04-10T19:25:58
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_nodetest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_nodetest.cpp \
    ../node.cpp \
    ../computer.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    ../node.h \
    ../computer.h
