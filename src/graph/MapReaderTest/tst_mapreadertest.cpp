#include <QtCore/QString>
#include <QtTest/QtTest>
#include <QSize>
#include <QPoint>

#include "../mapreader.h"
#include "../computer.h"
#include "../node.h"
#include "../graph.h"

class MapReaderTest : public QObject
{
    Q_OBJECT
    
public:
    MapReaderTest();
    
private Q_SLOTS:
    //mapreader tests
    void mapReaderDefaultConstructorTest();
    void loadMapByIntTest();
    void loadMapByStringTest();

    //graph tests
    void graphDefaultConstructorTest();
    void graphConstructorTest();
    void getNetworkTest();
    void isInsideTest();
    void getSizeTest();
    void atQPointTest();
    void atXYCoordTest();

private:
    mapReader* _test;
    QString _filename;
    QString _mapFilename;
    Graph* _testGraph;

    int graphXDim;

};

MapReaderTest::MapReaderTest()
{
    graphXDim = 10;
    _filename = "HFZ_MAPS.hfz";
    _mapFilename = "map1.hfz";
    _test = new mapReader(_filename);
    _testGraph = NULL;
}

void MapReaderTest::mapReaderDefaultConstructorTest()
{
    QVERIFY2(_test!=NULL, "Constructor failing");
}

void MapReaderTest::loadMapByIntTest()
{
    QVERIFY2(_testGraph==NULL, "Graph is already initialized");
    _testGraph = _test->loadMap(0);
    QVERIFY2(_testGraph!=NULL, "loadMap(int) is failing");
}

void MapReaderTest::loadMapByStringTest()
{
    _testGraph = NULL;
    QVERIFY2(_testGraph!=NULL, "Graph is already initialized");
    _testGraph = _test->loadMap(_mapFilename);
    QVERIFY2(_testGraph!=NULL, "loadMap(filename) is failing");
}

void MapReaderTest::graphDefaultConstructorTest()
{
    Graph* test = new Graph();
    QVERIFY2(test!=NULL, "Default constructor failing");
}

void MapReaderTest::graphConstructorTest()
{
    Graph* test = new Graph(graphXDim);
    QVERIFY2(test->getSize().height()==0, "Constructor is failing, height is incorrect");
    QVERIFY2(test->getSize().width()==graphXDim, "Constructor is failing, width is incorrect");
}

void MapReaderTest::getNetworkTest()
{
    Graph* test = new Graph(graphXDim);
    QVector<QVector<Node> >* networkTest = test->getNetwork();
    QVERIFY2(networkTest->size()==graphXDim, "network X dimension size is off");
    QVERIFY2(networkTest->at(0).size()==0, "network Y dimension size is off");
}

void MapReaderTest::isInsideTest()
{
    Graph* test = new Graph(graphXDim);
    QVERIFY2(test->isInside(5,0)==true, "isInside failing (indecies within bounds, returning false)");
    QVERIFY2(test->isInside(20,4)==false, "isInside failing (indecies out of bounds, returning true)");

}

void MapReaderTest::getSizeTest()
{
    QSize testSize (graphXDim,0);
    Graph* test = new Graph(graphXDim);
    QVERIFY2(test->getSize()==testSize, "getSize failing");
}

void MapReaderTest::atQPointTest()
{
    _testGraph = _test->loadMap("map1.hfz");
    QPoint getNode (0,3);
    Node* atTest =  _testGraph->at(getNode);
    QVERIFY2(atTest->getType()=="00", "at using QPoint failing");

}

void MapReaderTest::atXYCoordTest()
{
    QFAIL("Not implemented");
}

QTEST_APPLESS_MAIN(MapReaderTest)

#include "tst_mapreadertest.moc"
