#-------------------------------------------------
#
# Project created by QtCreator 2012-04-10T20:25:20
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_mapreadertest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_mapreadertest.cpp \
    ../mapreader.cpp \
    ../graph.cpp \
    ../computer.cpp \
    ../node.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    ../mapreader.h \
    ../graph.h \
    ../computer.h \
    ../node.h

OTHER_FILES += \
    ../map2.hfz \
    ../map1.hfz \
    ../HFZ_MAPS.hfz
