#include "attack.h"
#include "attacker/attacker.h"
#include "audit.h"
#include "character/character.h"
#include "game_engine/structures.h"
#include "graph/computer.h"


/*** =Public Methods= ***/

Attack::Attack(Computer *from, Computer *to, Graph *graph)
{
    Audit::get()->registerQObject (this);
    _from = from;
    _to = to;
    _owner = from->getController ();
    _graph = graph;

    _strengthLeft = 0;
    _attackersPerSecond = 1;
    _strengthPerAttacker = 1;
    _attackerSpeedFactor = 1;
    _millisSinceLastAttacker = 0;
    _isPaused = false;
}

Attack::~Attack()
{
}

Computer *Attack::from()
{
    return _from;
}

Computer *Attack::to()
{
    return _to;
}

Character *Attack::owner()
{
    return _owner;
}

Graph *Attack::graph()
{
    return _graph;
}

int Attack::strengthLeft()
{
    return _strengthLeft;
}

qreal Attack::attackersPerSecond()
{
    return _attackersPerSecond;
}

int Attack::strengthPerAttacker()
{
    return _strengthPerAttacker;
}

qreal Attack::attackerSpeedFactor()
{
    return _attackerSpeedFactor;
}

Color Attack::getColor()
{
    return _owner->getColor ();
}

bool Attack::isPaused()
{
    return _isPaused;
}



/*** =Public Slots= ***/

void Attack::updateTime(int elapsedMillis)
{
    // If the object is paused, then do nothing.
    if (_isPaused)
        return;

    _millisSinceLastAttacker += elapsedMillis;
    int millisPerAttacker = static_cast<int>(1000 / _attackersPerSecond);

    // Send out the attackers if it's time
    while (_millisSinceLastAttacker >= millisPerAttacker)
    {
        _millisSinceLastAttacker -= millisPerAttacker;

        // find out the strength of the attack
        int maxAttackStrength = qMin (_strengthLeft, _from->getAvailableStrength ());
        int attackStrength = qBound (0, _strengthPerAttacker, maxAttackStrength);
        if (attackStrength == 0)
            return;

        // Create the attacker
        Attacker* newAttacker = new Attacker (_from, _to, _graph,
                                              attackStrength,
                                              _attackerSpeedFactor);

        // update the strength that is left.
        _strengthLeft -= attackStrength;
        //_from->subtractStrength (attackStrength);  this is automatically done in the Computer class

        // update the attacker with the amount of left-over milliseconds.
        newAttacker->unpause ();
        newAttacker->updateTime (_millisSinceLastAttacker);

        // Mark this object as the parent to cause it to be deleted when this
        // attack is deleted.
        newAttacker->setParent (this);

        emit sendAttacker (newAttacker);
    }

    if (_strengthLeft == 0)
        emit attackIsDone ();
}

void Attack::increaseStrength(int toAdd)
{
    _strengthLeft += toAdd;
}

void Attack::setStrength(int newStrength)
{
    _strengthLeft = newStrength;
}

void Attack::clearStrength()
{
    _strengthLeft = 0;
}

void Attack::setAttackersPerSecond(qreal attackersPerSecond)
{
    _attackersPerSecond = attackersPerSecond;
}

void Attack::setStrengthPerAttacker(int strengthPerAttacker)
{
    _strengthPerAttacker = strengthPerAttacker;
}

void Attack::setAttackerSpeedFactor (qreal attackerSpeedFactor)
{
    _attackerSpeedFactor = attackerSpeedFactor;
}

void Attack::setOwner(Character *newOwner)
{
    if (newOwner != _owner)
    {
        _owner = newOwner;
        _strengthLeft = 0;
    }
}

void Attack::pause()
{
    _isPaused = true;
}

void Attack::unpause()
{
    _isPaused = false;
}

void Attack::setPaused(bool toPause)
{
    // call pause() or unpause() based on the value of toPause
    toPause ? pause() : unpause();
}


