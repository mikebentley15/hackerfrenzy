#include <qmath.h>

#include "attacker/trojan.h"

#include "attacker/attacker.h"
#include "graph/computer.h"
#include "graph/graph.h"
#include "graph/mapreader.h"

Trojan::Trojan(Computer* from, Computer* destination, Graph* graph,
               int quantity, qreal speedFactor)
//call the attacker constructor
    : Attacker (from, destination, graph, quantity, speedFactor)

{
    //need a way to implement special abilities
    //trojan takes over a neutral comptuer without
    //depleting the neutral troops (neutral troops come
    //under attacking player's control)
}
