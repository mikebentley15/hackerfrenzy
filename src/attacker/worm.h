#ifndef WORM_H
#define WORM_H

#include <qmath.h>

#include "attacker.h"
#include "graph/computer.h"
#include "graph/graph.h"
#include "graph/mapreader.h"

class Worm : public Attacker
{
public:
    Worm(Computer* from, Computer* destination, Graph* graph,
         int quantity, qreal speedFactor);
};

#endif // WORM_H
