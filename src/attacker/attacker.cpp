#include "attacker/attacker.h"

#include "audit.h"
#include "graph/computer.h"
#include "graph/graph.h"
#include "graph/mapreader.h"

#include <qmath.h>

/*** =Public Methods= ***/

Attacker::Attacker(Computer *from, Computer *destination, Graph *graph,
                   int quantity, qreal speedFactor)
{
    Audit::get()->registerQObject(this);
    _from = from;
    _destination = destination;
    _owner = from->getController ();
    _graph = graph;
    _quantity = quantity;
    _speedFactor = speedFactor;
    
    // Start in the center of where the owner computer is
    _pos = QPointF(_from->getParentNode()->getPos());
    // Find the direction to send this attacker
    _direction = _from->getDirection (_destination);
    // set the owner
    _controller = from->getController ();

    _destPos = _destination->getParentNode()->getPos();
    
    // start out as unpaused.  To pause call the pause() method.  The
    // reason is because the updateTime() method is the only thing
    // effected and you can decide when to connect a signal to that slot
    _isPaused = false;

    setupPathToTravel();
}

Attacker::~Attacker()
{
    foreach (PathwayPiece* piece, _pathToTravel)
    {
        delete piece;
        _pathToTravel.pop_back ();
    }
}

QPointF Attacker::pos() const
{
    return _pos;
}

Computer* Attacker::from() const
{
    return _from;
}

Computer* Attacker::destination() const
{
    return _destination;
}

Character *Attacker::owner() const
{
    return _owner;
}

qreal Attacker::speedFactor() const
{
    return _speedFactor;
}

int Attacker::quantity () const
{
    return _quantity;
}

Computer::Direction Attacker::currentDirection() const
{
    return _direction;
}

Character *Attacker::controller() const
{
    return _controller;
}

bool Attacker::isPaused() const
{
    return _isPaused;
}



/*** =Public Slots= ***/

void Attacker::updateTime (int elapsedMillis)
{
    if (!_isPaused)
    {
        QPointF oldPos = _pos;
        Computer::Direction oldDirection = _direction;

        // Travel one node at a time until we've traveled the whole distance
        qreal distanceLeft = elapsedMillis * _speedFactor / 1000.0;

        forever
        {
            // Obtain the current path piece
            if (_pathToTravel.size() == 0)
                return;
            PathwayPiece* currentPathPiece = _pathToTravel.last ();
            _direction = currentPathPiece->direction;

            // If the path is finished, then finish it off
            if (currentPathPiece->travelDistanceLeft <= distanceLeft)
            {
                _pos = currentPathPiece->end;
                distanceLeft -= currentPathPiece->travelDistanceLeft;

                _pathToTravel.pop_back (); // remove the finished piece
                delete currentPathPiece;

                // If we've reached our destination, then emit some signals and be done
                if (isAtDestination ())
                {
                    emit posChanged (this);
                    emit posChanged (_pos);
                    emit reachedDestination (this);
                    return;
                }
            }
            // If the path isn't finished, then update where you are in the path
            else
            {
                // Move the position
                QPointF directionVector =
                        (currentPathPiece->end - currentPathPiece->begin)
                        / currentPathPiece->length;
                _pos += distanceLeft * directionVector;

                // update the distance left
                currentPathPiece->travelDistanceLeft -= distanceLeft;

                break; // we are done traveling so exit the loop
            }
        }


//        while (distanceLeft > 0)
//        {
//            // Find the distance to the next node
//            qreal distanceToNextNode = calculateDistanceToNextNode();

//            // Travel to the next node if there is enough distance left
//            if (distanceToNextNode <= distanceLeft)
//            {
//                travelInCurrentDirection (distanceToNextNode);

//                // If we've reached our destination, then emit some signals and be done
//                if (isAtDestination())
//                {
//                    emit posChanged (this);
//                    emit posChanged (_pos);
//                    emit reachedDestination (this);
//                    return;
//                }

//                // Update the direction based on this new node
//                _direction = mapReader::updateDirection (
//                            this->_graph->at((_pos - QPointF(0.5,0.5)).toPoint())->getType(),
//                            oldDirection);

//                if (_direction == Computer::NoDirection)
//                    return;

//                // Update the remaining distance to travel
//                distanceLeft -= distanceToNextNode;
//            }
//            // Travel only the remaining distance and be done with this loop
//            else
//            {
//                travelInCurrentDirection (distanceLeft);
//                distanceLeft = 0;
//                break;
//            }
//        }

        // If the position changed, then send out the signal
        if (oldPos != _pos)
        {
            emit posChanged (this);
            emit posChanged (_pos);
        }
        // If the direction changed, then send out the signal
        if (oldDirection != _direction)
            emit directionChanged (this);
    }
}

void Attacker::setPos (QPointF pos)
{
    if (_pos != pos)
    {
        _pos = pos;
        emit posChanged (this);
        emit posChanged (_pos);
    }
}

void Attacker::setOwner(Character *newOwner)
{
    if (newOwner != _owner)
    {
        _owner = newOwner;
        emit ownerChanged (this);
    }
}

void Attacker::setSpeedFactor (qreal newSpeedFactor)
{
    _speedFactor = newSpeedFactor;
}

void Attacker::setQuantity (int newQuantity)
{
    _quantity = newQuantity;
}

void Attacker::setDirection (Computer::Direction direction)
{
    if (direction != _direction)
    {
        _direction = direction;
        emit directionChanged (this);
    }
}

void Attacker::setController (Character *newController)
{
    if (_controller != newController)
    {
        _controller = newController;
        emit controllerChanged (this);
    }
}

void Attacker::pause ()
{
    _isPaused = true;
}

void Attacker::unpause ()
{
    _isPaused = false;
}

void Attacker::setPaused (bool toPause)
{
    _isPaused = toPause;
}


/*** =Private Methods= ***/

qreal Attacker::calculateDistanceToNextNode ()
{
    qreal distance;

    // Since _pos is in the center of the pipe, do a transformation to simplify calculations
    QPointF nonCenteredPos = _pos - QPointF(0.5, 0.5);
    switch (_direction)
    {
      case Computer::North:
        distance = nonCenteredPos.y() + 1 - qCeil(nonCenteredPos.y());
        break;
      case Computer::South:
        distance = qFloor(nonCenteredPos.y()) + 1 - nonCenteredPos.y();
        break;
      case Computer::East:
        distance = qFloor(nonCenteredPos.x()) + 1 - nonCenteredPos.x();
        break;
      case Computer::West:
        distance = nonCenteredPos.x() + 1 - qCeil(nonCenteredPos.x());
        break;
      case Computer::NoDirection:
      default:
        distance = 0;
    }

    // Protect against weird floating point stuff
    if (distance == 0 &&
           (nonCenteredPos.x() != qFloor(nonCenteredPos.x()) ||
            nonCenteredPos.y() != qFloor(nonCenteredPos.y())))
    {
        nonCenteredPos.setX(qFloor(nonCenteredPos.x()));
        nonCenteredPos.setY(qFloor(nonCenteredPos.y()));
        _pos = nonCenteredPos + QPointF(0.5, 0.5);
        return 1;
    }

    return distance;
}

void Attacker::travelInCurrentDirection (qreal distance)
{
    switch (_direction)
    {
      case Computer::North:
        _pos.setY(_pos.y() - distance);
        break;
      case Computer::South:
        _pos.setY(_pos.y() + distance);
        break;
      case Computer::East:
        _pos.setX(_pos.x() + distance);
        break;
      case Computer::West:
        _pos.setX(_pos.x() - distance);
        break;
      case Computer::NoDirection:
      default:
        break;
    }
}

bool Attacker::isAtDestination ()
{
    // TODO: make this more robust than simply truncate down
    return _destination == _graph->at(qFloor(_pos.x()), qFloor(_pos.y()))->getComp();
}

void Attacker::setupPathToTravel ()
{
    QPointF beginning = _pos;
    _pos = _destPos;
    _direction = _destination->getDirection(_from);

    // travel from the destination to the from to build the path
    while (!qFuzzyCompare(_pos.x(), beginning.x()) || !qFuzzyCompare(_pos.y(), beginning.y()))
    {
        // Update the direction
        QString nodeType = _graph->at(_pos.toPoint())->getType();
        if (nodeType[0] == '/' || nodeType[0] == '\\')
            _direction = mapReader::updateDirection(nodeType, _direction);

        // Redundancy checking to make sure we don't do something wrong.
        if (_direction == Computer::NoDirection)
        {
            Audit::get()->log("In setupPathToTravel(), there was a Computer::NoDirection seen");
            break;
        }

        PathwayPiece* piece = new PathwayPiece();
        piece->end = _pos;

        // Travel until we reach an elbow or our destination
        qreal travelDistance = 0;
        do
        {
            travelInCurrentDirection (1);
            nodeType = _graph->at(_pos.toPoint())->getType();
            travelDistance++;
        } while ((nodeType[0] != '/' && nodeType[0] != '\\') &&
                 (!qFuzzyCompare(_pos.x(), beginning.x()) || !qFuzzyCompare(_pos.y(), beginning.y())));

        piece->begin = _pos;
        piece->direction = Computer::oppositeDirection(_direction);
        piece->length = qRound(travelDistance);
        piece->travelDistanceLeft = piece->length;

        _pathToTravel.append(piece);
    }

    _direction = _from->getDirection(_destination);
}
