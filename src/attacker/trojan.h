#ifndef TROJAN_H
#define TROJAN_H

#include <qmath.h>

#include "attacker/attacker.h"
#include "graph/computer.h"
#include "graph/graph.h"
#include "graph/mapreader.h"

class Trojan : public Attacker
{
public:
    Trojan(Computer *from, Computer *destination, Graph *graph,
           int quantity, qreal speedFactor);
};

#endif // TROJAN_H
