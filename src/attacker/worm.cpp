#include <qmath.h>

#include "attacker/worm.h"

#include "attacker/attacker.h"
#include "graph/computer.h"
#include "graph/graph.h"
#include "graph/mapreader.h"

Worm::Worm(Computer* from, Computer* destination, Graph* graph,
           int quantity, qreal speedFactor)
    //call the attacker constructor
    : Attacker (from, destination, graph, quantity, speedFactor)
{
    //need a way to implement special abilities
    //worm class slows movement of enemy troops in connectors to
    //adjacent computers
}
