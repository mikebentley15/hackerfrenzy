/**
 * File:    attacker.h
 * Authors: Michael Bentley
 * Team:    Sinister Intentions (all rights reserved)
 * Created: Saturday, April 7 2012
 */

#ifndef ATTACKER_H
#define ATTACKER_H


#include <QObject>
#include <QPointF>
#include "macros.h"
#include "graph/computer.h"

class Graph;
class Character;



/**
 * Abstract class for the attackers from one computer to another.
 * 
 * Represents a single visible item on the game screen that can
 * represent any quantity of attackers.
 */
class Attacker : public QObject // Blip class
{
    Q_OBJECT

    struct PathwayPiece
    {
        QPointF begin;
        QPointF end;
        Computer::Direction direction;
        int length; // = norm(end - begin)
        qreal travelDistanceLeft;
    };

public:
    /**
     * Constructor
     * 
     * Creates an attacker that travels from the from computer to the
     * destination computer.  The attacker starts out as not moving,
     * you get to tell it when to start moving so that you can use the
     * setters and decide when it's okay to start moving.
     * 
     * @param from
     *   Starting computer (will infer ownership from this computer)
     * @param destination
     *   Destination computer
     * @param graph
     *   The game map
     * @param quantity
     *   How many attackers this object represents
     * @param speedFactor
     *   How many graph nodes this attacker travels in a second
     */
    Attacker (Computer* from, Computer *destination, Graph *gameMap,
              int quantity = 1, qreal speedFactor = 20);
    
    ~Attacker ();

    // Returns current position according to the game map graph
    QPointF pos() const;
    
    // Returns the computer from which this attacker originated
    Computer* from() const;
    
    // Returns the computer that this attacker is being sent to
    Computer* destination() const;

    // Returns the owner of this attacker.  Usually the one who created it.
    Character* owner() const;
    
    // Returns the speed factor, which is graph nodes / second
    qreal speedFactor() const;
    
    // Returns how many attackers this one object represents
    int quantity() const;
    
    /**
     * Returns the current moving direction.
     * (or direction it would be moving if it is currently paused)
     */
    Computer::Direction currentDirection() const;
    
    // Returns who sent this attacker (i.e. who it belongs to)
    Character* controller() const;
    
    // True if the attacker is paused (meaning not moving)
    bool isPaused() const;
    
public slots:
    
    /**
     * Updates the position of this attacker based on the path 
     * it's traveling, the amount of time since its last update
     * and the speedFactor.
     * 
     * Note: if this object is paused, then this method does
     * absolutely nothing.
     * 
     * @param elapsedMillis
     *   Number of milliseconds since the last update
     */
    void updateTime (int elapsedMillis);
    
    // Sets the position
    void setPos (QPointF pos);
    
    // Sets the owner, and hence will change its color (hopefully)
    void setOwner (Character* newOwner);

    // Sets the speed factor (graph nodes / second)
    void setSpeedFactor (qreal newSpeedFactor);
    
    // Sets how many attackers this one object represents
    void setQuantity (int newQuantity);
    
    /**
     * Sets the current traveling direction
     *
     * Be careful not to set this incorrectly.  The possible directions
     * are limited by the current position.
     */
    void setDirection (Computer::Direction direction);

    /**
     * Sets the controller to a different computer.
     * Maybe useful for a special ability or something.
     */
    void setController (Character* newController);
    
    /**
     * Pauses the object causing updateTime to do nothing.
     *
     * This is useful if you want to pause the game or if a special ability
     * causes all attackers to be paused for a certain period of time.
     */
    void pause ();
    
    /**
     * Unpauses the object allowing it to update its position when
     * updateTime() is called.
     */
    void unpause ();
    
    // if true is sent, calls pause(), otherwise calls unpause()
    void setPaused (bool toPause);
    
signals:
    void controllerChanged (Attacker* thisAttacker = NULL);
    void posChanged (Attacker* thisAttacker = NULL);
    void posChanged (QPointF newPos);
    void directionChanged (Attacker* thisAttacker = NULL);
    void reachedDestination (Attacker* thisAttacker = NULL);
    void ownerChanged (Attacker* thisAttacker = NULL);
    
private:
    /**
     * _pos           Location according to the Graph size
     * _from          Attacking computer
     * _destination   Computer being attacked
     * _owner         Character who sent this attacker
     * _speedFactor   How many nodes it travels per second
     * _graph         Graph object representing the level and paths
     * _quantity      How many attackers this object represents
     * _direction     Current moving direction
     * _controller    The entity that owns this attacker
     * _isPaused      True means this attacker is paused
     */
    QPointF _pos;
    QPointF _destPos;
    Computer *_from;
    Computer *_destination;
    Character *_owner;
    qreal _speedFactor;
    Graph *_graph;
    int _quantity;
    Computer::Direction _direction;
    Character* _controller;
    bool _isPaused;
    QVector<PathwayPiece*> _pathToTravel;
    
    qreal calculateDistanceToNextNode ();
    void travelInCurrentDirection (qreal distance);
    bool isAtDestination ();
    void setupPathToTravel ();

    // Do not allow this object to be copied or assigned with the = operator
    DISALLOW_COPY_AND_ASSIGN (Attacker);
};


#endif // ATTACKER_H
