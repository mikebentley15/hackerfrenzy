#include <qmath.h>

#include "attacker/virus.h"

#include "attacker/attacker.h"
#include "graph/computer.h"
#include "graph/graph.h"
#include "graph/mapreader.h"

Virus::Virus(Computer* from, Computer* destination, Graph* graph,
             int quantity, qreal speedFactor)
    //call the attacker constructor
    : Attacker (from, destination, graph, quantity, speedFactor)
{

    //need a way to implement special abilites
    //virus multiplies 2x as fast as other attackers

}
