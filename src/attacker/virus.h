#ifndef VIRUS_H
#define VIRUS_H

#include <qmath.h>

#include "attacker/attacker.h"
#include "graph/computer.h"
#include "graph/graph.h"
#include "graph/mapreader.h"

class Virus : public Attacker
{
public:
    Virus(Computer *from, Computer *destination, Graph *graph,
          int quantity, qreal speedFactor);
};

#endif // VIRUS_H
