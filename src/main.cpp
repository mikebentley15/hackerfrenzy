#include <QApplication>
#include <QtOpenGL/QGLWidget>
#include <QtGlobal>
#include <QTime>

#include "gamewindow.h"
#include "audit.h"

int main (int argCount, char* argList [])
{
    // Seed the random number generator
    QTime midnight = QTime(0, 0, 0, 0);
    QTime current = QTime::currentTime ();
    qsrand (midnight.secsTo (current) * current.msec());

    // Create the application
    QApplication *app = new QApplication(argCount, argList);
    app->setApplicationName("hackerfrenzy");
    GameWindow *game = new GameWindow();

    // Show the window
    //game->setWindowFlags (Qt::FramelessWindowHint); // don't show the window frame

#ifndef USE_FULLSCREEN
    game->show ();
    game->resize(1280, 780);
#else
    game->showFullScreen (); // take the entire screen
#endif

    // Wait until the application closes
    int returnValue = app->exec();

    // Delete the allocated memory
    delete game;
    delete app;

    // Delete the audit object which prints out a final report
    delete Audit::get();

    // return the QApplication exit code
    return returnValue;
}
