#include "gamewindow.h"

#include <QtGui>
#include <QtDebug>
#include <QString>

#include "audit.h"
#include "game_engine/fastloop.h"
#include "game_engine/gamesound.h"
#include "game_engine/levelloader.h"
#include "game_engine/SpriteRendering/texture.h"
#include "game_engine/SpriteRendering/texture_manager.h"
#include "game_engine/statesystem.h"
#include "state/levelselectionstate.h"
#include "state/shutdownstate.h"
#include "state/splashscreenstate.h"
#include "state/titlemenustate.h"



/*** =Public Methods= ***/

GameWindow::GameWindow(QWidget *parent)
    : QGLWidget(QGLFormat(QGL::SampleBuffers), parent)
{
    Audit::get()->registerQObject (this);

    this->setMouseTracking(true);

    _fastLoop = new FastLoop<GameWindow> (this, &GameWindow::gameLoop);

    _textureManager = new TextureManager();
    _system = new StateSystem (this);
    _soundCreator = new GameSound (this);
}

GameWindow::~GameWindow ()
{
    delete _fastLoop;
    delete _system;
}

void GameWindow::gameLoop (double elapsedTime)
{
  _system->update(elapsedTime);
  _system->render();
  this->updateGL();
}



/*** =Protected Methods= ***/

void GameWindow::closeEvent            (QCloseEvent* event)
{
    _system->closeEvent(event);
    if (!event->isAccepted())
        QGLWidget::closeEvent(event);
}

void GameWindow::dragEnterEvent        (QDragEnterEvent* event)
{
    _system->dragEnterEvent(event);
    if (!event->isAccepted())
        QGLWidget::dragEnterEvent(event);
}

void GameWindow::dragLeaveEvent        (QDragLeaveEvent* event)
{
    _system->dragLeaveEvent(event);
    if (!event->isAccepted())
        QGLWidget::dragLeaveEvent(event);
}

void GameWindow::dragMoveEvent         (QDragMoveEvent* event)
{
    _system->dragMoveEvent(event);
    if (!event->isAccepted())
        QGLWidget::dragMoveEvent(event);
}

void GameWindow::dropEvent             (QDropEvent* event)
{
    _system->dropEvent(event);
    if (!event->isAccepted())
        QGLWidget::dropEvent(event);
}

void GameWindow::enterEvent            (QEvent* event)
{
    _system->enterEvent(event);
    if (!event->isAccepted())
        QGLWidget::enterEvent(event);
}

void GameWindow::keyPressEvent         (QKeyEvent* event)
{
    _system->keyPressEvent(event);
    if (!event->isAccepted())
        QGLWidget::keyPressEvent(event);
}

void GameWindow::keyReleaseEvent       (QKeyEvent* event)
{
    _system->keyReleaseEvent(event);
    if (!event->isAccepted())
        QGLWidget::keyReleaseEvent(event);
}

void GameWindow::leaveEvent            (QEvent* event)
{
    _system->leaveEvent(event);
    if (!event->isAccepted())
        QGLWidget::leaveEvent(event);
}

void GameWindow::mouseDoubleClickEvent (QMouseEvent* event)
{
    _system->mouseDoubleClickEvent(event);
    if (!event->isAccepted())
        QGLWidget::mouseDoubleClickEvent(event);
}

void GameWindow::mouseMoveEvent        (QMouseEvent* event)
{
    _system->mouseMoveEvent(event);
    if (!event->isAccepted())
        QGLWidget::mouseMoveEvent(event);
}

void GameWindow::mousePressEvent       (QMouseEvent* event)
{
    _system->mousePressEvent(event);
    if (!event->isAccepted())
        QGLWidget::mousePressEvent(event);
}

void GameWindow::mouseReleaseEvent     (QMouseEvent* event)
{
    _system->mouseReleaseEvent(event);
    if (!event->isAccepted())
        QGLWidget::mouseReleaseEvent(event);
}

void GameWindow::resizeEvent           (QResizeEvent* event)
{
    Setup2DGraphics(this->width(), this->height());
    _system->resizeEvent(event);
    if (!event->isAccepted())
        QGLWidget::resizeEvent(event);
}

void GameWindow::tabletEvent           (QTabletEvent* event)
{
    _system->tabletEvent(event);
    if (!event->isAccepted())
        QGLWidget::tabletEvent(event);
}

void GameWindow::wheelEvent            (QWheelEvent* event)
{
    _system->wheelEvent(event);
    if (!event->isAccepted())
        QGLWidget::wheelEvent(event);
}




/*** =Private Methods= ***/

void GameWindow::initializeGL()
{
    glEnable (GL_TEXTURE_2D);
    glShadeModel (GL_SMOOTH);                        // Enable Smooth Shading
    glClearDepth (1.0f);                             // Depth Buffer Setup

    LoadTextures ();

    Setup2DGraphics (this->width(), this->height());

    LoadStates ();
    loadSounds ();

    _fastLoop->start();
}

void GameWindow::paintGL()
{

}

void GameWindow::Setup2DGraphics (int width, int height)
{
    double halfWidth = width/2;
    double halfHeight = height/2;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-halfWidth, halfWidth, -halfHeight, halfHeight, -100, 100);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void GameWindow::FlipImage (QImage &image)
{
    //Use a transform to alter the state of the image
    QTransform flipImage;
    //The Image is upside down so rotate it to be facing up
    flipImage.rotate(180);
    //Apply the transform to the image
    image = image.transformed(flipImage);
    //The image has been flipped so you are looking from the wrong side
    flipImage.rotate(180, Qt::XAxis);
    //Apply the transform again
    image = image.transformed(flipImage);
}

void GameWindow::LoadStates ()
{
    // TODO: replace the tutorial state with an actual tutorial state

    State *splash, *title_menu, *level_select, *game_play, *tutorial, *shutdown;
    splash = new SplashScreenState(_system, _textureManager, _soundCreator);
    title_menu = new TitleMenuState(_system, _textureManager, _soundCreator);
    game_play = new LevelLoader(0, _system, _textureManager, _soundCreator);
    level_select = new LevelSelectionState(_system, _textureManager, _soundCreator,
                                           qobject_cast<LevelLoader*>(game_play));
    tutorial = new LevelLoader(3, _system, _textureManager, _soundCreator);
    shutdown = new ShutdownState(_system, _textureManager, this);

    _system->addState ("splash",       splash);
    _system->addState ("title_menu",   title_menu);
    _system->addState ("level_select", level_select);
    _system->addState ("game_play",    game_play);
    _system->addState ("tutorial",     tutorial);
    _system->addState ("shutdown",     shutdown);

    //Change the state of the game
    //_system->changeState ("splash");
    //_system->changeState ("level_one");
    _system->changeState ("title_menu");
}

void GameWindow::LoadTextures()
{
    loadTextureDirectory (":/images/Attacks",     4, "attacktexture");
    loadTextureDirectory (":/images/Backgrounds", 2, "backgroundtexture");
    loadTextureDirectory (":/images/Connections", 6, "connecttexture");
    loadTextureDirectory (":/images/Devices",     7, "devicetexture");
    loadTextureDirectory (":/images/TitleMenu",   4, "titlemenutexture");
    loadTextureDirectory (":/images/Other",       2, "othertexture");
    loadTextureDirectory (":/images/Font",        1, "fonttexture");
    loadTextureDirectory (":/images/Splash",      5, "splashtexture");
    loadTextureDirectory (":/images/Levels",      6, "leveltexture");

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

void GameWindow::loadTextureDirectory (const QString &directory,
                                       int textureCount,
                                       const QString &baseLookupName)
{
    //Load the textures used for the devices
    QImage* textures = new QImage[textureCount]();
    GLuint* textureAdd = new GLuint[textureCount]();
    for (int i = 0; i < textureCount; i++ )
    {
        QString file = QString(directory + "/texture%1.png").arg(i+1);
        
        textures[i].load(file);
        FlipImage(textures[i]);
        textureAdd[i] = this->bindTexture(textures[i]);
        
        QString lookup = QString(baseLookupName + "%1").arg(i+1);
        _textureManager->LoadTexture(lookup, textureAdd[i], textures[i]);
    }

    delete[] textures;   textures = NULL;
    delete[] textureAdd; textureAdd = NULL;
}

void GameWindow::loadSounds()
{
    _soundCreator->addSound ("background1", "sounds/bg1.mp3");
    _soundCreator->addSound ("background2", "sounds/bg2.mp3");
    _soundCreator->addSound ("boom",        "sounds/boom.mp3");
    _soundCreator->addSound ("click",       "sounds/click.mp3");
    _soundCreator->addSound ("impact",      "sounds/impact.mp3");
}


