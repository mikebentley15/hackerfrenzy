#ifndef __MACROS_H_
#define __MACROS_H_

/**
 * Put into a class in the private section to not allow the object
 * to be copied or to use the assignment operator.  It forces you
 * to fix attempts in the compilation process.
 *
 * Usage Example:
 *
 * class SomeClass
 * {
 * private:
 *   DISALLOW_COPY_AND_ASSIGN (SomeClass);
 * };
 *
 * @param Class
 *   The name of the class this is placed in
 */

#ifndef DISALLOW_COPY_AND_ASSIGN
#define DISALLOW_COPY_AND_ASSIGN(Class)         \
    Class (const Class &other);                 \
    Class& operator = (const Class &other)
#endif // DISALLOW_COPY_AND_ASSIGN


/**
 * If defined, then will have a window showing the audit
 * information during runtime.  Simply comment out this
 * define and the window will not show up.
 */
//#define AUDIT_WINDOW

/**
 * If defined, then will use the audit class to write to
 * a log file.  To disable the audit class, simply comment
 * out this line.
 */
//#define USE_AUDIT

/**
 * If defined, then will show the game in fullscreen mode.
 * To disable this feature, simply comment out the following
 * line.
 *
 * Note: you must not use fullscreen mode when debugging, it
 * simply does not work.
 */
//#define USE_FULLSCREEN

/**
 * If defined, this will have the config window show up at the title screen.
 * To disable this feature, simply comment out this line.
 */
//#define TITLE_MENU_CONFIG

/**
 * If defined, this will have the change map form window show up at the
 * game play state.  To disable this feature, simply comment out this line.
 */
//#define CHANGE_MAP_FORM

/**
 * If defined, the GameSound class will play sounds.
 * To disable this feature, simply comment out this line.
 */
//#define USE_SOUNDS


#endif // __MACROS_H_
