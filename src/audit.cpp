#include "audit.h"
#include "macros.h"

#include <iostream>
#include <QtDebug>
#include <QFile>
#include <QHash>
#include <QString>
#include <QDateTime>
#include <QTextStream>
#include <QTextEdit>
#include <QWidget>



#ifdef USE_AUDIT
/*** =Static Members= ***/
// Private
Audit* Audit::_theSingletonObject = NULL;

// Public
Audit* Audit::get ()
{
    if (_theSingletonObject == NULL)
        _theSingletonObject = new Audit();
    return _theSingletonObject;
}

// Protected
QString Audit::nameOfQObjectClass (QObject *object)
{
    QString name;
    name = (object->metaObject()->className());
    return name;
}


/*** =Public Methods= ***/

Audit::~Audit ()
{
    // Prepare one final message to be logged
    QString finalMessage = "\n"
        "Final analysis:\n"
        "    Total Object Count = " + QString::number(_totalObjectCount) + "\n";
    QHash<QString, int>::const_iterator i = _objectTracker->constBegin();
    while (i != _objectTracker->constEnd())
    {
        finalMessage += "    " + i.key() + " Objects = " 
            + QString::number(i.value()) + "\n";
        ++i;
    }

#ifdef AUDIT_WINDOW
    _logWindow->close();
    _logWindow->deleteLater();
    _logWindow = NULL;
#endif

    log (finalMessage);


    delete _objectTracker;
    _objectTracker = NULL;
    delete _qObjectNames;
    _qObjectNames = NULL;
}

int Audit::getTotalObjectCount () const
{
    return _totalObjectCount;
}

void Audit::objectCreated (const QString &type)
{
    _totalObjectCount++;
    _objectTracker->insert(type, _objectTracker->value(type, 0) + 1);
  
    QString logMessage = "Created " + type
        + "\n    total objects = " + QString::number(_totalObjectCount)
        + "\n    " + type + " objects = "
        + QString::number(_objectTracker->value(type))
        + "\n";
  
    log (logMessage);
}

void Audit::objectDestroyed (const QString &type)
{
    _totalObjectCount--;
    _objectTracker->insert(type, _objectTracker->value(type, 0) - 1);
  
    QString logMessage = "Destroyed " + type
        + "\n    total objects = " + QString::number(_totalObjectCount)
        + "\n    " + type + " objects = "
        + QString::number(_objectTracker->value(type))
        + "\n";
  
    log (logMessage);
}

void Audit::registerQObject (QObject *object)
{
    QString name = nameOfQObjectClass(object);
    objectCreated (name);
    _qObjectNames->insert (object, name);
    connect (object, SIGNAL(destroyed(QObject*)),
             this,   SLOT(registerQObjectAsDestroyed(QObject*)));
}

void Audit::log (const QString &message) const
{
    if (_logFile == NULL)
        std::cout << message.toLocal8Bit().data();
    else
    {
        QTextStream stream (_logFile);
        stream << message;
#ifdef AUDIT_WINDOW
        if (_logWindow != NULL)
            _logWindow->append(message);
#endif
    }
}


/*** =Private Slots= ***/

void Audit::registerQObjectAsDestroyed (QObject *object)
{
    objectDestroyed (_qObjectNames->value (object, "QObject"));
}


/*** =Private Methods= ***/

Audit::Audit ()
{
    _totalObjectCount = 0;
    _objectTracker = new QHash<QString, int>();
    _qObjectNames = new QHash<QObject*, QString>();

    startLog();
}

void Audit::startLog ()
{
    _logFile = new QFile(".audit_log", this);
  
    if (!_logFile->open(QIODevice::Append | QIODevice::Text))
        _logFile = NULL;
  
    QString startingMessage = "\n"
        "------------------------------------------------\n"
        + QDateTime::currentDateTime().toString()
        + "\n\n";

#ifdef AUDIT_WINDOW // This is defined in macros.h
    // Create and display the log window
    _logWindow = new QTextEdit();
    _logWindow->setReadOnly(true);
    _logWindow->setLineWrapMode(QTextEdit::WidgetWidth);
    _logWindow->resize(500, 700);
    _logWindow->show();
#else
    _logWindow = NULL; // don't show a log window
#endif

    log (startingMessage);
}

#else

Audit* Audit::_theSingletonObject = NULL;
Audit* Audit::get ()
{
    if (_theSingletonObject == NULL)
        _theSingletonObject = new Audit();
    return _theSingletonObject;
}
QString Audit::nameOfQObjectClass (QObject *object) { Q_UNUSED(object); return ""; }
Audit::~Audit () {}
int Audit::getTotalObjectCount () const { return 0; }
void Audit::objectCreated (const QString &type) { Q_UNUSED(type); }
void Audit::objectDestroyed (const QString &type) { Q_UNUSED(type); }
void Audit::registerQObject (QObject *object) { Q_UNUSED(object); }
void Audit::log (const QString &message) const { Q_UNUSED(message); }
void Audit::registerQObjectAsDestroyed (QObject *object) { Q_UNUSED(object); }
Audit::Audit () {}
void Audit::startLog () {}

#endif // USE_AUDIT


