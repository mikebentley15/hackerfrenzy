#ifndef NEUTRALCHARACTER_H
#define NEUTRALCHARACTER_H

#include "character/character.h"
#include "game_engine/structures.h"
#include <QString>


/**
 * This class represents a Neutral player.  It doesn't need to
 * implement anything above what the Character class does, simply
 * to be another class that can be checked for.
 */
class NeutralPlayer : public Character
{
    Q_OBJECT
public:
    NeutralPlayer(const QString &name, const Color &playerColor);
};

#endif // NEUTRALCHARACTER_H
