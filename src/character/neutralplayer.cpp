#include "character/neutralplayer.h"

NeutralPlayer::NeutralPlayer(const QString &name, const Color &playerColor)
    : Character(name, playerColor)
{ }
