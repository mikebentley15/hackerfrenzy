/**
    Authors: Brenden Tyler
    CS 3505 Spring 2012

    Defines methods and variables used for a player of the game
    Player object tracks someone (or something) that controls malware

  */

#ifndef CHARACTER_H
#define CHARACTER_H

#include <QObject>
#include <QString>
#include <QColor>
#include <QSet>

#include "game_engine/structures.h"

class Computer;

class Character : public QObject
{
    Q_OBJECT
    
public:
    Character();

    //constructor
    Character(const QString &name, const Color &playerColor);

    //returns a set of computers that the player controls
    QSet<Computer*> getControlledSet() const;

    Color getColor() const;

    //returns whether or not the computer is controlled by this player
    bool isController (Computer* isControlled) const;

    //name of the player
    QString name() const;

    //changes playerName to newName
    void changePlayerName(const QString &newName);

    //tracks the computer controlled by this player
    int computersControlled () const;

public slots:

    //adds the computer to the list of computers controlled
    void addToControlled(Computer* toAdd);

    //remove the computer from the list of computers controlled
    void removeFromControlled(Computer* toRemove);


signals:
    void lostAllComputers (Character* thisCharacter);


protected:
    //the player's name (should be a unique identifier)
    QString _playerName;

    //a list of comptuers controlled by the player
    QSet<Computer*> _controlled;

    //color to represent character
    Color _playerColor;
};

#endif // CHARACTER_H
