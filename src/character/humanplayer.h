#ifndef HUMANPLAYER_H
#define HUMANPLAYER_H

#include "character/character.h"
#include "game_engine/structures.h"
#include <QString>

/**
 * Represents a human player.  For now, we only have one human player which
 * is the user of this program.  For now, this human player will be able
 * to control all of the human controlled computers.
 */
class HumanPlayer : public Character
{
    Q_OBJECT
public:
    HumanPlayer(const QString &name, const Color &playerColor);
};

#endif // HUMANPLAYER_H
