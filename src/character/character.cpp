//system includes
#include <QString>
#include <QSet>
#include <QColor>

//local includes
#include "character/character.h"
#include "game_engine/structures.h"




/*** =Public Methods= ***/

//default constructor
Character::Character()
{

}

/**
    Sets playerName to name.  Retrives computers controlled and troop
    count from the graph created when a map file is read in
  */
Character::Character(const QString &name, const Color &playerColor)
{
    //identifier for the player
    _playerName = name;

    //set the player's color to the passed in value
    _playerColor = playerColor;
}

QSet<Computer *> Character::getControlledSet() const
{
    return _controlled;
}

Color Character::getColor() const
{
    return _playerColor;
}

//returns true if the comptuer is controlled by this player
//returns false if not
bool Character::isController(Computer* isControlled) const
{
    return _controlled.contains(isControlled);
}

QString Character::name() const
{
    return _playerName;
}

//changes playerName to newName
void Character::changePlayerName(const QString &newName)
{
    _playerName = newName;
}

int Character::computersControlled () const
{
    return _controlled.size();
}

/*** =Public Slots= ***/

//adds a computer to the list of controlled computers
void Character::addToControlled(Computer* toAdd)
{
    _controlled.insert(toAdd);
}

//removes the computer from the list of controlled computers
void Character::removeFromControlled(Computer* toRemove)
{
    if (!_controlled.isEmpty ())
    {
        _controlled.remove(toRemove);
        if (_controlled.isEmpty ())
            emit lostAllComputers (this);
    }
}


