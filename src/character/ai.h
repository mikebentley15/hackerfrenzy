#ifndef AI_H
#define AI_H

#include "character/character.h"
#include "game_engine/structures.h"
#include <QSet>

class Attack;
class Computer;
class Graph;
class QString;

class AI : public Character
{
    Q_OBJECT

public:

    /** different types of available algorithms for this AI player. */
    enum Algorithm
    {
        MEDIUM_AI,
        SIMPLE_AI,
        RANDOM_AI
    };

    AI (const QString &name, const Color &playerColor, Algorithm algorithmType = SIMPLE_AI);
    ~AI ();

    Algorithm algorithmType () const;

public slots:

    /** Sets the AI algorithm to be used. */
    void setAIAlgorithm (Algorithm type);

    /**
     * Updates the current time for this AI.
     *
     * This is where the AI algorithm gets called.  Every time the
     * updateTime is called, the corresponding algorithm will be
     * called that will decide whether or not to send one or more
     * attacks and to which devices.
     *
     * Calling this method may or may not instantiate the
     * initiateAttack() signal.
     */
    void updateTime (int elapsedMillis);

signals:
    /**
     * Signal that gets sent out to the game level state to start an attack.
     *
     * This attack object is created on the heap and sent.  The attack is
     * unpaused and also unstarted.
     *
     * Note: this AI class is the parent to the attack object sent, so if
     * it is not deleted by the recipient of this signal, then it will be
     * deleted when this object is deleted.
     *
     * If you want to delete this object and not the attack object with it,
     * then you must reset the parent either to NULL or another QObject.
     */
    void initiateAttack (Computer* from, Computer* to);

private:
    /**
     * _ownedComputers  - The list of owned computers.  This is used
     *                    by the algorithms to determine how to attack.
     * _type            - The type of AI algorithm to use.
     */
    Graph* _graph;
    Algorithm _type;

    // algorithms used based on the Algorithm _type value
    void mediumAlgorithm (int elapsedMillis);
    void simpleAlgorithm (int elapsedMillis);
    void randomAlgorithm (int elapsedMillis);

    //used by simple algorithm to reinforce forward computers
    void checkForReinforce (Computer *origin, Computer *toReinforce);

};

#endif // AI_H
