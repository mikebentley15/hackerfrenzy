#include "character/ai.h"

#include "attack.h"
#include "audit.h"
#include "character/character.h"
#include "game_engine/structures.h"
#include "graph/computer.h"

#include <QSet>
#include <QString>
#include <QtDebug>
#include <limits.h>


/*** =Public Methods= ***/

AI::AI(const QString &name, const Color &playerColor, Algorithm algorithmType)
    : Character(name, playerColor)
{
    Audit::get()->registerQObject (this);
    _type = algorithmType;
}

AI::~AI()
{
}

AI::Algorithm AI::algorithmType() const
{
    return _type;
}



/*** =Public Slots= ***/

void AI::setAIAlgorithm(AI::Algorithm type)
{
    _type = type;
}

void AI::updateTime(int elapsedMillis)
{
    switch (_type)
    {
    case MEDIUM_AI:
        mediumAlgorithm(elapsedMillis);
        break;
    case SIMPLE_AI:
        simpleAlgorithm (elapsedMillis);
        break;
    case RANDOM_AI:
        randomAlgorithm (elapsedMillis);
        break;
    default:
        randomAlgorithm (elapsedMillis);
    }
}



/*** =Private Methods= ***/

/**
  * Medium attack algorithm
  * Same as simple algorithm except it prioritizes attacking and reinforcing weaker computers
  * over attacking and reinforcing stronger computers
  */

void AI::mediumAlgorithm(int elapsedMillis)
{
    Q_UNUSED(elapsedMillis);

    //if the AI doesn't control anything, don't do anything
    if(_controlled.size()==0)
    {
        return;
    }

    QSetIterator<Computer*> itThroughList = QSetIterator<Computer*>(_controlled);

    //pointer to attacking computer
    Computer* attackOrigin = NULL;

    while(itThroughList.hasNext())
    {
        attackOrigin = itThroughList.next();

        //stores the computer with the least strength
        Computer* smallest = NULL;

        //store the lowest strength we have encountered so far
        int leastStrength = INT_MAX;

        for(int dir = 0; dir<4; dir++)
        {
            //cast dir as a Computer::Direction to work with the adjacency list
            Computer::Direction direction = (Computer::Direction) dir;

            //get the computer from the adjacency list
            Computer* toAttack = attackOrigin->getEdge(direction);

            //if there is a computer there
            if(toAttack!=NULL)
            {
                //and it has less strength than the last one we checked
                if(toAttack->getAvailableStrength()<leastStrength)
                {
                    smallest = toAttack;
                    leastStrength = toAttack->getAvailableStrength();

                }
            }
        }

        //if smallest is not controlled by the player, attack it
        if(smallest->getController()!=this)
        {
            emit initiateAttack(attackOrigin, smallest);
        }
        //otherwise, check to see if it needs to be reinforced
        else
        {
            checkForReinforce(attackOrigin, smallest);
        }
    }

}

/**
  * Simple attack algorithm
  * Algorithm checks adjacent computers
  *     if adjacent computer is not controlled by AI and controlled
  *     computer has full strength or 50% of the strength of the adjacent
  *     computer an attack is launched.
  *
  *
  */
void AI::simpleAlgorithm(int elapsedMillis)
{
    Q_UNUSED(elapsedMillis);

    //if the AI doesn't control any computers, don't do anything
    if(_controlled.size() == 0)
    {
        return;
    }

    //iterator to go through the list of controlled computers
    QSetIterator<Computer*> itThroughList = QSetIterator<Computer*>(_controlled);

    //pointer to the attacking computer
    Computer* attackOrigin = NULL;

    //go through the list of computers controlled by the AI
    while(itThroughList.hasNext())
    {

        //get the computer from the iterator
        attackOrigin = itThroughList.next();

        //loop through the directions in the adjacency list
        for(int i = 0; i<4; i++)
        {
            Computer::Direction direction = (Computer::Direction) i;

            //get the computer in the direction
            Computer* toAttack = attackOrigin->getEdge(direction);

            //if there is a computer in that direction
            if(toAttack!=NULL)
            {
                //check if it's controlled by the AI
                if(toAttack->getController()!=this)
                {
                    //if attackOrigin has at least 50% strength
                    if( (float) attackOrigin->getAvailableStrength()/attackOrigin->getCapacity()>=.5)
                    {
                        //attack
                        emit initiateAttack(attackOrigin, toAttack);
                    }

                }
                //computer is controlled by the AI
                else
                {
                    //check to see if it needs to be reinforced
                    checkForReinforce(attackOrigin, toAttack);
                }

            }
        }

    }

}

void AI::checkForReinforce(Computer* origin, Computer* toReinforce)
{

    //get the registered attacks for origin
    QSet<Attack*> attacks = *origin->getRegisteredAttacks();

    //iterate through the attacks on origin
    QSetIterator<Attack*> itThroughAttacks = QSetIterator<Attack*>(attacks);

    //if origin has more than 60% of its strength and toReinforce is below 50%
    if(float(origin->getAvailableStrength())/origin->getCapacity()>=.5 && float(toReinforce->getAvailableStrength())/toReinforce->getCapacity()<.3)
    {

        //while there are attacks left to analyze
        while(itThroughAttacks.hasNext() && !attacks.empty())
        {
            //get the attack object
            Attack* isFriendly = itThroughAttacks.next();

            //if there is an attack coming from toReinforce, don't attack toReinforce
            if(isFriendly->from()==toReinforce)
            {
                return;
            }
        }
        emit initiateAttack(origin, toReinforce);
    }
}


/**
 * This algorithm is as such:
 *   5% chance to initiate an attack.
 *   equal probability of source computer.
 *   equal probability of destination computer.
 *   maximum of one attack initiated per update.
 *   attack sends 50% of the origin computer's available strength
 */
void AI::randomAlgorithm(int elapsedMillis)
{
    Q_UNUSED (elapsedMillis);

    if (_controlled.size() == 0)
        return;

    // 5% chance to initiate an attack
    bool shouldAttack = (static_cast<qreal>(qrand()) / RAND_MAX) <= 0.05;

    if (shouldAttack)
    {
        // equal probability of source computer
        int choiceCount = _controlled.size();
        int choice = qrand() % choiceCount;

        // iterate to the choice made
        Computer* attackOrigin = NULL;
        QSetIterator<Computer*> it = QSetIterator<Computer*> (_controlled);
        while (it.hasNext ())
        {
            if (choice == 0)
            {
                attackOrigin = it.next();
                break;
            }
            else
                it.next();

            // decrement how far away we are from the choice now
            choice--;
        }

        if (attackOrigin == NULL)
            qWarning() << "attackOrigin should not be NULL";

        // equal probability of destination computer
        QVector<Computer*> adjacencyList = attackOrigin->getAdjacencyList();
        choice = qrand() % adjacencyList.size();
        Computer* attackDestination = adjacencyList.at(choice);

        emit initiateAttack (attackOrigin, attackDestination);
    }
}
