#ifndef STATE_TITLEMENU_H
#define STATE_TITLEMENU_H

#include "state/state.h"
#include <QPoint>
#include <QObject>
#include <QRect>

class Input;
class RenderSprite;
class Sprite;
class StateSystem;
class TextureManager;
class TitleMenuConfig;

class TitleMenuState : public State
{
    Q_OBJECT

    /** Represents one of the menu selections */
    struct Selection
    {
        QRect _rect;
        Sprite* _associatedSprite;
        QString _targetState;
    };

public:
    TitleMenuState (StateSystem* system, TextureManager* textureManager, GameSound *soundCreator);
    ~TitleMenuState ();
    
    // Abstract methods from State
    void Update(double elapsedTime);
    void Render();
    QString uploadTexture (QString type);
    void updateSprites (Action event, Sprite* sprite);

public slots:
    // Abstract methods from the State class
    void activateState ();
    void deactivateState ();

protected:
    void mouseMoveEvent (QMouseEvent *event);
    void mouseReleaseEvent (QMouseEvent *event);
    void keyPressEvent (QKeyEvent *event);

private slots:
    void setTitleX (int newX);
    void setPlayX  (int newX);
    void setTutorialX (int newX);
    void setQuitX  (int newX);

    void setTitleY (int newY);
    void setPlayY  (int newY);
    void setTutorialY (int newY);
    void setQuitY  (int newY);

    void displayConfigWindow ();

    void changeSelection (Selection* selection);
    void setupPossibleSelections ();

private:
    TextureManager* _textureManager;
    StateSystem* _system;
    RenderSprite* _renderSprite;
    Sprite** _sprites;
    int _spriteCount;
    TitleMenuConfig* _config;
    QPoint _titlePos;
    QPoint _playPos;
    QPoint _tutorialPos;
    QPoint _quitPos;
    Selection  _possibleSelections[3];
    Selection* _currentSelection;
    int _possibleSelectionCount;

    // Helper methods
    void loadSprites ();
    void setupSprites ();
};

#endif // TITLEMENU_H
