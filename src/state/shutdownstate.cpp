#include "state/shutdownstate.h"

#include "game_engine/statesystem.h"
#include "game_engine/SpriteRendering/texture_manager.h"

#include <QGLWidget>

/*** =Public Methods= ***/

ShutdownState::ShutdownState(StateSystem* system, TextureManager* textureManager,
                             QGLWidget *parent)
    : State (parent)
{
    _system = system;
    _textureManager = textureManager;
    _widget = parent;
}

// Empty implementations because we don't render anything for this state.
void ShutdownState::Update (double elapsedTime) { Q_UNUSED (elapsedTime); }
void ShutdownState::Render () {}
QString ShutdownState::uploadTexture (QString type)
{ Q_UNUSED (type); return ""; }
void ShutdownState::updateSprites (Action event, Sprite* sprite)
{ Q_UNUSED (event); Q_UNUSED (sprite); }


/*** =Public Slots= ***/

void ShutdownState::activateState ()
{
    _widget->close ();
}
