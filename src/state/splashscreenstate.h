#ifndef STATE_SPLASHSCREEN_H
#define STATE_SPLASHSCREEN_H

#include "state/state.h"

class Font;
class FontParser;
class GameSound;
class RenderSprite;
class Sprite;
class StateSystem;
class Text;
class Texture;
class TextureManager;

class SplashScreenState : public State
{
public:
    SplashScreenState (StateSystem* system, TextureManager* textureManager, GameSound *soundCreator);
    ~SplashScreenState ();

    /** Virtual Methods from base **/
    void Update (double elapsedTime);
    void Render ();
    void updateSprites (Action event, Sprite* sprite);
    QString uploadTexture (QString type);


public slots:
    void activateState ();
    void deactivateState ();

private:
    Sprite** _sprites;
    int _spriteCount;
    int _currentSprite;
    double _delay;
    QPointF _viewportAdjustor;
    double _wait;
    bool walking;

    Font* _font;
    FontParser* _fontParser;
    QVector<Text*> _text;


};

#endif // SPLASHSCREENSTATE_H
