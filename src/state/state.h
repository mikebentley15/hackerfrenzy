#ifndef STATE_H
#define STATE_H

#include <QString>
#include <QObject>
#include <QWidget>
#include <QHash>

class GameSound;
class RenderSprite;
class Graph;
class Sprite;
class StateSystem;
class TextureManager;

class State : public QObject
{
    Q_OBJECT
    friend class StateSystem;

public:
    explicit State (QObject *parent = 0);

    QPointF convertToGLCoordinates (const QPointF &point);
    QPoint  convertToGLCoordinates (const QPoint  &point);

    enum Action
    {
        MouseClick,
        MouseEnter,
        MouseExit,
        MouseDrag
    };

    /**
     * Updates the state of this state based on how much time has
     * elapsed since the last update.
     *
     * @param elapsedTime
     *   time elapsed in seconds since the last update (or since
     *   activation if it's the first time)
     */
    virtual void Update (double elapsedTime) = 0;

    /** Renders this state to the screen. */
    virtual void Render () = 0;

    /** Tells the current state what textures to use **/
    virtual QString uploadTexture (QString type);

    /** Tells the current state how to update its sprites for specific events **/
    virtual void updateSprites (Action event, Sprite* sprite);

signals:

public slots:
    /**
     * This slot is called from the StateMachine class as soon as
     * this state becomes active.  Basically, this is where the
     * initialization for the state happens.
     */
    virtual void activateState ();

    /**
     * This slot is called from the StateMachine class as soon as
     * the state changes from this state to another state (i.e.
     * you are leaving this state).  Basically, this is where the
     * cleanup for this state occurs.
     */
    virtual void deactivateState ();

protected:
    QHash<QString, Sprite*> _spriteMap;
    Sprite* _background;
    RenderSprite* _renderSprite;
    GameSound* _soundCreator;
    Sprite* _tempSprite;
    StateSystem* _system;
    TextureManager* _textureManager;
    double _elapsedTime;
    double _totalTime;
    QPointF _viewportAdjustor;

    QPoint mousePosition ();
    QSize windowSize ();

    bool mouseOnSprite (QString key);
    bool clickedOnSprite (Action event, QString key);

    Sprite* getSprite(QString key);

    void createSprites (int count, Sprite** _sprites, QString texture = "", Graph* level = NULL);
    void renderBackground();

    // These events are forwarded here and can be overwritten by children classes
    // Their documentation is found in the Qt documentation for QWidget.
    virtual void closeEvent            (QCloseEvent* event);
    virtual void dragEnterEvent        (QDragEnterEvent* event);
    virtual void dragLeaveEvent        (QDragLeaveEvent* event);
    virtual void dragMoveEvent         (QDragMoveEvent* event);
    virtual void dropEvent             (QDropEvent* event);
    virtual void enterEvent            (QEvent* event);
    virtual void keyPressEvent         (QKeyEvent* event);
    virtual void keyReleaseEvent       (QKeyEvent* event);
    virtual void leaveEvent            (QEvent* event);
    virtual void mouseDoubleClickEvent (QMouseEvent* event);
    virtual void mouseMoveEvent        (QMouseEvent* event);
    virtual void mousePressEvent       (QMouseEvent* event);
    virtual void mouseReleaseEvent     (QMouseEvent* event);
    virtual void resizeEvent           (QResizeEvent* event);
    virtual void tabletEvent           (QTabletEvent* event);
    virtual void wheelEvent            (QWheelEvent* event);

private:
    QPoint _mousePosition;
    QSize _windowSize;

    //Used to check if the key exists in the hashmap
    QHash<QString, Sprite*>::iterator _hashIter;

    void setMousePosition (const QPoint &position);
    void setWindowSize (const QSize &size);

};

#endif // STATE_H
