//#include <QBitmap>
#include <QtOpenGL>
#include <QDebug>

#include "state/splashscreenstate.h"

#include "audit.h"
#include "game_engine/gamesound.h"
#include "game_engine/SpriteRendering/rendersprite.h"
#include "game_engine/SpriteRendering/sprite.h"
#include "game_engine/SpriteRendering/texture.h"
#include "game_engine/SpriteRendering/texture_manager.h"
#include "game_engine/statesystem.h"
#include "game_engine/structures.h"
#include "game_engine/TextRendering/font.h"
#include "game_engine/TextRendering/fontparser.h"
#include "game_engine/TextRendering/text.h"


/*** =Public Methods= ***/

SplashScreenState::SplashScreenState(StateSystem* system, TextureManager* textureManager, GameSound* soundCreator)
    : State ()
{
    Audit::get()->objectCreated ("SplashScreenState");

    _delay = 1;

    _system = system;
    _textureManager = textureManager;
    _renderSprite = new RenderSprite ();
    _soundCreator = soundCreator;

    _spriteCount = 5;
    _sprites = new Sprite* [_spriteCount];

    _background = new Sprite ();
    _background->setTexture(_textureManager->Get("backgroundtexture1"));

    _tempSprite = NULL;
    _currentSprite = 0;

    walking = true;
    _delay = 6;
    _wait = 1;

    _fontParser = new FontParser();

    QHash<QChar, CharacterData> temp;
    temp = _fontParser->Parse(":/images/Font/font.txt");

    _font = new Font (*_textureManager->Get("fonttexture1"),
                      _fontParser->Parse(":/images/Font/font.txt"));

    _text.push_back(new Text("Sinister", *_font));
    _text.push_back(new Text("Intentions", *_font));
    _text[0]->setPos (0, 20);
    _text[1]->setPos (0,  0);

    createSprites(_spriteCount, _sprites, "12345");
}

SplashScreenState::~SplashScreenState ()
{
    Audit::get()->objectDestroyed ("SplashScreenState");
    delete _renderSprite;

    for (int i = 0; i < 2; i++)
        delete _sprites[i];
    
    for (int t = 0; t <_text.size(); t ++)
        delete _text.at(t);

    delete[] _sprites;
    delete _font;
    delete _fontParser;
}

void SplashScreenState::Update(double elapsedTime)
{
  _elapsedTime = elapsedTime;

  if(!walking)
    _delay -= elapsedTime;

  if(_delay <= 0)
  {
    _delay = 1;

    _soundCreator->playSoundEffect ("impact");
    _system->changeState("title_menu");
  }
}

void SplashScreenState::Render()
{
    glClearColor (0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    renderBackground();

    _wait -= _elapsedTime;

    if(walking)
    {
        if(_wait < 0.96)
        {
            _totalTime += _elapsedTime;
            _wait = 1;

            _currentSprite++;

            if (_currentSprite >= _spriteCount - 1)
                _currentSprite = 0;
        }

        QString key;
        key = key.setNum(_currentSprite);

        _tempSprite = getSprite(key);

        int position = (_totalTime * 500) - windowSize().width()/2;

        if (position >= 0)
        {
            _tempSprite->setTexture(_textureManager->Get("othertexture1"));
            _tempSprite->setWidth(150); _tempSprite->setHeight(150);

            _renderSprite->DrawSprite(_tempSprite);

            walking = false;
        }

        _tempSprite->setPosition(position, 0);
        _renderSprite->DrawSprite(_tempSprite);
    }
    else
    {
        if (_delay > 5)
            _renderSprite->DrawSprite(_tempSprite);
        else
        {
            _text.at(0)->setScale(2);
            _renderSprite->DrawText(*(_text.at(0)));
            _renderSprite->DrawText(*(_text.at(1)));
        }
    }
    glFinish();
}


QString SplashScreenState::uploadTexture(QString type)
{
    if(type == "1")
        return "splashtexture1";
    else if(type == "2")
        return "splashtexture2";
    else if(type == "3")
        return "splashtexture3";
    else if(type == "4")
        return "splashtexture4";
    else if(type == "5")
        return "splashtexture5";
    else // default
        return "splashtexture1";
}

void SplashScreenState::updateSprites (Action event, Sprite* sprite)
{
    Q_UNUSED (event);
    Q_UNUSED (sprite);
}



/*** =Public Slots= ***/

void SplashScreenState::activateState ()
{

}

void SplashScreenState::deactivateState ()
{

}

