#include "state/levelselectionstate.h"

#include "game_engine/levelloader.h"
#include "game_engine/SpriteRendering/sprite.h"
#include "game_engine/SpriteRendering/rendersprite.h"
#include "game_engine/SpriteRendering/texture_manager.h"
#include "game_engine/statesystem.h"
#include "game_engine/structures.h"

#include <QFileDialog>


/*** =Public Methods= ***/

LevelSelectionState::LevelSelectionState(StateSystem* system, TextureManager* textureManager,
                                         GameSound* soundCreator, LevelLoader* gameState)
{
    _system       = system;
    _textureManager = textureManager;
    _renderSprite = new RenderSprite (this);
    _soundCreator = soundCreator;
    _gameState    = gameState;
    _levelCount   = _gameState->getLevelCount () + 1; // one is the choose your own
    _selectedSprite = NULL;

    setupSprites ();
}

void LevelSelectionState::Update (double elapsedTime)
{
    Q_UNUSED (elapsedTime);
}

void LevelSelectionState::Render ()
{
    glClearColor (0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    for (int i = 0; i < _levelCount; i++)
    {
        _renderSprite->DrawSprite (_levelSprites[i]);
    }

    glFinish();
}



/*** =Public Slots= ***/

void LevelSelectionState::activateState ()
{
    if (_selectedSprite != NULL)
        unselectSprite ();
}

void LevelSelectionState::deactivateState ()
{

}

void LevelSelectionState::keyPressEvent(QKeyEvent *event)
{
    if (event->key () == Qt::Key_Escape)
    {
        _system->changeState ("title_menu");
    }
}

void LevelSelectionState::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED (event);

    positionSprites ();
}

void LevelSelectionState::mouseMoveEvent(QMouseEvent *event)
{
    Q_UNUSED (event);

    bool mouseIsOverSprite = false;
    for (int i = 0; i < _levelCount; i++)
    {
        QString key = QString ("level%1").arg(i + 1);
        if (mouseOnSprite (key))
        {
            mouseIsOverSprite = true;
            if (_selectedSprite != NULL && _selectedSprite != _levelSprites[i])
            {
                unselectSprite ();
            }
            selectSprite (i);
            break;
        }
    }

    if (!mouseIsOverSprite && _selectedSprite != NULL)
        unselectSprite ();
}

void LevelSelectionState::mousePressEvent(QMouseEvent *event)
{
    Q_UNUSED (event);

    if (_selectedSprite != NULL)
    {
        for (int i = 0; i < _levelCount; ++i)
        {
            if (_selectedSprite == _levelSprites[i])
            {
                // If the choose your own map was chosen
                if (i == _levelCount - 1)
                {
                    QString filename = QFileDialog::getOpenFileName (NULL, tr("Open User Level"));
                    _gameState->loadMap (filename);
                }

                // If a built-in map was chosen
                else
                {
                    _gameState->loadMap (i);
                }

                _system->changeState ("game_play");
            }
        }
    }
}

void LevelSelectionState::selectSprite(int index)
{
    _selectedSprite = _levelSprites[index];
    _selectedSprite->setColor (Color(1,1,1,0.5));
}

void LevelSelectionState::unselectSprite()
{
    _selectedSprite->setColor (Color(1,1,1,1));
    _selectedSprite = NULL;
}

void LevelSelectionState::setupSprites()
{
    _levelSprites = new Sprite*[_levelCount];
    for (int i = 0; i < _levelCount; i++)
    {
        QString textureName = QString ("leveltexture%1").arg(i + 1);
        _levelSprites[i] = new Sprite (this);
        _levelSprites[i]->setTexture (_textureManager->Get (textureName));
        _levelSprites[i]->setChangeability (true);

        QString key = QString("level%1").arg(i + 1);
        _spriteMap.insert (key, _levelSprites[i]);
    }

    positionSprites ();
}

void LevelSelectionState::positionSprites()
{
    for (int i = 0; i < _levelCount; i++)
    {
        // Position the sprite.
        int row = i / 3;
        int col = i % 3;
        int w = windowSize ().width ();
        int h = windowSize ().height ();

        int x = col * w / 3 + w / 6 - w / 2;
        int y = - row * h / 3 - h / 6 + h / 2;

        _levelSprites[i]->setPosition (x, y);
    }
}



