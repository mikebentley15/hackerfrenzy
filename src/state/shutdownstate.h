#ifndef SHUTDOWNSTATE_H
#define SHUTDOWNSTATE_H

#include "state/state.h"

class QGLWidget;
class StateSystem;
class TextureManager;

/**
 * Represents the state that cleans up the system and prepares for closing
 * the application.
 */
class ShutdownState : public State
{
    Q_OBJECT

public:
    ShutdownState(StateSystem* system, TextureManager* textureManager,
                  QGLWidget* parent);

    void Update (double elapsedTime);
    void Render ();
    QString uploadTexture (QString type);
    void updateSprites (Action event, Sprite* sprite);

public slots:
    void activateState ();

private:
    QGLWidget* _widget;
};

#endif // SHUTDOWNSTATE_H
