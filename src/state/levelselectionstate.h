#ifndef LEVELSELECTIONSTATE_H
#define LEVELSELECTIONSTATE_H

#include "state/state.h"

class StateSystem;
class TextureManager;
class SoundCreator;
class LevelLoader;

class LevelSelectionState : public virtual State
{
public:
    LevelSelectionState(StateSystem* system, TextureManager *textureManager,
                        GameSound* soundCreator, LevelLoader* gameState);

    void Update (double elapsedTime);
    void Render ();

public slots:
    void activateState ();
    void deactivateState ();

protected slots:
    void keyPressEvent (QKeyEvent *event);
    void resizeEvent (QResizeEvent *event);
    void mouseMoveEvent (QMouseEvent *event);
    void mousePressEvent (QMouseEvent *event);

private:
    Sprite** _levelSprites;
    LevelLoader* _gameState;
    int _levelCount;
    Sprite* _selectedSprite;

    void selectSprite (int index);
    void unselectSprite ();

    void setupSprites ();
    void positionSprites ();
};

#endif // LEVELSELECTIONSTATE_H
