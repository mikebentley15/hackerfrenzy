#include <QWidget>
#include <QMouseEvent>

#include "state/state.h"

#include "audit.h"
#include "game_engine/gamesound.h"
#include "game_engine/SpriteRendering/rendersprite.h"
#include "game_engine/SpriteRendering/sprite.h"
#include "game_engine/SpriteRendering/texture_manager.h"
#include "graph/graph.h"

/*** =Public Methods= ***/

State::State (QObject *parent)
    : QObject(parent)
{
    Audit::get()->registerQObject (this);
    _mousePosition = QPoint (0, 0);
}

QPointF State::convertToGLCoordinates (const QPointF &point)
{
    QPointF adjusted;
    adjusted.setX (point.x() - windowSize().width()/2);
    adjusted.setY (windowSize().height()/2 - point.y());
    return adjusted;
}

QPoint State::convertToGLCoordinates (const QPoint &point)
{
    QPoint adjusted;
    adjusted.setX (point.x() - windowSize().width()/2);
    adjusted.setY (windowSize().height()/2 - point.y());
    return adjusted;
}

QString State::uploadTexture (QString type)
{ Q_UNUSED (type); return ""; }
void State::updateSprites(State::Action event, Sprite *sprite)
{ Q_UNUSED (event); Q_UNUSED (sprite); }



/*** =Public Slots= ***/

// empty implementations so that inherited classes can choose to implement these
void State::activateState() {}
void State::deactivateState() {}

/*** =Protected Methods= ***/

QPoint State::mousePosition ()
{
    return _mousePosition;
}

QSize State::windowSize ()
{
    return _windowSize;
}

// There is an empty implementation of each of these because inherited
// classes don't need to implement all of these, just the ones that it
// needs.
void State::closeEvent            (QCloseEvent*) {}
void State::dragEnterEvent        (QDragEnterEvent*) {}
void State::dragLeaveEvent        (QDragLeaveEvent*) {}
void State::dragMoveEvent         (QDragMoveEvent*) {}
void State::dropEvent             (QDropEvent*) {}
void State::enterEvent            (QEvent*) {}
void State::keyPressEvent         (QKeyEvent*) {}
void State::keyReleaseEvent       (QKeyEvent*) {}
void State::leaveEvent            (QEvent*) {}
void State::mouseDoubleClickEvent (QMouseEvent*) {}
void State::mouseMoveEvent        (QMouseEvent*) {}
void State::mousePressEvent       (QMouseEvent*) {}
void State::mouseReleaseEvent     (QMouseEvent*) {}
void State::resizeEvent           (QResizeEvent *) {}
void State::tabletEvent           (QTabletEvent*) {}
void State::wheelEvent            (QWheelEvent*) {}



/*** =Private Methods= ***/

void State::setMousePosition (const QPoint &position)
{
    _mousePosition = convertToGLCoordinates(position);
}

void State::setWindowSize (const QSize &size)
{
    this->_windowSize = size;
}

void State::createSprites (int count, Sprite** sprites, QString texture, Graph* level)
{
    QString key, finalKey, xLoc, yLoc;
    QPoint location;

    int setWidth = 56;
    int setHeight = 56;

    //if it's a level then the count should be the number of devices or connectors
    if(level != NULL)
    {
        if(texture == "connector")
        {
            count = level->getNumberOfConnectors();
            setWidth = 22; setHeight = 22;
        }
        else
            count = level->getNumberOfDevices();
    }
    for(int i = 0; i < count; i ++)
    {
        sprites[i] = new Sprite();

        if (level != NULL)
        {
            if(texture == "connector")
            {
                location = level->connectorLocations()->at(i);
                sprites[i]->setChangeability(false);
            }
            else
                location = level->deviceLocations()->at(i);

            key = level->at(location)->getType();
            finalKey = key + xLoc.setNum(location.x()) + "," + yLoc.setNum(location.y());

            sprites[i]->setTexture(_textureManager->Get(uploadTexture(key)));
        }
        else
        {
            sprites[i]->setTexture(_textureManager->Get(uploadTexture(texture.at(i))));
            finalKey = finalKey.setNum(i);
        }

        sprites[i]->setWidth(setWidth);
        sprites[i]->setHeight(setHeight);

        _spriteMap.insert(finalKey, sprites[i]);
    }
}

Sprite* State::getSprite(QString key)
{
    _hashIter = _spriteMap.find(key);

    if (_hashIter != _spriteMap.end())
        return _spriteMap.value(key);

    return NULL;
}

bool State::mouseOnSprite (QString key)
{
    Sprite* current = NULL;
    current = _spriteMap.value(key);
    double x = mousePosition().x();
    double y = mousePosition().y();

    if(current->getChangeability())
        if(x <= (current->getCenter().X + current->getHeight()/2) &&
           x >= (current->getCenter().X - current->getHeight()/2) &&
           y <= (current->getCenter().Y + current->getWidth()/2)  &&
           y >= (current->getCenter().Y - current->getWidth()/2))
           return true;

    return false;
}

bool State::clickedOnSprite (Action event, QString key)
{
    if(mouseOnSprite(key))
    {
        updateSprites(event, _spriteMap.value(key));
        return true;
    }

    return false;
}

void State::renderBackground()
{
    _background->setWidth(windowSize().width());
    _background->setHeight(windowSize().height());
    _background->setPosition(0,0);
    _background->setChangeability(false);

    _renderSprite->DrawSprite(_background);
}
