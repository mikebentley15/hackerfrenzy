#include <QtOpenGL>
#include <QPoint>

#include "state/titlemenustate.h"

#include "audit.h"
#include "forms/titlemenuconfig.h"
#include "game_engine/gamesound.h"
#include "game_engine/SpriteRendering/texture_manager.h"
#include "game_engine/SpriteRendering/rendersprite.h"
#include "game_engine/SpriteRendering/sprite.h"
#include "game_engine/statesystem.h"
#include "macros.h"


/*** =Public Methods= ***/

TitleMenuState::TitleMenuState (StateSystem* system,
                                TextureManager* textureManager,
                                GameSound* soundCreator)
    : State()
{
    Audit::get()->objectCreated ("TitleMenuState");
    
    // Set all of the pointers to NULL before initializing.
    // This will help debug if we forget to initialize something
    _textureManager = NULL;
    _system = NULL;
    _renderSprite = NULL;
    _sprites = NULL;
    _currentSelection = NULL;

    _config = NULL;

    // Start initializing
    _system = system;
    _textureManager = textureManager;
    _soundCreator = soundCreator;
    
    _renderSprite = new RenderSprite();
    _spriteCount = 4;
    _titlePos    = QPoint (-268, 195);
    _playPos     = QPoint (-421, 0);
    _tutorialPos = QPoint (-367, -57);
    _quitPos     = QPoint (-424, -117);
    _possibleSelectionCount = 3;

    this->loadSprites ();
    this->setupSprites ();
    this->setupPossibleSelections (); // Will have to redo this when the state activates
}

TitleMenuState::~TitleMenuState ()
{
    Audit::get()->objectDestroyed ("TitleMenuState");
    if (_renderSprite != NULL)
    {
        delete _renderSprite;
        _renderSprite = NULL;
    }

    if (_sprites != NULL)
    {
        for (int i = 0; i < _spriteCount; ++i)
            if (_sprites[i] != NULL)
            {
                delete _sprites[i];
                _sprites[i] = NULL;
            }

        delete[] _sprites;
        _sprites = NULL;
    }

    if (_config != NULL)
    {
        delete _config;
        _config = NULL;
    }
}

void TitleMenuState::Render ()
{
    glClearColor (0, 0, 0, 1);
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    _renderSprite->DrawSprite (_sprites[0]);
    _renderSprite->DrawSprite (_sprites[1]);
    _renderSprite->DrawSprite (_sprites[2]);
    _renderSprite->DrawSprite (_sprites[3]);


    glFinish ();
}

void TitleMenuState::Update (double elapsedTime)
{
    Q_UNUSED (elapsedTime);
}

QString TitleMenuState::uploadTexture(QString type)
{
    Q_UNUSED (type);
    return "TODO";
}

void TitleMenuState::updateSprites (Action event, Sprite* sprite)
{
    Q_UNUSED (event);
    Q_UNUSED (sprite);
}



/*** =Public Slots= ***/

void TitleMenuState::activateState ()
{
    this->setupPossibleSelections ();
    _soundCreator->playRepeatingBackgroundMusic ("background1");
    displayConfigWindow ();
}

void TitleMenuState::deactivateState ()
{
    if (_config != NULL)
    {
        _config->close();  // Close the window
        _config->deleteLater();  // Delete when it's convenient for the system
        _config = NULL;
    }

    _soundCreator->stopRepeatingBackgroundMusic ();
}



/*** =Protected Methods= ***/

void TitleMenuState::mouseMoveEvent (QMouseEvent *event)
{
    Q_UNUSED (event);

    // Find the first region the mouse is over
    bool foundSelection = false;
    for (int i = 0; i < _possibleSelectionCount; ++i)
    {
        if (_possibleSelections[i]._rect.contains(mousePosition()))
        {
            foundSelection = true;
            changeSelection (&_possibleSelections[i]);
            break;
        }
    }

    // If no box is moused over, then unselect whatever is selected
    if (!foundSelection)
        changeSelection (NULL);
}

void TitleMenuState::mouseReleaseEvent (QMouseEvent *event)
{
    Q_UNUSED (event);

    // If there is a choice selected, then change to the corresponding state
    if (_currentSelection != NULL)
    {
        _soundCreator->playSoundEffect ("click");
        _system->changeState(_currentSelection->_targetState);
    }
}

void TitleMenuState::keyPressEvent(QKeyEvent *event)
{
    if (event->key () == Qt::Key_Escape)
    {
        _system->changeState ("shutdown");
    }
}



/*** =Private Slots= ***/

void TitleMenuState::setTitleX (int newX)
{
    _titlePos.setX (newX);
    _sprites[0]->setPosition (_titlePos.x(), _titlePos.y());
}

void TitleMenuState::setPlayX  (int newX)
{
    _playPos.setX (newX);
    _sprites[1]->setPosition (_playPos.x(), _playPos.y());
}

void TitleMenuState::setTutorialX (int newX)
{
    _tutorialPos.setX (newX);
    _sprites[2]->setPosition (_tutorialPos.x(), _tutorialPos.y());
}

void TitleMenuState::setQuitX  (int newX)
{
    _quitPos.setX (newX);
    _sprites[3]->setPosition (_quitPos.x(), _quitPos.y());
}

void TitleMenuState::setTitleY (int newY)
{
    _titlePos.setY (newY);
    _sprites[0]->setPosition (_titlePos.x(), _titlePos.y());
}

void TitleMenuState::setPlayY  (int newY)
{
    _playPos.setY (newY);
    _sprites[1]->setPosition (_playPos.x(), _playPos.y());
}

void TitleMenuState::setTutorialY (int newY)
{
    _tutorialPos.setY (newY);
    _sprites[2]->setPosition (_tutorialPos.x(), _tutorialPos.y());
}

void TitleMenuState::setQuitY  (int newY)
{
    _quitPos.setY (newY);
    _sprites[3]->setPosition (_quitPos.x(), _quitPos.y());
}

void TitleMenuState::displayConfigWindow ()
{
    // Show the configure window.  This is for finding a good positioning
    _config = new TitleMenuConfig ();
    _config->setTitleX(_titlePos.x());
    _config->setTitleY(_titlePos.y());
    _config->setPlayX(_playPos.x());
    _config->setPlayY(_playPos.y());
    _config->setTutorialX(_tutorialPos.x());
    _config->setTutorialY(_tutorialPos.y());
    _config->setQuitX(_quitPos.x());
    _config->setQuitY(_quitPos.y());
    connect (_config, SIGNAL(titleXChanged(int)),
             this,      SLOT(setTitleX(int)));
    connect (_config, SIGNAL(playXChanged(int)),
             this,      SLOT(setPlayX(int)));
    connect (_config, SIGNAL(tutorialXChanged(int)),
             this,      SLOT(setTutorialX(int)));
    connect (_config, SIGNAL(quitXChanged(int)),
             this,      SLOT(setQuitX(int)));
    connect (_config, SIGNAL(titleYChanged(int)),
             this,      SLOT(setTitleY(int)));
    connect (_config, SIGNAL(playYChanged(int)),
             this,      SLOT(setPlayY(int)));
    connect (_config, SIGNAL(tutorialYChanged(int)),
             this,      SLOT(setTutorialY(int)));
    connect (_config, SIGNAL(quitYChanged(int)),
             this,      SLOT(setQuitY(int)));

    connect (_config, SIGNAL(playYChanged()),
             this,      SLOT(setupPossibleSelections()));
    connect (_config, SIGNAL(tutorialYChanged()),
             this,      SLOT(setupPossibleSelections()));
    connect (_config, SIGNAL(quitYChanged()),
             this,      SLOT(setupPossibleSelections()));
    _config->show();
}

void TitleMenuState::changeSelection (Selection* selection)
{
    if (_currentSelection != selection)
    {
        // Undo display effects for the previous selection
        if (_currentSelection != NULL)
        {
            // TODO: modify the sprite the selection corresponds to
            _currentSelection->_associatedSprite->setColor (Color(1,1,1,1));
        }

        // Set the selection to the new selection
        _currentSelection = selection;

        // Setup display effects for the new selection
        if (_currentSelection != NULL)
        {
            // TODO: modify the sprite the selection corresponds to.
            _currentSelection->_associatedSprite->setColor (Color(0.5,0.5,0.5,1));
        }
    }
}

void TitleMenuState::setupPossibleSelections ()
{
    QRect baseRect = QRect(-this->windowSize().width()/2, 0,
                           this->windowSize().width(), 80);
    for (int i = 0; i < _possibleSelectionCount; ++i)
    {
        _possibleSelections[i]._associatedSprite = _sprites[i+1];
        _possibleSelections[i]._rect = baseRect;
        _possibleSelections[i]._rect.setTop(_sprites[i+1]->getCenter().Y - 40);
        _possibleSelections[i]._rect.setBottom(_sprites[i+1]->getCenter().Y + 40);
    }

    _possibleSelections[0]._targetState = "level_select";
    _possibleSelections[1]._targetState = "tutorial";
    _possibleSelections[2]._targetState = "shutdown";
}



/*** =Private Methods= ***/

void TitleMenuState::loadSprites ()
{
    _sprites = new Sprite* [_spriteCount];

    for(int i = 0; i < _spriteCount; i ++)
    {
        _sprites[i] = new Sprite ();
        QString texture = QString("titlemenutexture%1").arg(i+1);
        _sprites[i]->setTexture(_textureManager->Get(texture));
    }
}

void TitleMenuState::setupSprites ()
{
    _sprites[0]->setPosition (_titlePos.x(),    _titlePos.y());
    _sprites[1]->setPosition (_playPos.x(),     _playPos.y());
    _sprites[2]->setPosition (_tutorialPos.x(), _tutorialPos.y());
    _sprites[3]->setPosition (_quitPos.x(),     _quitPos.y());
}



